<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class KphControllerTest extends TestCase
{
    public function testStore(){
        $response = $this->post('master-data/kph');
        $response->assertSessionHas('success');
    }
}
