<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiTiketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_tikets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tiket_wisata_id');
            $table->string('nomor_karcis',27);
            $table->enum('negara_pengunjung',['wna', 'wni']);
            $table->string('harga');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('changed_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_tikets');
    }
}
