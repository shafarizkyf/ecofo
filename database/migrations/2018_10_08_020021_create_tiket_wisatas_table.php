<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiketWisatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiket_wisatas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lokasi_id');
            $table->unsignedInteger('jenis_tiket_id');
            $table->unsignedInteger('loket_id');
            $table->integer('harga');
            $table->string('kode_karcis',15);
            $table->enum('jenis_waktu', ['weekday', 'weekend', 'all'])->default('all');
            $table->boolean('is_active')->default(true);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('changed_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiket_wisatas');
    }
}
