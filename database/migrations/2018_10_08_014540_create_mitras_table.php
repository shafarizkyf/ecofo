<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMitrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mitras', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('kph_id');
            $table->string('nama',60);
            $table->string('ketua',30)->nullable();
            $table->string('kontak',15)->nullable();
            $table->string('jabatan',60)->nullable();
            $table->boolean('is_active')->default(true);
            $table->boolean('is_pemda')->default(false);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('changed_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mitras');
    }
}
