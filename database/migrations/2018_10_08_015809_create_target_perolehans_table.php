<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTargetPerolehansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_perolehans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lokasi_id');
            $table->integer('pendapatan');
            $table->integer('pengunjung_wna');
            $table->integer('pengunjung_wni');
            $table->integer('pengunjung');
            $table->year('tahun');
            $table->boolean('is_active')->default(true);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('changed_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_perolehans');
    }
}
