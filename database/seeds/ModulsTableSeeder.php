<?php

use Illuminate\Database\Seeder;

class ModulsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/moduls.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('moduls')->insert($data);
    }
}
