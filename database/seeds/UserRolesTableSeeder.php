<?php

use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/user_roles.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('user_roles')->insert($data);
    }
}
