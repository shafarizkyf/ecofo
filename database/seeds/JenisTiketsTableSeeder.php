<?php

use Illuminate\Database\Seeder;

class JenisTiketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/jenis_tikets.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('jenis_tikets')->insert($data);        
    }
}
