<?php

use Illuminate\Database\Seeder;

class RolePermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/role_permissions.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('role_permissions')->insert($data);
    }
}
