<?php

use Illuminate\Database\Seeder;

class BiayaOperasionalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/biaya_operasionals.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('biaya_operasionals')->insert($data);                
    }
}
