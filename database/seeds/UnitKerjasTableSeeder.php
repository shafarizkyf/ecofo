<?php

use Illuminate\Database\Seeder;

class UnitKerjasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/unit_kerjas.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('unit_kerjas')->insert($data);
    }
}
