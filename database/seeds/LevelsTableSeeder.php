<?php

use Illuminate\Database\Seeder;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/levels.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('levels')->insert($data);
    }
}
