<?php

use Illuminate\Database\Seeder;

class TargetPerolehansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/target_perolehans.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('target_perolehans')->insert($data);        
    }
}
