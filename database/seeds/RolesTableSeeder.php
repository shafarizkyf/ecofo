<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/roles.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('roles')->insert($data);
    }
}
