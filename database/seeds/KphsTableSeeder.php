<?php

use Illuminate\Database\Seeder;

class KphsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/kphs.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('kphs')->insert($data);
    }
}
