<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/permissions.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('permissions')->insert($data);
    }
}
