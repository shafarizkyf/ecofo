<?php

use Illuminate\Database\Seeder;

class LokasisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/lokasis.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('lokasis')->insert($data);
    }
}
