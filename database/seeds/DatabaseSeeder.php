<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(UnitKerjasTableSeeder::class);
        $this->call(KphsTableSeeder::class);
        $this->call(LokasisTableSeeder::class);
        $this->call(JenisTiketsTableSeeder::class);
        $this->call(TargetPerolehansTableSeeder::class);
        $this->call(MitrasTableSeeder::class);
        $this->call(BiayaOperasionalsTableSeeder::class);
        $this->call(LevelsTableSeeder::class);
        $this->call(ModulsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        $this->call(RolePermissionsTableSeeder::class);
    }
}
