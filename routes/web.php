<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('reset-config', function(){
  Artisan::call('config:cache');
  Artisan::call('view:clear');
});

Route::get('/', function(){
  return redirect(route('dashboard.index'));
});

Route::get('download', function(){
  return response()->file(public_path("uploads/ecofo.apk") ,[
      'Content-Type'=>'application/vnd.android.package-archive',
      'Content-Disposition'=> 'attachment; filename="ecofo.apk"',
  ]);
});

Route::get('login', 'LoginController@index')->name('login');
Route::post('login', 'LoginController@login')->name('login.login');
Route::get('logout', 'LoginController@logout')->name('logout.logout');

Route::group(['middleware'=>['auth']], function(){

  Route::prefix('dashboard')->group(function(){
    Route::get('', 'DashboardController@index')->name('dashboard.index');
    Route::get('axIndex', 'DashboardController@axIndex')->name('dashboard.axIndex');
  });

  Route::prefix('transaksi')->group(function(){
    Route::get('', 'TransaksiTiketController@index')->name('transaksi_tiket.index');
    Route::get('tambah', 'TransaksiTiketController@add')->name('transaksi_tiket.add');
    Route::post('', 'TransaksiTiketController@store')->name('transaksi_tiket.store');
    Route::post('delete/{id}', 'TransaksiTiketController@delete')->name('transaksi_tiket.delete');
  });
  
  Route::prefix('laporan')->group(function(){
    Route::get('realisasi', 'LaporanController@realisasi')->name('laporan.realisasi'); 
    Route::get('perolehan', 'LaporanController@perolehan')->name('laporan.perolehan'); 
    Route::get('berita-acara', 'LaporanController@beritaAcara')->name('laporan.berita_acara');     
  });
  
  Route::prefix('master-data')->group(function(){
    Route::prefix('kph')->group(function(){
      Route::get('', 'KphController@index')->name('kph.index');
      Route::get('ax', 'KphController@axIndex')->name('kph.axIndex');
      Route::post('', 'KphController@store')->name('kph.store');
      Route::post('update/{id}', 'KphController@update')->name('kph.update');
      Route::post('delete/{id}', 'KphController@delete')->name('kph.delete');    
    });
  
    Route::prefix('pengguna')->group(function(){
      Route::get('', 'UserController@index')->name('user.index');
      Route::get('tambah', 'UserController@add')->name('user.add');
      Route::get('edit/{id}', 'UserController@edit')->name('user.edit');
      Route::get('ax', 'UserController@axIndex')->name('user.axIndex');
      Route::post('', 'UserController@store')->name('user.store');
      Route::post('update/{id}', 'UserController@update')->name('user.update');
      Route::post('delete/{id}', 'UserController@delete')->name('user.delete');    
    });
  
    Route::prefix('jenis-tiket')->group(function(){
      Route::get('', 'JenisTiketController@index')->name('jenis_tiket.index');
      Route::post('', 'JenisTiketController@store')->name('jenis_tiket.store');
      Route::post('update/{id}', 'JenisTiketController@update')->name('jenis_tiket.update');
      Route::post('delete/{id}', 'JenisTiketController@delete')->name('jenis_tiket.delete');    
    });
  
    Route::prefix('lokasi')->group(function(){
      Route::get('', 'LokasiController@index')->name('lokasi.index');
      Route::get('edit/{id}', 'LokasiController@edit')->name('lokasi.edit');
      Route::get('ax', 'LokasiController@axIndex')->name('lokasi.axIndex');
      Route::get('detail/{id}', 'LokasiController@detail')->name('lokasi.detail');
      Route::post('', 'LokasiController@store')->name('lokasi.store');
      Route::post('update/{id}', 'LokasiController@update')->name('lokasi.update');
      Route::post('delete/{id}', 'LokasiController@delete')->name('lokasi.delete');    
    });
  
    Route::prefix('loket')->group(function(){
      Route::get('', 'LoketController@index')->name('loket.index');
      Route::get('detail/{id}', 'LoketController@detail')->name('loket.detail');
      Route::get('edit/{id}', 'LoketController@edit')->name('loket.edit');
      Route::get('ax', 'LoketController@axIndex')->name('loket.axIndex');
      Route::post('', 'LoketController@store')->name('loket.store');
      Route::post('update/{id}', 'LoketController@update')->name('loket.update');
      Route::post('delete/{id}', 'LoketController@delete')->name('loket.delete');    
    });
  
    Route::prefix('role')->group(function(){
      Route::get('', 'RoleController@index')->name('role.index');
      Route::get('tambah', 'RoleController@add')->name('role.add');
      Route::get('edit/{id}', 'RoleController@edit')->name('role.edit');
      Route::post('', 'RoleController@store')->name('role.store');
      Route::post('update/{id}', 'RoleController@update')->name('role.update');
      Route::post('delete/{id}', 'RoleController@delete')->name('role.delete');    
    });
  
    Route::prefix('tiket')->group(function(){
      Route::get('', 'TiketWisataController@index')->name('tiket.index');
      Route::get('ax', 'TiketWisataController@axIndex')->name('tiket.axIndex');
      Route::get('tambah', 'TiketWisataController@add')->name('tiket.add');
      Route::get('detail/{id}', 'TiketWisataController@detail')->name('tiket.detail');
      Route::get('edit/{id}', 'TiketWisataController@edit')->name('tiket.edit');
      Route::post('', 'TiketWisataController@store')->name('tiket.store');
      Route::post('update/{id}', 'TiketWisataController@update')->name('tiket.update');
      Route::post('delete/{id}', 'TiketWisataController@delete')->name('tiket.delete');    
    });
  
    Route::prefix('target-perolehan')->group(function(){
      Route::get('', 'TargetPerolehanController@index')->name('target_perolehan.index');
      Route::post('', 'TargetPerolehanController@store')->name('target_perolehan.store');
      Route::post('update/{id}', 'TargetPerolehanController@update')->name('target_perolehan.update');
      Route::post('delete/{id}', 'TargetPerolehanController@delete')->name('target_perolehan.delete');    
    });
  
    Route::prefix('biaya-operasional')->group(function(){
      Route::get('', 'BiayaOperasionalController@index')->name('biaya_operasional.index');
      Route::post('', 'BiayaOperasionalController@store')->name('biaya_operasional.store');
      Route::post('update/{id}', 'BiayaOperasionalController@update')->name('biaya_operasional.update');
      Route::post('delete/{id}', 'BiayaOperasionalController@delete')->name('biaya_operasional.delete');    
    });
  
    Route::prefix('mitra')->group(function(){
      Route::get('', 'MitraController@index')->name('mitra.index');
      Route::get('ax', 'MitraController@axIndex')->name('mitra.axIndex');
      Route::post('', 'MitraController@store')->name('mitra.store');
      Route::post('update/{id}', 'MitraController@update')->name('mitra.update');
      Route::post('delete/{id}', 'MitraController@delete')->name('mitra.delete');    
    });
  });
});


