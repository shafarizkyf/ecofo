<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'LoginController@apiLogin');
Route::post('transaksi', 'TransaksiTiketController@apiStore');
Route::post('delete-transaksi', 'TransaksiTiketController@apiDeleteTransaction');
Route::get('transaksi', 'TransaksiTiketController@apiGetTransaction');
Route::get('user', 'UserController@apiPetugasTiket');
