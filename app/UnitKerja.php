<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class UnitKerja extends Model{
    
  public function scopeActive($query){
    return $query->where('is_active', true);
  }
  
  public function scopeInMyOrganization($query){
    $auth = Auth::user();
    return strtolower($auth->level->nama) == 'pusat' ? $query : $query->where('id', $auth->kph->unit_kerja_id);
  }

}
