<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLoket extends Model{
    
    public $timestamps = false;

    public function loket(){
        return $this->belongsTo('App\Loket', 'loket_id');
    }

    public function tiketWisata(){
        return $this->hasMany('App\TiketWisata', 'loket_id', 'loket_id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
