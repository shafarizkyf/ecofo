<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendapatanBagiHasil extends Model{

  protected $table = 'pendapatan_bagi_hasil';

  public function tiketWisata(){
    return $this->belongsTo('App\TiketWisata');
  }

}
