<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Level extends Model{
  
  
  public function scopeInMyOrganization($query){
    $auth = Auth::user();
    
    if($auth->level->nama == 'KPH'){
      return $query->whereIn('nama', ['KPH', 'Loket']);
    }

    if($auth->level->nama == 'Regional'){
      return $query->whereIn('nama', ['Regional', 'KPH']);
    }
    
    if($auth->level->nama == 'Pusat'){
      return $query->whereIn('nama', ['Pusat', 'Regional']);      
    }
  }
  
}
