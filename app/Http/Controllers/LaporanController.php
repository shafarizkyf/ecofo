<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use PDF;
use App\BiayaPengurang;
use App\PendapatanBagiHasi;
use App\Lokasi;
use App\Kph;
use App\PendapatanBagiHasil;
use App\TargetPerolehan;
use App\TiketWisata;
use App\TransaksiTiket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LaporanController extends Controller{
  
  public function perolehan(Request $request){
    Common::hasPermission();

    $bulan = null;
    $tahun = null;
    $laporan = false;
    $lokasi = Lokasi::active()->inMyOrganization()->get();
    $date = Carbon::parse("{$bulan}-01");
    $periode = $request->periode;

    if($request->bulan && $request->kph){
      $laporan = true;
      $bulan = date('Y-m', strtotime($request->bulan));
      $tahun = date('Y', strtotime($request->bulan));
      $lokasi = Lokasi::active()->whereKph($request->kph)->get();

      if(!$lokasi->count()){
        return redirect()->back()->with('danger', 'Belum Ada Transaksi');
      }
    }

    if($laporan && $request->laporan == 'pendapatan'){
      $targetPendapatan = TargetPerolehan::active()->kph($request->kph)->where('tahun', $tahun)->sum('pendapatan');

      $filename = 'pendapatan_'.$bulan.'.pdf';
      PDF::loadView('laporan.pendapatan_pdf', compact('lokasi', 'targetPendapatan', 'bulan', 'tahun', 'date', 'periode'))
      ->setPaper('legal', 'landscape')
      ->save("report/{$filename}");

      return response()->file("report/{$filename}");
    }

    if($laporan && $request->laporan == 'pengunjung'){
      $targetPengunjung = TargetPerolehan::active()->kph($request->kph)->where('tahun', $tahun);
      
      $filename = 'pengunjung_'.$bulan.'.pdf';
      PDF::loadView('laporan.pengunjung_pdf', compact('lokasi', 'bulan', 'tahun', 'targetPengunjung', 'date', 'periode'))
      ->setPaper('legal', 'landscape')
      ->save("report/{$filename}");
      
      return response()->file("report/{$filename}");
    }

    $kph = Kph::active()->inMyOrganization()->get();
    return view('laporan.pendapatan', compact('kph'));

  }

  public function beritaAcara(Request $request){
    Common::hasPermission();

    $tiketWisata = TiketWisata::find($request->id);
    $tiketByLokasi = TiketWisata::where('lokasi_id', $tiketWisata->lokasi_id)->get();

    $transaksi = TransaksiTiket::where('tiket_wisata_id', $tiketWisata->id)
    ->select(DB::raw('*, Date(created_at) as created_at, min(nomor_karcis) as min, max(nomor_karcis) as max, count(created_at) as total'))
    ->startDate($request->tanggal_dari)
    ->endDate($request->tanggal_sampai)
    ->groupBy(DB::raw('Date(created_at), tiket_wisata_id '))
    ->get();

    if(!count($transaksi)){
      return redirect()->back()->with('danger', 'Data Tidak Ditemukan');
    }

    $tanggal = [
      'dari' => Carbon::parse($request->tanggal_dari),
      'sampai' => Carbon::parse($request->tanggal_sampai),
    ];

    $biayaKomponen = BiayaPengurang::select(DB::raw('*, sum(total_biaya_operasional) as total'))
    ->where('lokasi_id', $tiketWisata->lokasi_id)
    ->groupBy('biaya_operasional_id')
    ->get();

    $bagiHasil = PendapatanBagiHasil::select(DB::raw('
      sum(bagi_hasil_mitra) as bagi_hasil_mitra, 
      sum(bagi_hasil_perhutani) as bagi_hasil_perhutani,
      sum(bagi_hasil_mitra) + sum(bagi_hasil_perhutani) as total'))
    ->where('lokasi_id', $tiketWisata->lokasi_id)
    ->first();

    $filename = 'berita_acara_'.$tiketWisata->lokasi->nama.'.pdf';
    PDF::loadView('laporan.berita_acara_pdf', compact('tiketWisata', 'transaksi', 'tiketByLokasi', 'tanggal', 'biayaKomponen', 'bagiHasil'))
    ->setPaper('a4', 'potrait')
    ->save("report/{$filename}");

    return response()->file("report/{$filename}");
  }

  public function realisasi(){
    Common::hasPermission();

    $targetPerolehan = TargetPerolehan::where('tahun', date('Y'))->get();
    return view('laporan.realisasi', compact('targetPerolehan'));
  }

}
