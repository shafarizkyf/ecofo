<?php

namespace App\Http\Controllers;

use App\BagiHasil;
use App\Mitra;
use App\TiketWisata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BagiHasilController extends Controller{
  
  public function index(){
    return view('bagi_hasil.index');
  }
  
  public function store(Request $request){
    DB::transaction(function() use ($request){

      $mitra = Mitra::find($request->id_mitra);
      $tiketWisata = TiketWisata::find($request->id_tiket_wisata);

      $bagiHasil = new BagiHasil();
      $bagiHasil->bagi_mitra = $request->bagi_mitra;
      $bagiHasil->bagi_perhutani = $request->bagi_perhutani;
      $bagiHasil->tiketWisata()->associate($tiketWisata);
      $bagiHasil->mitra()->associate($mitra);
      $bagiHasil->save();
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    DB::transaction(function() use ($request, $id){
      $mitra = Mitra::find($request->id_mitra);
      $tiketWisata = TiketWisata::find($request->id_tiket_wisata);

      $bagiHasil = BagiHasil::find($id);
      $bagiHasil->bagi_mitra = $request->bagi_mitra;
      $bagiHasil->bagi_perhutani = $request->bagi_perhutani;
      $bagiHasil->tiketWisata()->associate($tiketWisata);
      $bagiHasil->mitra()->associate($mitra);
      $bagiHasil->save();
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function delete($id){
    DB::transaction(function() use ($request, $id){
      $bagiHasil = BagiHasil::find($id);
      $bagiHasil->is_active = false;
      $bagiHasil->save();
    });

    return redirect()->back()->with('success', 'Berhasil menghapus data');
  }  
    
}
