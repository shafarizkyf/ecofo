<?php

namespace App\Http\Controllers;

use App\BiayaOperasional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BiayaOperasionalController extends Controller{
  
  public function index(){
    $biayaOperasional = BiayaOperasional::active()->get();
    return view('biaya_operasional.index', compact('biayaOperasional'));
  }
  
  public function store(Request $request){
    DB::transaction(function() use ($request){
      $biayaOperasional = new BiayaOperasional();
      $biayaOperasional->nama = $request->nama;
      $biayaOperasional->save();
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    DB::transaction(function() use ($request, $id){
      $biayaOperasional = BiayaOperasional::find($id);
      $biayaOperasional->nama = $request->nama;
      $biayaOperasional->save();
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function delete($id){

    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      $biayaOperasional = BiayaOperasional::find($id);
      $biayaOperasional->is_active = false;
      $biayaOperasional->save();
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }  
  
}
