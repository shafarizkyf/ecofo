<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use App\Kph;
use App\Mitra;
use App\UnitKerja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MitraController extends Controller{
    
  public function index(){
    Common::hasPermission();

    $unitKerja = UnitKerja::active()->inMyOrganization()->get();
    $mitra = Mitra::active()->inMyOrganization()->get();
    return view('mitra.index', compact('mitra', 'unitKerja'));
  }
  
  public function store(Request $request){
    Common::hasPermission();

    DB::transaction(function() use ($request){
      $kph = Kph::find($request->kph);

      $mitra = new Mitra();
      $mitra->nama = $request->nama;
      $mitra->is_pemda = $request->pemda ? true : false;
      $mitra->kontak = $request->kontak;
      $mitra->ketua = $request->ketua;
      $mitra->jabatan = $request->jabatan;
      $mitra->kph()->associate($kph);
      $mitra->save();
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    Common::hasPermission();

    DB::transaction(function() use ($request, $id){
      $kph = Kph::find($request->kph);

      $mitra = Mitra::find($id);
      $mitra->nama = $request->nama;
      $mitra->is_pemda = $request->pemda ? true : false;
      $mitra->kontak = $request->kontak;
      $mitra->ketua = $request->ketua;
      $mitra->jabatan = $request->jabatan;
      $mitra->kph()->associate($kph);
      $mitra->save();
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function delete($id){
    Common::hasPermission();

    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      $mitra = Mitra::find($id);
      $mitra->is_active = false;
      $mitra->save();
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }  
  
  public function axIndex(Request $request){
    $mitra = Mitra::active();

    if($request->id_kph){
      $mitra = $mitra->where('kph_id', $request->id_kph);
    }

    if($request->id_lokasi){
      $mitra = $mitra->whereHas('mitraLokasi', function($q) use ($request){
        $q->where('lokasi_id', $request->id_lokasi);
      });
    }

    return $mitra->get(); 
  }  
}
