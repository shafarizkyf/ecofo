<?php

namespace App\Http\Controllers;

use App\TiketWisata;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller{
  
  public function index(){
    return view('login.index');
  }
  
  public function login(Request $request){

    $user = User::active()->where('username', $request->username)->first();
    
    if(!$user){
      return redirect()->back()->with('danger', 'Username tidak terdaftar');
    }

    if(!Auth::attempt($request->only('username', 'password'))){
      return redirect()->back()->with('danger', 'Username atau Password salah');
    }

    $redirect = $user->level->nama == 'Loket' ? 'transaksi_tiket.index' : 'dashboard.index';
    return redirect(route($redirect));
  }

  public function logout(){
    Auth::logout();
    return redirect(route('login'));
  }


  public function apiLogin(Request $request){
    
    $user = User::active()->where('username', $request->username)->whereHas('level', function($q){
      $q->where('nama', 'Loket');
    })->first();
    
    if(!$user){
      return $response = ['success'=>true, 'login'=>false, 'msg'=>'Username tidak terdaftar dalam sistem'];
    }

    if(!Auth::attempt(['username'=>$request->username, 'password'=>$request->password])){
      return $response = ['success'=>true, 'login'=>false, 'msg'=>'Username/Password salah'];
    }

    $idLoket =  $user->userLoket ?  $user->userLoket->id : null;
    $idLokasi =  $idLoket ?  $user->userLoket->loket->lokasi->nama : null;

    if(!$idLokasi || !$idLoket){
      return $response = ['success'=>true, 'login'=>false, 'msg'=>'Username ditemukan namun Admin belum menentukan lokasi loket Anda'];
    }

    $jenisWaktu = ['all'];
    Carbon::now()->isWeekday() ? array_push($jenisWaktu, 'weekday') : array_push($jenisWaktu, 'weekend');  
    
    $tiket = TiketWisata::whereHas('loket', function($q) use ($user){
      $q->where('user_id', $user->id);
    })->get(); //->whereIn('jenis_waktu', $jenisWaktu)->get();
    
    // $dataTiket = array();
    // foreach($tiket  as $o){
    //   $dataTiket[] = [
    //     'id' => $o->id,
    //     'jenis_tiket' => $o->jenisTiket->nama,
    //     'loket' => $o->loket->nama,
    //     'lokasi' => $o->lokasi->nama,
    //     'harga' => $o->harga,
    //     'waktu' => $o->jenis_waktu,
    //     'asal' => $o->jenisTiket->jenis
    //   ];
    // }

    $dataTiket = array();
    foreach($user->userLoket->tiketWisata  as $o){
      $dataTiket[] = [
        'id' => $o->id,
        'jenis_tiket' => $o->jenisTiket->nama,
        'loket' => $o->loket->nama,
        'lokasi' => $o->lokasi->nama,
        'harga' => $o->harga,
        'waktu' => $o->jenis_waktu,
        'asal' => $o->jenisTiket->jenis
      ];
    }

    $data = [
      'id' => $user->id,
      'username' => $user->username,
      'nama'=> $user->nama,
      'email' => $user->email,
      'foto' => $user->foto,
      'telp' => $user->telp,
      'id_lokasi' => $idLokasi,
      'lokasi' => $user->userLoket->loket->lokasi->nama,
      'kph' => $user->kph->nama,
      'tiket' => $dataTiket,
    ];

    return $response = [
      'success' => true, 
      'login' => true, 
      'msg' => 'Berhasil login',
      'user' => $data
    ];
  }

}
