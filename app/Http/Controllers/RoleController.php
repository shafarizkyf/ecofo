<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use App\Modul;
use App\Permission;
use App\Role;
use App\RolePermission;
use App\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class RoleController extends Controller{
  
  public function index(){
    Common::hasPermission();

    $role = Role::all();
    return view('role.index', compact('role'));
  }

  public function add(){
    Common::hasPermission();
    
    $modul = Modul::all();
    return view('role.add', compact('modul'));
  }

  public function store(Request $request){
    Common::hasPermission();
    
    if(!$request->permission){
      return redirect()->back()->with('danger', 'Tidak ada permission yang diberikan');
    }

    DB::transaction(function() use ($request){

      $role = new Role;
      $role->nama = $request->nama;
      $role->save();

      $permissions = array_keys($request->permission);
      foreach($permissions as $o){
        $permission = Permission::find($o);
        $rolePermission = new RolePermission;
        $rolePermission->permission()->associate($permission);
        $rolePermission->role()->associate($role);
        $rolePermission->save();
      }

    });

    return redirect()->back()->with('success', 'Berhasil menambah data role');
  }

  public function edit($id){
    Common::hasPermission();

    $role = Role::find($id);
    $modul = Modul::all();
    return view('role.edit', compact('modul', 'role'));
  }

  public function update(Request $request, $id){
    Common::hasPermission();
    
    if(!$request->permission){
      return redirect()->back()->with('danger', 'Tidak ada permission yang diberikan');
    }

    DB::transaction(function() use ($request, $id){
      $role = Role::find($id);
      $role->nama = $request->nama;
      $role->save();

      RolePermission::where('role_id', $id)->delete();
      $permissions = array_keys($request->permission);
      
      foreach($permissions as $o){
        $permission = Permission::find($o);
        $rolePermission = new RolePermission;
        $rolePermission->permission()->associate($permission);
        $rolePermission->role()->associate($role);
        $rolePermission->save();
      }
    });
    
    return redirect()->back()->with('success', 'Berhasil memperbarui data role');
  }

  public function delete($id){
    Common::hasPermission();
    
    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      Role::find($id)->delete();
      RolePermission::where('role_id', $id)->delete();
      UserRole::where('role_id', $id)->delete();
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }

}
