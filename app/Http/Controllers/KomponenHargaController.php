<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use App\BiayaOperasional;
use App\KomponenHarga;
use App\TiketWisata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KomponenHargaController extends Controller{
  
  public function index(){
    Common::hasPermission();

    $komponenHarga = KomponenHarga::all();
    return view('jenis_tiket.index', compact('komponenHarga'));
  }
  
  public function store(Request $request){
    Common::hasPermission();

    DB::transaction(function() use ($request){

      $tiketWisata = TiketWisata::find($request->id_tiket_wisata);
      $biayaOperasional = BiayaOperasional::find($request->id_biaya_operasional);

      $komponenHarga = new KomponenHarga();
      $komponenHarga->biaya = $request->biaya;
      $komponenHarga->tiketWisata()->associate($tiketWisata);
      $komponenHarga->biayaOperasional()->associate($biayaOperasional);
      $komponenHarga->save();
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    Common::hasPermission();

    DB::transaction(function() use ($request, $id){
      $tiketWisata = TiketWisata::find($request->id_tiket_wisata);
      $biayaOperasional = BiayaOperasional::find($request->id_biaya_operasional);
      
      $komponenHarga = KomponenHarga::find($id);
      $komponenHarga->biaya = $request->biaya;
      $komponenHarga->tiketWisata()->associate($tiketWisata);
      $komponenHarga->biayaOperasional()->associate($biayaOperasional);
      $komponenHarga->save();
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function delete($id){
    Common::hasPermission();
    
    DB::transaction(function() use ($request, $id){
      $komponenHarga = KomponenHarga::find($id);
      $komponenHarga->is_active = false;
      $komponenHarga->save();
    });

    return redirect()->back()->with('success', 'Berhasil menghapus data');
  }
  
}
