<?php

namespace App\Http\Controllers;

use App\Mitra;
use App\MitraLokasi;
use App\Lokasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MitraLokasiController extends Controller{
  
  public function index(){
    $mitra = MitraLokasi::all();
    return view('mitra_lokasi.index', compact('mitra'));
  }
  
  public function store(Request $request){
    DB::transaction(function() use ($request){

      $mitra = Mitra::find($request->id_lokasi);
      $lokasi = Lokasi::find($request->id_lokasi);

      $mitraLokasi = new MitraLokasi();
      $mitraLokasi->lokasi()->associate($lokasi);
      $mitraLokasi->mitra()->associate($mitra);
      $mitraLokasi->save();
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    DB::transaction(function() use ($request, $id){

      $mitra = Mitra::find($request->id_lokasi);
      $lokasi = Lokasi::find($request->id_lokasi);
      
      $mitraLokasi = MitraLokasi::find($id);
      $mitraLokasi->lokasi()->associate($lokasi);
      $mitraLokasi->mitra()->associate($mitra);
      $mitraLokasi->save();
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function delete($id){
    DB::transaction(function() use ($request, $id){
      $mitraLokasi = MitraLokasi::find($id);
      $mitraLokasi->is_active = false;
      $mitraLokasi->save();
    });

    return redirect()->back()->with('success', 'Berhasil menghapus data');
  }
  
  
}
