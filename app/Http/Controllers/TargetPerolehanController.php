<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use App\Kph;
use App\Lokasi;
use App\UnitKerja;
use App\TargetPerolehan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TargetPerolehanController extends Controller{
  
  public function index(){
    Common::hasPermission();

    $today = Carbon::now();
    $tahun = [$today->year, $today->addYear(1)->year];
    $unitKerja = UnitKerja::active()->inMyOrganization()->get();
    $targetPerolehan = TargetPerolehan::active()->inMyOrganization()->latest()->get();
    return view('target_perolehan.index', compact('targetPerolehan', 'tahun', 'unitKerja'));
  }
  
  public function store(Request $request){
    Common::hasPermission();

    DB::transaction(function() use ($request){
      $lokasi = Lokasi::find($request->lokasi);
      
      $targetPerolehan = new TargetPerolehan();
      $targetPerolehan->pendapatan = $request->pendapatan;
      $targetPerolehan->pengunjung_wna = $request->pengunjung_wna;
      $targetPerolehan->pengunjung_wni = $request->pengunjung_wni;
      $targetPerolehan->pengunjung = $request->pengunjung_wna + $request->pengunjung_wni;
      $targetPerolehan->tahun = $request->tahun;
      $targetPerolehan->lokasi()->associate($lokasi);
      $targetPerolehan->save();
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    Common::hasPermission();

    DB::transaction(function() use ($request, $id){
      $lokasi = Lokasi::find($request->lokasi);

      $targetPerolehan = TargetPerolehan::find($id);
      $targetPerolehan->pendapatan = $request->pendapatan;
      $targetPerolehan->pengunjung_wna = $request->pengunjung_wna;
      $targetPerolehan->pengunjung_wni = $request->pengunjung_wni;
      $targetPerolehan->pengunjung = $request->pengunjung_wna + $request->pengunjung_wni;
      $targetPerolehan->tahun = $request->tahun;
      $targetPerolehan->lokasi()->associate($lokasi);
      $targetPerolehan->save();
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function delete($id){
    Common::hasPermission();

    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      $targetPerolehan = TargetPerolehan::find($id);
      $targetPerolehan->is_active = false;
      $targetPerolehan->save();
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }  
  
}
