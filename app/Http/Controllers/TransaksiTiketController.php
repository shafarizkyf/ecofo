<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use App\JenisTiket;
use App\Kph;
use App\Lokasi;
use App\Loket;
use App\TiketWisata;
use App\TransaksiTiket;
use App\UnitKerja;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TransaksiTiketController extends Controller{
  
  public function index(Request $request){
    Common::hasPermission();
    
    $transaksi = TransaksiTiket::inMyOrganization();

    if(!isset($request->date) && (!isset($request->tanggal_dari) || !isset($request->tanggal_sampai))){
      $transaksi = $transaksi->whereDate('created_at', date('Y-m-d'));
    }

    if(isset($request->date)){
      $transaksi = $transaksi->date($request->date);
    }
    
    if(isset($request->tanggal_dari)){
      $transaksi = $transaksi->startDate($request->tanggal_dari);
    }

    if(isset($request->tanggal_sampai)){
      $transaksi = $transaksi->endDate($request->tanggal_sampai);
    }

    if(isset($request->lokasi)){
      $transaksi = $transaksi->lokasi($request->lokasi);
    }

    if(isset($request->jenis_tiket)){
      $transaksi = $transaksi->jenisTiket($request->jenis_tiket);
    }

    $kph = Kph::active()->inMyOrganization()->select('id')->get()->toArray();
    $lokasi = Lokasi::active()->inMyOrganization()->get();
    $jenisTiket = JenisTiket::active()->get();

    $transaksi = $transaksi->latest('id')->get();
    return view('transaksi_tiket.index', compact('transaksi', 'lokasi', 'jenisTiket'));
  }

  public function add(){
    Common::hasPermission();

    $unitKerja = UnitKerja::active()->inMyOrganization()->get();
    return view('transaksi_tiket.add', compact('unitKerja'));
  }

  public function store(Request $request){
    Common::hasPermission();
    
    DB::transaction(function() use ($request){
      foreach($request->data as $o){
        $tiket = TiketWisata::where('lokasi_id', $o['lokasi'])
        ->where('jenis_tiket_id', $o['jenis_tiket'])
        ->where('loket_id', $o['loket'])
        ->first();

        for($i=0; $i<$o['jumlah']; $i++){
          $transaksi = new TransaksiTiket();
          $transaksi->nomor_karcis = $this->generateKodeKarcis($tiket);
          $transaksi->negara_pengunjung = $tiket->jenisTiket->jenis;
          $transaksi->harga = $tiket->harga;
          $transaksi->tiketWisata()->associate($tiket);
          $transaksi->createdBy()->associate(Auth::user());
          $transaksi->save();
        }
      }
    });

    return redirect()->back()->with('success', 'Berhasil menambah data');
  }
  

  public function apiStore(Request $request){

    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($request, &$response){

      $user = User::find($request->id_user);
      $tiket = TiketWisata::find($request->id_tiket);
      
      for($i=0; $i<$request->jumlah; $i++){
        $transaksi = new TransaksiTiket();
        $transaksi->nomor_karcis = $this->generateKodeKarcis($tiket);
        $transaksi->negara_pengunjung = $tiket->jenisTiket->jenis;
        $transaksi->harga = $tiket->harga;

        if($request->created_at){
          $transaksi->created_at = $request->created_at;
        }

        $transaksi->tiketWisata()->associate($tiket);
        $transaksi->createdBy()->associate($user);
        $transaksi->save();
      }

      $response = ['success'=>true, 'msg'=>'Transaksi berhasil'];
    });

    return $response;
  }

  public function apiGetTransaction(Request $request){    
    $transaksi = TransaksiTiket::where('created_at', $request->date)
    ->where('created_by', $request->id_user)
    ->count();

    return ['total'=>$transaksi];
  }

  public function apiDeleteTransaction(Request $request){

    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($request, &$response){
      $transaksi = TransaksiTiket::where('created_at', $request->date)
      ->where('created_by', $request->id_user)
      ->delete();  
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }

  public function generateKodeKarcis($tiket){
    $nomor = '1';
    $latestTransaksi = TransaksiTiket::where('tiket_wisata_id', $tiket->id)->get()->last();

    if($latestTransaksi){
      $nomor = substr($latestTransaksi->nomor_karcis, -5, 5);
      $nomor = $nomor + 1;
    }

    $nomor = sprintf('%05d', $nomor);
    $lokasi = Lokasi::find($tiket->lokasi_id);
    return $lokasi->kode . '-' . date('dmy') . '-' . $nomor;
  }

  public function delete($id){
    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      TransaksiTiket::find($id)->delete();
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }

}
