<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use PDF;
use App\Kph;
use App\UnitKerja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class KphController extends Controller{
  
  public function index(){
    Common::hasPermission();

    $kph = Kph::active()->inMyOrganization()->get();
    $unitKerja = UnitKerja::active()->inMyOrganization()->get();

    return view('kph.index', compact('kph', 'unitKerja'));
  }
  
  public function store(Request $request){
    Common::hasPermission();

    DB::transaction(function() use ($request){
      $unitKerja = UnitKerja::find($request->unit_kerja);
      
      $kph = new Kph();
      $kph->nama = $request->nama;
      $kph->unitKerja()->associate($unitKerja);
      $kph->save();
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    Common::hasPermission();

    DB::transaction(function() use ($request, $id){
      $unitKerja = UnitKerja::find($request->unit_kerja);

      $kph = Kph::find($id);
      $kph->nama = $request->nama;
      $kph->unitKerja()->associate($unitKerja);
      $kph->save();
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function delete($id){
    Common::hasPermission();

    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      $kph = Kph::find($id);
      $kph->is_active = false;
      $kph->save();
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }


  public function axIndex(Request $request){
    
    $kph = Kph::active()->inMyOrganization();

    if($request->id_unit_kerja){
      $kph = $kph->where('unit_kerja_id', $request->id_unit_kerja);
    }

    return $kph->get(); 
  }

}
