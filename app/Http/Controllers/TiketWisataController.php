<?php

namespace App\Http\Controllers;

use App\MyClass\Common;
use App\BagiHasil;
use App\BiayaOperasional;
use App\JenisTiket;
use App\Kph;
use App\KomponenHarga;
use App\Lokasi;
use App\Loket;
use App\Mitra;
use App\TiketWisata;
use App\TransaksiTiket;
use App\UnitKerja;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TiketWisataController extends Controller{
  
  public function index(){
    Common::hasPermission();

    $tiketWisata = TiketWisata::inMyOrganization()->latest('id')->get();
    return view('tiket_wisata.index', compact('tiketWisata'));
  }
  
  public function add(){
    Common::hasPermission();

    $komponen = BiayaOperasional::active()->get();
    $jenisTiket = JenisTiket::active()->get();
    $unitKerja = UnitKerja::active()->inMyOrganization()->get();
    return view('tiket_wisata.add', compact('unitKerja', 'jenisTiket', 'komponen'));
  }

  public function edit($id){
    Common::hasPermission();

    $tiket = TiketWisata::find($id);
    if($tiket->transaksi->count()){
      $msg  = 'Tiket sudah memiliki data transaksi, jika ingin memperbarui data yang sudah memiliki transaksi, ';
      $msg .= 'silahkan disable tiket terkait dan buat tiket baru';
      return redirect()->back()->with('danger', $msg);
    }

    $komponen   = BiayaOperasional::active()->get();
    $jenisTiket = JenisTiket::active()->get();

    $unitKerja  = UnitKerja::active()->inMyOrganization()->get();
    $kph        = Kph::where('id', $tiket->lokasi->kph_id)->get();
    
    $lokasi     = Lokasi::where('kph_id', $tiket->lokasi->kph_id)->get();
    $loket      = Loket::where('lokasi_id', $tiket->lokasi_id)->get();
    $mitra      = Mitra::lokasi($tiket->lokasi_id)->get();

    return view('tiket_wisata.edit', compact(
      'tiket', 'unitKerja', 'jenisTiket', 'komponen', 'kph', 'lokasi', 'loket',
      'mitra'
    ));
  }

  public function detail($id){
    Common::hasPermission();

    $tiket = TiketWisata::find($id);
    $transaksi = TransaksiTiket::where('tiket_wisata_id', $id)
    ->select(DB::raw('Date(created_at) as created_at, min(nomor_karcis) as min, max(nomor_karcis) as max, count(created_at) as total'))
    ->groupBy(DB::raw('Date(created_at)'))
    ->get();

    return view('tiket_wisata.detail', compact('tiket', 'transaksi'));
  }

  public function store(Request $request){
    Common::hasPermission();

    DB::transaction(function() use ($request){

      $lokasi = Lokasi::find($request->lokasi);
      $loket = Loket::find($request->loket);
      $jenisTiket = JenisTiket::find($request->jenis_tiket);

      $tiketWisata = new TiketWisata();
      $tiketWisata->harga = $request->harga;
      $tiketWisata->kode_karcis = '001';
      $tiketWisata->jenis_waktu = $request->jenis_waktu;
      $tiketWisata->lokasi()->associate($lokasi);
      $tiketWisata->loket()->associate($loket);
      $tiketWisata->jenisTiket()->associate($jenisTiket);
      $tiketWisata->save();

      $totalBiayaKomponen = 0;
      foreach($request->komponen as $o){
        $biayaOperasional = BiayaOperasional::find($o['operasional']);
        $totalBiayaKomponen += $o['harga'];

        $komponen = new KomponenHarga();
        $komponen->biaya = $o['harga'];
        $komponen->biayaOperasional()->associate($biayaOperasional);
        $komponen->tiketWisata()->associate($tiketWisata);
        $komponen->save();
      }

      $hargaTiketBersih = $request->harga - $totalBiayaKomponen;
      foreach($request->bagi as $o){
        $mitra = Mitra::find($o['id_mitra']);
        $operator = $o['operasi'];

        $bagiPerhutani = $o['perhutani'];
        $bagiMitra = $o['mitra'];

        if($operator == 'persen'){
          $bagiMitra = $hargaTiketBersih * $o['mitra'] / 100;
          $bagiPerhutani = $hargaTiketBersih * $o['perhutani'] / 100;
        }
        
        $bagiHasil = new BagiHasil();
        $bagiHasil->bagi_perhutani = $bagiPerhutani;
        $bagiHasil->bagi_mitra = $bagiMitra;
        $bagiHasil->mitra()->associate($mitra);
        $bagiHasil->tiketWisata()->associate($tiketWisata);
        $bagiHasil->save();
      }

    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    Common::hasPermission();

    DB::transaction(function() use ($request, $id){

      $loket = Loket::find($request->loket);
      $jenisTiket = JenisTiket::find($request->jenis_tiket);

      $tiketWisata = TiketWisata::find($id);
      $tiketWisata->harga = $request->harga;
      $tiketWisata->jenis_waktu = $request->jenis_waktu;
      $tiketWisata->kode_karcis = '001';
      $tiketWisata->loket()->associate($loket);
      $tiketWisata->jenisTiket()->associate($jenisTiket);
      $tiketWisata->save();


      $totalBiayaKomponen = 0;
      KomponenHarga::where('tiket_wisata_id', $id)->delete();
      foreach($request->komponen as $o){
        $biayaOperasional = BiayaOperasional::find($o['operasional']);
        $totalBiayaKomponen += $o['harga'];

        $komponen = new KomponenHarga();
        $komponen->biaya = $o['harga'];
        $komponen->biayaOperasional()->associate($biayaOperasional);
        $komponen->tiketWisata()->associate($tiketWisata);
        $komponen->save();
      }
      
      $hargaTiketBersih = $request->harga - $totalBiayaKomponen;
      BagiHasil::where('tiket_wisata_id', $id)->delete();
      foreach($request->bagi as $o){
        $mitra = Mitra::find($o['id_mitra']);
        $operator = $o['operasi'];

        $bagiPerhutani = $o['perhutani'];
        $bagiMitra = $o['mitra'];

        if($operator == 'persen'){
          $bagiMitra = $hargaTiketBersih * $o['mitra'] / 100;
          $bagiPerhutani = $hargaTiketBersih * $o['perhutani'] / 100;
        }
        
        $bagiHasil = new BagiHasil();
        $bagiHasil->bagi_perhutani = $bagiPerhutani;
        $bagiHasil->bagi_mitra = $bagiMitra;
        $bagiHasil->mitra()->associate($mitra);
        $bagiHasil->tiketWisata()->associate($tiketWisata);
        $bagiHasil->save();
      }

    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function delete($id){
    Common::hasPermission();

    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      $tiketWisata = TiketWisata::find($id);

      if($tiketWisata->transaksi->count()){
        $tiketWisata->is_active = false;
        $tiketWisata->save();
        $response = ['success'=>true, 'msg'=>'Data berhasil dimatikan'];

      }else{
        $tiketWisata->delete();
        $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
      }

    });

    return $response;
  }
  
  public function axIndex(Request $request){

    $waktu = Carbon::now('Asia/Jakarta')->isWeekend() ? 'weekend' : 'weekday';

    $tiket = TiketWisata::active()->where('jenis_waktu', $waktu);

    if($request->id_kph){
      $tiket = $tiket->whereHas('lokasi', function($q) use ($request){
        $q->where('kph_id', $request->id_kph);
      });
    }

    if($request->id_lokasi){
      $tiket = $tiket->where('lokasi_id', $request->id_lokasi);
    }

    if($request->id_jenis_tiket){
      $tiket = $tiket->where('jenis_tiket_id', $request->id_jenis_tiket);
    }

    if($request->id_loket){
      $tiket = $tiket->where('loket_id', $request->id_loket);
    }

    $dataList = array();
    foreach($tiket->get() as $o){
      $dataList[] = [
        'id' => $o->id,
        'id_lokasi' => $o->lokasi_id,
        'id_jenis_tiket' => $o->jenis_tiket_id,
        'id_loket' => $o->loket_id,
        'lokasi' => $o->lokasi->nama,
        'jenis_tiket' => $o->jenisTiket->nama,
        'loket' => $o->loket->nama,
      ];
    }

    return $dataList;
  }
  
}
