<?php

namespace App\Http\Controllers;

use App\Kph;
use App\UnitKerja;
use App\TransaksiTiket;
use App\TransaksiByLokasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller{
  public function index(){

    $tahun = TransaksiTiket::select(DB::raw("Year(created_at) as tahun"))
    ->groupBy(DB::raw('Year(created_at)'))
    ->get();
    
    $unitKerja = UnitKerja::active()
    ->inMyOrganization()
    ->get();

    return view('dashboard.index', compact('tahun', 'unitKerja', 'kph'));
  }


  public function axIndex(Request $request){

    $tahun = $request->tahun ? $request->tahun: date('Y');
    $kph = Auth::user()->kph_list_id;

    if($request->id_kph){
      $kph = [$request->id_kph];
    }

    $transaksi = TransaksiByLokasi::select(DB::raw('sum(pendapatan) as pendapatan, sum(total) as total, MONTHNAME(created_at) as bulan'))
    ->whereHas('lokasi', function($q) use ($kph){
      $q->whereIn('kph_id', $kph);
    })->groupBy(DB::raw('Month(created_at)'))
    ->whereYear('created_at', $tahun);

    if(is_numeric($request->id_lokasi)){
      $transaksi = $transaksi->where('lokasi_id', $request->id_lokasi)->get();  
    }else{
      $transaksi = $transaksi->get();
    }

    $topLokasiPendapatan = TransaksiByLokasi::select(DB::raw('*, sum(harga) as harga'))
    ->whereHas('lokasi', function($q) use ($kph){
      $q->whereIn('kph_id', $kph);
    })->groupBy(DB::raw('lokasi_id'))
    ->whereYear('created_at', $tahun)
    ->orderBy('harga', 'desc');

    if(is_numeric($request->id_lokasi)){
      $topLokasiPendapatan = $topLokasiPendapatan->where('lokasi_id', $request->id_lokasi)->get();  
    }else{
      $topLokasiPendapatan = $topLokasiPendapatan->get();
    }

    $topLokasiPengunjung = TransaksiByLokasi::select(DB::raw('*, sum(total) as total'))
    ->whereHas('lokasi', function($q) use ($kph){
      $q->whereIn('kph_id', $kph);
    })->groupBy(DB::raw('lokasi_id'))
    ->whereYear('created_at', $tahun)
    ->orderBy('total', 'desc');

    if(is_numeric($request->id_lokasi)){
      $topLokasiPengunjung = $topLokasiPengunjung->where('lokasi_id', $request->id_lokasi)->get();  
    }else{
      $topLokasiPengunjung = $topLokasiPengunjung->get();
    }

    $todayRevenue = TransaksiByLokasi::select(DB::raw('sum(total) as pengunjung, sum(pendapatan) as pendapatan'))
    ->whereHas('lokasi', function($q) use($kph){
      $q->whereIn('kph_id', $kph);
    })->whereDate('created_at', '>=', date('Y-m-d'));

    if(is_numeric($request->id_lokasi)){
      $todayRevenue = $todayRevenue->where('lokasi_id', $request->id_lokasi)->first();  
    }else{
      $todayRevenue = $todayRevenue->first();
    }
    
    if($todayRevenue){
      $todayRevenue = [
        'pengunjung' => number_format($todayRevenue->pengunjung),
        'pendapatan' => number_format($todayRevenue->pendapatan),
      ];
    }

    return [
      'transaksi' => $transaksi, 
      'top_pendapatan' => $topLokasiPendapatan, 
      'top_pengunjung' => $topLokasiPengunjung,
      'today_perolehan' => $todayRevenue
    ];
  }

}
