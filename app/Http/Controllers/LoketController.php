<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use App\Lokasi;
use App\Loket;
use App\User;
use App\Kph;
use App\UserLoket;
use App\UnitKerja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoketController extends Controller{
  
  public function index(){
    Common::hasPermission();

    $unitKerja = UnitKerja::active()->inMyOrganization()->get();
    $lokasi = Lokasi::active()->inMyOrganization()->latest('id')->get();

    $loket = Loket::whereHas('userLoket')->inMyOrganization()->active()->latest('id')->get();
    // $loket = UserLoket::whereHas('loket', function($q) use ($lokasi){
    //   $q->active();
    //   $q->whereIn('lokasi_id', $lokasi->pluck('id')->toArray());
    // })->latest('id')->get();

    $user = User::active()->inMyOrganization()->where('level_id', 3)->get();
    return view('loket.index', compact('loket', 'unitKerja', 'user'));
  }
  
  public function store(Request $request){
    Common::hasPermission();

    // $user = User::find($request->petugas);
    // $userLoket = Loket::active()->where('user_id', $user->id)->first();
    // if($userLoket && $userLoket->lokasi_id != $request->lokasi){
    //   return redirect()->back()->with('danger', 'Petugas sudah ditempatkan di lokasi lain');
    // }

    DB::transaction(function() use ($request){
      $lokasi = Lokasi::find($request->lokasi);

      $loket = new Loket();
      $loket->nama = $request->nama;
      $loket->user_id = 1;
      $loket->lokasi()->associate($lokasi);
      $loket->save();
      
      foreach($request->petugas as $o){
        $user = User::find($o['id']);
        $userLoket = new UserLoket();
        $userLoket->user()->associate($user);
        $userLoket->loket()->associate($loket);
        $userLoket->save();
      }
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    Common::hasPermission();

    // $user = User::find($request->petugas);
    // $userLoket = Loket::active()->where('user_id', $user->id)->first();
    // if($userLoket && $userLoket->lokasi_id != $request->lokasi){
    //   return redirect()->back()->with('danger', 'Petugas sudah ditempatkan di lokasi lain');
    // }

    DB::transaction(function() use ($request, $id){
      $lokasi = Lokasi::find($request->lokasi);

      $loket = Loket::find($id);
      $loket->nama = $request->nama;
      $loket->user_id = 1;
      $loket->lokasi()->associate($lokasi);
      $loket->save();


      UserLoket::where('loket_id', $id)->delete();
      foreach($request->petugas as $o){
        $user = User::find($o['id']);
        $userLoket = new UserLoket();
        $userLoket->user()->associate($user);
        $userLoket->loket()->associate($loket);
        $userLoket->save();
      }      
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function detail(Request $request, $id){
    $loket = Loket::find($id);
    return view('loket.detail', compact('loket'));
  }

  public function edit(Request $request, $id){
    $loket = Loket::find($id);
    $unitKerja = UnitKerja::active()->inMyOrganization()->get();
    $lokasi = Lokasi::where('kph_id', $loket->lokasi->kph_id)->get();
    $kph = Kph::where('id', $loket->lokasi->kph_id)->get();
    $users = User::where('kph_id', $loket->lokasi->kph_id)->where('level_id', 3)->get();

    return view('loket.edit', compact('loket', 'lokasi', 'unitKerja', 'kph', 'users'));
  }

  public function delete($id){
    Common::hasPermission();

    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      $loket = Loket::find($id);
      $loket->is_active = false;
      $loket->save();
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }  
  
  public function axIndex(Request $request){
    $loket = Loket::active()->inMyOrganization();

    if($request->id_lokasi){
      $loket = $loket->where('lokasi_id', $request->id_lokasi);
    }

    return $loket->get(); 
  }  
}
