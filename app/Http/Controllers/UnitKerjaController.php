<?php

namespace App\Http\Controllers;

use App\UnitKerja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UnitKerjaController extends Controller{
  
  public function index(){
    $unitKerja = UnitKerja::all();
    return view('jenis_tiket.index', compact('unitKerja'));
  }
  
  public function store(Request $request){
    DB::transaction(function() use ($request){
      $unitKerja = new UnitKerja();
      $unitKerja->nama = $request->nama;
      $unitKerja->save();
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    DB::transaction(function() use ($request, $id){
      $unitKerja = UnitKerja::find($id);
      $unitKerja->nama = $request->nama;
      $unitKerja->save();
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function delete($id){
    DB::transaction(function() use ($request, $id){
      $unitKerja = UnitKerja::find($id);
      $unitKerja->is_active = false;
      $unitKerja->save();
    });

    return redirect()->back()->with('success', 'Berhasil menghapus data');
  }
  
}
