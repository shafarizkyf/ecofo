<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use App\Kph;
use App\Level;
use App\Role;
use App\UnitKerja;
use App\TiketWisata;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller{
  
  public function index(){
    Common::hasPermission();

    $user = User::active()->inMyOrganization()->get();
    return view('user.index', compact('user', 'unitKerja'));
  }
  
  public function add(){
    Common::hasPermission();
    
    $level = Level::inMyOrganization()->get();
    $unitKerja = UnitKerja::active()->inMyOrganization()->get();
    $role = Role::all();
    return view('user.add', compact('unitKerja', 'role', 'level'));
  }

  public function store(Request $request){
    Common::hasPermission();

    $request->validate([
      'nama' => 'required',
      'username' => 'required',
      'password' => 'required|min:6',
      'repassword' => 'required|same:password',
      'kph' => 'required',
      'level' => 'required',
      'data' => 'required',
      'data.*' => 'required' 
    ]);

    DB::transaction(function() use ($request){
      
      $kph = Kph::find($request->kph);
      $level = Level::find($request->level);

      $user = new User();
      $user->nama = $request->nama;
      $user->foto = Common::saveImage($request, 'foto', 'users');
      $user->username = $request->username;
      if($request->email) $user->email = $request->email;
      $user->password = Hash::make($request->password);
      $user->kph()->associate($kph);
      $user->level()->associate($level);
      $user->save();

      foreach($request->data as $o){
        $role = Role::find($o['role']);
        
        $userRole = new UserRole();
        $userRole->user()->associate($user);
        $userRole->role()->associate($role);
        $userRole->save();
      }

    });

    return redirect()->back()->with('success', 'Berhasil menambah user');
  }

  public function edit($id){
    Common::hasPermission();

    $user = User::find($id);
    $level = Level::all();
    $unitKerja = UnitKerja::active()->get();
    $role = Role::all();
    $kph = Kph::where('unit_kerja_id', $user->kph->unit_kerja_id)->get();
    return view('user.edit', compact('user', 'role', 'level', 'unitKerja', 'kph'));
  }

  public function update(Request $request, $id){
    Common::hasPermission();

    $request->validate([
      'nama' => 'required',
      'username' => 'required',
      'kph' => 'required',
      'level' => 'required',
      'data' => 'required',
      'data.*' => 'required' 
    ]);
    
    if($request->password){
      $request->validate([
        'password' => 'required|min:6',
        'repassword' => 'required|same:password',
      ]);
    }

    DB::transaction(function() use ($request, $id){
      
      $kph = Kph::find($request->kph);
      $level = Level::find($request->level);
      $user = User::find($id);

      $foto = Common::saveImage($request, 'foto', 'users');
      $foto = $foto ? $foto : $user->foto;

      $user->nama = $request->nama;
      $user->username = $request->username;
      $user->foto = $foto;
      if($request->email) $user->email = $request->email;
      if($request->password) $user->password = Hash::make($request->password);
      $user->kph()->associate($kph);
      $user->level()->associate($level);
      $user->save();

      UserRole::where('user_id', $id)->delete();
      foreach($request->data as $o){
        $role = Role::find($o['role']);
        
        $userRole = new UserRole();
        $userRole->user()->associate($user);
        $userRole->role()->associate($role);
        $userRole->save();
      }
    });

    return redirect()->back()->with('success', 'Data berhasil diperbarui');
  }

  public function delete($id){
    Common::hasPermission();

    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      $user = User::find($id);
      $user->is_active = false;
      $user->save();
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }  

  public function apiPetugasTiket(Request $request){
    $tiket = TiketWisata::whereHas('loket', function($q) use ($request){
      $q->where('user_id', $request->id_user);
    })->get();

    $dataTiket = array();
    foreach($tiket  as $o){
      $dataTiket[] = [
        'id' => $o->id,
        'jenis_tiket' => $o->jenisTiket->nama,
        'loket' => $o->loket->nama,
        'lokasi' => $o->lokasi->nama,
        'harga' => $o->harga
      ];
    }

    return $dataTiket;
  }

}
