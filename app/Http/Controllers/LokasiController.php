<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use App\UnitKerja;
use App\Kph;
use App\Lokasi;
use App\Mitra;
use App\MitraLokasi;
use App\TransaksiTiket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LokasiController extends Controller{
    
  public function index(){
    Common::hasPermission();

    $unitKerja = UnitKerja::active()->inMyOrganization()->get();
    $lokasi = Lokasi::active()->inMyOrganization()->latest('id')->get();
    return view('lokasi.index', compact('lokasi', 'unitKerja'));
  }
  
  public function edit($id){
    Common::hasPermission();

    $lokasi = Lokasi::find($id);
    $unitKerja = UnitKerja::active()->inMyOrganization()->get();
    $kph = Kph::where('id', $lokasi->kph_id)->get();
    $mitra = Mitra::where('kph_id', $lokasi->kph_id)->get();
    
    return view('lokasi.edit', compact('lokasi', 'unitKerja', 'kph', 'mitra'));
  }

  public function store(Request $request){
    Common::hasPermission();

    DB::transaction(function() use ($request){
      $kph = Kph::find($request->kph);
      
      $lokasi = new Lokasi();
      $lokasi->nama = $request->nama;
      $lokasi->kode = $request->kode;
      $lokasi->kph()->associate($kph);
      $lokasi->save();

      if($request->mitra){
        foreach($request->mitra as $o){
          $mitra = Mitra::find($o['id']);
          $mitraLokasi = new MitraLokasi();
          $mitraLokasi->mitra()->associate($mitra);
          $mitraLokasi->lokasi()->associate($lokasi);
          $mitraLokasi->save();
        }
      }
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    Common::hasPermission();

    DB::transaction(function() use ($request, $id){
      $kph = Kph::find($request->kph);

      $lokasi = Lokasi::find($id);
      $lokasi->nama = $request->nama;
      $lokasi->kode = $request->kode;
      $lokasi->kph()->associate($kph);
      $lokasi->save();

      MitraLokasi::where('lokasi_id', $id)->delete();
      foreach($request->mitra as $o){
        $mitra = Mitra::find($o['id']);
        $mitraLokasi = new MitraLokasi();
        $mitraLokasi->mitra()->associate($mitra);
        $mitraLokasi->lokasi()->associate($lokasi);
        $mitraLokasi->save();
      }      
    });

    return redirect(route('lokasi.index'))->with('success', 'Berhasil memperbarui data');
  }

  public function detail($id){
    Common::hasPermission();

    $lokasi = Lokasi::find($id);
    $transaksi = TransaksiTiket::whereHas('tiketWisata', function($q) use ($id){
      $q->where('lokasi_id', $id);
    })->select(DB::raw('*, count(id) as total'))
    ->groupBy(DB::raw('Date(created_at), tiket_wisata_id'))
    ->get();
    
    return view('lokasi.detail', compact('lokasi', 'transaksi'));
  }

  public function delete($id){
    Common::hasPermission();

    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      $lokasi = Lokasi::find($id);
      $lokasi->is_active = false;
      $lokasi->save();
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }  


  public function axIndex(Request $request){
    $lokasi = Lokasi::active()->inMyOrganization();
    // $lokasi = Lokasi::active()->whereHas('loket', function($q){
    //   $q->active();
    // })->inMyOrganization();

    if($request->id_kph){
      $lokasi = $lokasi->where('kph_id', $request->id_kph);
    }

    if($request->has_tiket){
      $lokasi = $lokasi->whereHas('tiket');
    }

    return $lokasi->get(); 
  }    
}
