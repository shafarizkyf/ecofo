<?php

namespace App\Http\Controllers;

use App\MyClass\Common;

use App\JenisTiket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JenisTiketController extends Controller{
  
  public function index(){
    Common::hasPermission();

    $jenisTiket = JenisTiket::active()->latest()->get();
    return view('jenis_tiket.index', compact('jenisTiket'));
  }
  
  public function store(Request $request){
    Common::hasPermission();

    DB::transaction(function() use ($request){
      $jenisTiket = new JenisTiket();
      $jenisTiket->nama = $request->nama;
      $jenisTiket->jenis = $request->jenis;
      $jenisTiket->save();
    });
    
    return redirect()->back()->with('success', 'Berhasil menambah data');
  }

  public function update(Request $request, $id){
    Common::hasPermission();

    DB::transaction(function() use ($request, $id){
      $jenisTiket = JenisTiket::find($id);
      $jenisTiket->nama = $request->nama;
      $jenisTiket->jenis = $request->jenis;
      $jenisTiket->save();
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui data');
  }

  public function delete($id){
    Common::hasPermission();
    
    $response = ['success'=>false, 'msg'=>'Internal server error'];
    DB::transaction(function() use ($id, &$response){
      $jenisTiket = JenisTiket::find($id);
      $jenisTiket->is_active = false;
      $jenisTiket->save();
      $response = ['success'=>true, 'msg'=>'Data berhasil dihapus'];
    });

    return $response;
  }
  
}
