<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class JenisTiket extends Model{
  
  protected static function boot(){
    parent::boot();

    static::addGlobalScope('orderByName', function(Builder $builder){
      $builder->orderBy('nama');
    });
    
  }

  public function tiketWisata(){
    return $this->hasMany('App\TiketWisata');
  }
  
  public function scopeActive($query){
    return $query->where('is_active', true);
  }  
  
}
