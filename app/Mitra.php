<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Mitra extends Model{
  
  protected static function boot(){
    parent::boot();

    static::addGlobalScope('orderByName', function(Builder $builder){
      $builder->orderBy('nama');
    });
  }

  public function getKontakAttribute($kontak){
    return $kontak ?? '-';
  }

  public function bagiHasil(){
    return $this->hasMany('App\BagiHasil');
  }

  public function getPemdaAttribute(){
    return $this->is_pemda ? 'Pemda' : 'Non Pemda';
  }

  public function kph(){
    return $this->belongsTo('App\Kph');
  }

  public function mitraLokasi(){
    return $this->hasMany('App\MitraLokasi');
  }
  
  public function scopeActive($query){
    return $query->where('is_active', true);
  }

  public function scopeInMyOrganization($query){
    return $query->whereIn('kph_id', Auth::user()->kph_list_id);
  }

  public function scopeLokasi($query, $idLokasi){
    return $query->whereHas('mitraLokasi', function($q) use ($idLokasi){
      $q->where('lokasi_id', $idLokasi);
    });
  }
}
