<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Loket extends Model{
  
  public function getNamaAttribute($nama){
    return strtoupper($nama);
  }

  public function lokasi(){
    return $this->belongsTo('App\Lokasi');
  }

  public function transaksiTiket(){
    return $this->hasMany('App\TransaksiTiket');
  }
  
  public function tiketWisata(){
    return $this->hasMany('App\TiketWisata', 'loket_id');
  }

  public function scopeActive($query){
    return $query->where('is_active', true);
  }  
  
  public function scopeInMyOrganization($query){
    $auth = Auth::user();

    if($auth->level->nama == 'Loket'){
      return $query->where('id', $auth->loket->id);

    }elseif($auth->level->nama == 'KPH'){
      return $query->whereHas('lokasi', function($q) use ($auth){
        $q->whereIn('kph_id', $auth->kph_list_id);
      });
    }
    
  }

  //ini salah
  public function user(){
    return $this->belongsTo('App\User');
  }

  public function userLoket(){
    return $this->hasMany('App\UserLoket', 'loket_id');
  }

}
