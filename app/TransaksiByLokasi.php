<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiByLokasi extends Model{

  protected $table = 'transaksi_by_lokasi';

  public function lokasi(){
    return $this->belongsTo('App\Lokasi');
  }

}
