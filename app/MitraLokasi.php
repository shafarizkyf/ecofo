<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MitraLokasi extends Model{
  
  public function lokasi(){
    return $this->belongsTo('App\Lokasi');
  }

  public function mitra(){
    return $this->belongsTo('App\Mitra');
  }
  
}
