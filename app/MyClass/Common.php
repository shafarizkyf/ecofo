<?php

namespace App\MyClass;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class Common {
  
  public static function fromExcelToLinux($time, $format = 'Y-m-d') {
    $linuxTime = ($time-25569)*86400;
    return date($format, $linuxTime);
  }
  
  public static function saveImage($request, $input, $folder){
    $file = $request->file($input);
    $filename = null;
    if($file){
      $filename = md5($file->getClientOriginalName()).'_'.time().'.'.$file->getClientOriginalExtension();
      $destinationPath = "uploads/{$folder}";

      if(!is_dir(public_path($destinationPath))){
        mkdir($destinationPath, 0777, true);
      }

      $file->move($destinationPath, $filename);
    }
    return $filename;
  }

  public static function hasPermission(){
    if(!Auth::check()){
      return false;
    }

    $currentRoute = Route::currentRouteName();

    $routeName = explode('.', $currentRoute);
    $routePage = $routeName[0];
    $routeMethod = $routeName[1];

    if($routeMethod == 'add'){
      $routeMethod = 'store';
    }elseif($routeMethod == 'edit'){
      $routeMethod = 'update';
    }

    $renamedCurrentRoute = "{$routePage}.{$routeMethod}";

    $permission = Auth::user()->permissions();
    $has = in_array($renamedCurrentRoute, $permission);

    if(!$has){
      abort(404);
    }
  }

}
