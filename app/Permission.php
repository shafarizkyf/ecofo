<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model{
  
  public function modul(){
    return $this->belongsTo('App\Modul');
  }
  
}
