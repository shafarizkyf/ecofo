<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable {

  use Notifiable;
  protected $fillable = [
    'name', 'email', 'password',
  ];
  
  protected $hidden = [
    'password', 'remember_token',
  ];

  protected static function boot(){
    parent::boot();

    static::addGlobalScope('orderByName', function(Builder $builder){
      $builder->orderBy('nama');
    });
  }

  public function getNamaAttribute($nama){
    return strtoupper($nama);
  }

  public function getFotoPathAttribute(){
    return $this->foto ? asset('uploads/users/'.$this->foto) : asset('placeholder.jpg');
  }

  public function getAuthLevelAttribute(){
    return Auth::check() ? Auth::user()->level->nama : null;
  }

  public function getKphListIdAttribute(){
    $auth = Auth::user();
    $kph = Kph::active();
    $level = strtolower($auth->level->nama);
    
    if($level == 'regional'){
      $kph = $kph->where('unit_kerja_id', $auth->kph->unit_kerja_id);
    }elseif($level == 'kph' || $level == 'loket'){
      $kph = $kph->where('id', $auth->kph_id);
    }

    return $kph->select('id')->get()->toArray();
  }

  public function hasPermission($routeName){
    return in_array($routeName, $this->permissions());
  }

  public function kph(){
    return $this->belongsTo('App\Kph');
  }

  public function level(){
    return $this->belongsTo('App\Level');
  }

  //ini salah
  public function loket(){
    return $this->hasOne('App\Loket')->where('is_active', true);
  }

  public function userLoket(){
    return $this->hasOne('App\UserLoket')->latest('id');
  }

  public function permissions(){
    $idRoles = $this->userRole()->select('role_id')->get()->toArray();
    $idPermissions = RolePermission::whereIn('role_id', $idRoles)->select('permission_id')->get()->toArray();
    $permissionsObj = Permission::whereIn('id', $idPermissions)->select('route')->get();
    $permissionsArr = array();
    foreach($permissionsObj as $o){
      array_push($permissionsArr, $o->route);
    }
    
    return $permissionsArr;
  }

  public function scopeActive($query){
    return $query->where('is_active', true);
  }  

  public function scopeInMyOrganization($query){
    return $query->whereIn('kph_id', Auth::user()->kph_list_id);
  }

  public function userRole(){
    return $this->hasMany('App\UserRole');
  }

}
