<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Kph extends Model{
  
  protected static function boot(){
    parent::boot();

    static::addGlobalScope('orderByName', function(Builder $builder){
      $builder->orderBy('nama');
    });
    
  }

  public function getNamaAttribute($nama){
    return strtoupper($nama);
  }
  
  public function unitKerja(){
    return $this->belongsTo('App\UnitKerja');
  }

  public function lokasi(){
    return $this->hasMany('App\Lokasi');
  }

  public function scopeActive($query){
    return $query->where('is_active', true);
  }
  
  public function scopeInMyOrganization($query){
    return $query->whereIn('id', Auth::user()->kph_list_id);
  }


}
