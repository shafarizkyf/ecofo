<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TransaksiTiket extends Model{
    
  public function bagiHasil(){
    return $this->hasMany('App\BagiHasil', 'tiket_wisata_id', 'tiket_wisata_id');
  }

  public function createdBy(){
    return $this->belongsTo('App\User', 'created_by');
  }

  public function tiketWisata(){
    return $this->belongsTo('App\TiketWisata');
  }
  
  public function getCreatedAtAttribute($date){
    return Carbon::parse($date)->addHours(7);
  }
  
  public function scopeDate($query, $date){
    return $query->startDate($date)->endDate($date);
  }

  public function scopeEndDate($query, $date){
    return $query->whereDate('created_at', '<=', $date);
  }

  public function scopeInMyOrganization($query){
    $auth =  Auth::user();

    if($auth->level->nama == 'Loket'){
      return $query->where('created_by', $auth->id);
    }else{
      return $query->whereHas('tiketWisata.lokasi', function($q) use ($auth){
        $q->whereIn('kph_id', $auth->kph_list_id);
      });
    }
  }

  public function scopeJenisTiket($query, $idJenisTiket){
    return $query->whereHas('tiketWisata', function($q) use ($idJenisTiket){
      $q->where('jenis_tiket_id', $idJenisTiket);
    });
  }

  public function scopeKph($query, $idKph){
    return $query->whereHas('tiketWisata.lokasi', function($q) use ($idKph){
      $q->where('kph_id', $idKph);
    });    
  }

  public function scopeLokasi($query, $idLokasi){
    return $query->whereHas('tiketWisata', function($q) use ($idLokasi){
      $q->where('lokasi_id', $idLokasi);
    });
  }

  public function scopeStartDate($query, $date){
    return $query->whereDate('created_at', '>=', $date);
  }

  public function scopeByDate($query){
    return $query->groupBy(DB::raw('Date(created_at)'));
  }

  public function scopeTransaksi($query){
    return $query->byDate()->select(DB::raw('*, count(id) as total, min(nomor_karcis) as min, max(nomor_karcis) as max'));
  }

}
