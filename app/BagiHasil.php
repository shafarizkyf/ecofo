<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BagiHasil extends Model{
  
  public function tiketWisata(){
    return $this->belongsTo('App\TiketWisata');
  }
  
  public function mitra(){
    return $this->belongsTo('App\Mitra');
  }

}
