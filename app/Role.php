<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Role extends Model{
  
  protected static function boot(){
    parent::boot();

    static::addGlobalScope('orderByName', function(Builder $builder){
      $builder->orderBy('nama');
    });
  }

  public function hasPermission($idPermission){
    $hasPermission = $this->rolePermission()->where('permission_id', $idPermission)->first();
    return $hasPermission ? true : false;
  }

  public function permission(){
    return $this->hasMany('App\Permission');
  }
  
  public function rolePermission(){
    return $this->hasMany('App\RolePermission');
  }
  
}
