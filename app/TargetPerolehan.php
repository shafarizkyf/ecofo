<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class TargetPerolehan extends Model{

  public function getCountPengunjungAttribute(){
    return TransaksiTiket::whereHas('tiketWisata', function($q){
      $q->where('lokasi_id', $this->lokasi_id);
    })->count();
  }

  public function getPendapatanSaatIniAttribute(){
    $price = TiketWisata::active()->where('lokasi_id', $this->lokasi_id)->first();
    return $price ? $price->harga * $this->count_pengunjung : 0;
  }

  public function getPendapatanFormatAttribute(){
    return number_format($this->pendapatan);
  }

  public function getPengunjungFormatAttribute(){
    return number_format($this->pengunjung);
  }

  public function lokasi(){
    return $this->belongsTo('App\Lokasi');
  }

  public function presentase($target, $saatIni){
    $presentase = $saatIni/$target;
    return number_format($presentase, 3);
  }

  public function scopeActive($query){
    return $query->where('is_active', true);
  }

  public function scopeInMyOrganization($query){
    return $query->whereHas('lokasi.kph', function($q){
      $q->whereIn('id', Auth::user()->kph_list_id);
    });
  }

  public function scopeKph($query, $idKph){
    return $query->whereHas('lokasi', function($q) use ($idKph){
      $q->where('kph_id', $idKph);
    });
  }

}
