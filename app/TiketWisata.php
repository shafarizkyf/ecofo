<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TiketWisata extends Model{

  public function bagiHasil(){
    return $this->hasMany('App\BagiHasil');
  }

  public function getCreatedAtAttribute($date){
    return Carbon::parse($date);
  }

  public function getJenisWaktuAttribute($jenis){
    return $jenis == 'all' ? 'SEMUA HARI' : strtoupper($jenis);
  }

  public function getHargaFormatAttribute(){
    return number_format($this->harga);
  }

  public function getActiveAttribute($date){
    return $this->is_active ? 'Aktif' : 'Dimatikan';
  }

  public function komponenHarga(){
    return $this->hasMany('App\KomponenHarga');
  }

  public function lokasi(){
    return $this->belongsTo('App\Lokasi');
  }

  public function loket(){
    return $this->belongsTo('App\Loket');
  }

  public function jenisTiket(){
    return $this->belongsTo('App\JenisTiket');
  }

  public function scopeActive($query){
    return $query->where('is_active', true);
  }

  public function scopeInMyOrganization($query){
    return $query->whereHas('lokasi', function($q){
      $q->whereIn('kph_id', Auth::user()->kph_list_id);
    });
  }

  public function transaksi(){
    return $this->hasMany('App\TransaksiTiket');
  }

}
