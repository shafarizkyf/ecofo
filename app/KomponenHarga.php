<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomponenHarga extends Model{
  
  public function tiketWisata(){
    return $this->belongsTo('App\TiketWisata');
  }

  public function biayaOperasional(){
    return $this->belongsTo('App\BiayaOperasional');
  }
  
}
