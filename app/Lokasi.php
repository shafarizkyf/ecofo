<?php

namespace App;

use App\MyClass\Common;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Lokasi extends Model{
  
  protected static function boot(){
    parent::boot();
    static::addGlobalScope('orderByNama', function(Builder $builder){
      $builder->orderBy('nama');
    });
  }


  public function asuransi($periode, $date){
    $asuransi = BiayaPengurang::where('kebutuhan', 'like', '%asuransi%')->where('lokasi_id', $this->id);

    if($periode == 3){
      $periode1 = $this->_getPeriode($asuransi, 1, $date)->sum('total_biaya_operasional');

      $asuransi = BiayaPengurang::where('kebutuhan', 'like', '%asuransi%')->where('lokasi_id', $this->id);
      $periode2 = $this->_getPeriode($asuransi, 2, $date)->sum('total_biaya_operasional');
      return $periode1 + $periode2;
      
    }else{
      return $this->_getPeriode($asuransi, $periode, $date)->sum('total_biaya_operasional');
    }

  }

  public function getKodeAttribute($kode){
    return $kode ? $kode : substr($this->nama, 0, 5);
  }

  public function getNamaAttribute($nama){
    return strtoupper($nama);
  }

  public function kewajibanLain($periode, $date){
    $kewajibanLain = PendapatanBagiHasil::where('lokasi_id', $this->id)->where('is_pemda', true);

    if($periode == 3){
      $periode1 = $this->_getPeriode($kewajibanLain, 1, $date)->sum('bagi_hasil_mitra');

      $kewajibanLain = PendapatanBagiHasil::where('lokasi_id', $this->id)->where('is_pemda', true);
      $periode2 = $this->_getPeriode($kewajibanLain, 2, $date)->sum('bagi_hasil_mitra');
      return $periode1 + $periode2;
      
    }else{
      return $this->_getPeriode($kewajibanLain, $periode, $date)->sum('bagi_hasil_mitra');
    }
  }
  
  public function kph(){
    return $this->belongsTo('App\Kph');
  }
  
  public function loket(){
    return $this->hasMany('App\Loket');
  }
  
  public function mitra(){
    return $this->hasMany('App\MitraLokasi');
  }

  public function tiket(){
    return $this->hasMany('App\TiketWisata');
  }
  
  public function pendapatanBersih($periode, $date){

    if($periode == 3){

      $total = 0;
      for($i=1; $i<3; $i++){
        $pendapatanKotor = $this->pendapatanKotor($i, $date);
        $sharing = $this->sharing($i, $date);
        $asuransi = $this->asuransi($i, $date);
        $kewajibanLain = $this->kewajibanLain($i, $date);
        $total += $pendapatanKotor - ($sharing + $asuransi + $kewajibanLain);
      }
      
      return $total;
    }else{
      $pendapatanKotor = $this->pendapatanKotor($periode, $date);
      $sharing = $this->sharing($periode, $date);
      $asuransi = $this->asuransi($periode, $date);
      $kewajibanLain = $this->kewajibanLain($periode, $date);
      return $pendapatanKotor - ($sharing + $asuransi + $kewajibanLain);
    }    

  }

  public function pendapatanKotor($periode, $date){
    $transaksi = TransaksiTiket::lokasi($this->id);

    if($periode == 3){
      $periode1 = $this->_getPeriode($transaksi, 1, $date)->sum('harga');

      $transaksi = TransaksiTiket::lokasi($this->id);
      $periode2 = $this->_getPeriode($transaksi, 2, $date)->sum('harga');
      return $periode1 + $periode2;

    }else{
      return $this->_getPeriode($transaksi, $periode, $date)->sum('harga');
    }

  }

  public function pengunjung($periode, $date, $asal=null){
    $transaksi = TransaksiTiket::lokasi($this->id);
    if($asal) $transaksi = $transaksi->where('negara_pengunjung', $asal);

    if($periode == 3){
      $periode1 = $this->_getPeriode($transaksi, 1, $date)->count();

      $transaksi = TransaksiTiket::lokasi($this->id);
      if($asal) $transaksi = $transaksi->where('negara_pengunjung', $asal);
      $periode2 = $this->_getPeriode($transaksi, 2, $date)->count();

      return $periode1 + $periode2;
    }else{
      return $this->_getPeriode($transaksi, $periode, $date)->count();
    }

  }

  public function scopeActive($query){
    return $query->where('is_active', true);
  }  

  public function scopeInMyOrganization($query){
    $auth = Auth::user();
    return $auth->level->nama == 'Loket' 
    ? $query->where('id', $auth->userLoket->loket->lokasi_id)
    : $query->whereIn('kph_id', Auth::user()->kph_list_id);
  }

  public function scopeWhereKph($query, $idKph){
    return $query->where('kph_id', $idKph);
  }

  public function sharing($periode, $date){
    $sharing = PendapatanBagiHasil::where('lokasi_id', $this->id)->where('is_pemda', false);

    if($periode == 3){
      $periode1 = $this->_getPeriode($sharing, 1, $date)->sum('bagi_hasil_mitra');

      $sharing = PendapatanBagiHasil::where('lokasi_id', $this->id)->where('is_pemda', false);
      $periode2 = $this->_getPeriode($sharing, 2, $date)->sum('bagi_hasil_mitra');
      return $periode1 + $periode2;
      
    }else{
      return $this->_getPeriode($sharing, $periode, $date)->sum('bagi_hasil_mitra');
    }
  }

  public function targetPendapatan($tahun){
    $target = TargetPerolehan::where('lokasi_id', $this->id)->where('tahun', $tahun)->first();
    return $target ? $target->pendapatan : 0;
  }

  public function targetPengunjung($tahun, $asal=null){
    $target = TargetPerolehan::where('lokasi_id', $this->id)->where('tahun', $tahun)->first();

    if(!$target){
      return 0;
    }

    if($asal){
      $asal = "pengunjung_{$asal}";
      return $target->$asal;
    }else{
      return $target->pengunjung;
    }
  }

  public function totalAsuransi($periode, $date){
    $total = 0;
    $lokasi = Lokasi::where('kph_id', $this->kph_id)->get();
    foreach($lokasi as $o){
      $total += $o->asuransi($periode, $date);
    }
    return $total;
  }

  public function totalKewajibanLain($periode, $date){
    $total = 0;
    $lokasi = Lokasi::where('kph_id', $this->kph_id)->get();
    foreach($lokasi as $o){
      $total += $o->kewajibanLain($periode, $date);
    }
    return $total;
  }

  public function totalPendapatanBersih($periode, $date){
    $total = 0;
    $lokasi = Lokasi::where('kph_id', $this->kph_id)->get();
    foreach($lokasi as $o){
      $total += $o->pendapatanBersih($periode, $date);
    }
    return $total;
  }

  public function totalPendapatanKotor($periode, $date){
    $total = 0;
    $lokasi = Lokasi::where('kph_id', $this->kph_id)->get();
    foreach($lokasi as $o){
      $total += $o->pendapatanKotor($periode, $date);
    }
    return $total;
  }

  public function totalPengunjung($periode, $date, $asal=null){

    if($periode == 3){

      $total = 0;
      for($i=1; $i<3; $i++){
        $subTotal = 0;
        $lokasi = Lokasi::where('kph_id', $this->kph_id)->get();
        foreach($lokasi as $o){
          $subTotal += $o->pengunjung($i, $date, $asal);
        }

        $total += $subTotal;
      }

      return $total;

    }else{
      $total = 0;
      $lokasi = Lokasi::where('kph_id', $this->kph_id)->get();
      foreach($lokasi as $o){
        $total += $o->pengunjung($periode, $date, $asal);
      }
      return $total;
    }

  }

  public function totalSharing($periode, $date){
    $total = 0;
    $lokasi = Lokasi::where('kph_id', $this->kph_id)->get();
    foreach($lokasi as $o){
      $total += $o->sharing($periode, $date);
    }
    return $total;
  }

  public function _getPeriode($obj, $periode, $date){

    if($periode == 1){
      $tahun = date('Y', strtotime($date));
      $bulan = date('n', strtotime($date));
      $selectedMonth = $bulan - 1;
      $selectedMonth = sprintf('%02d', $selectedMonth);

      $startDate = "{$tahun}-01-01";
      $endDate = "{$tahun}-{$selectedMonth}-02";

      //misal ambil data periode desember cari dari 2018-01-01 s.d 2018-11-02
      $obj = $obj->whereDate('created_at', '>=', $startDate)
      ->whereDate('created_at', '<=', $endDate);

    }else{
      //satu bulan ada dua periode, 1-15 sama 16-31
      $date       = date('Y-m', strtotime($date));
      $startDate  = $periode == 2 ? "{$date}-1"   : "{$date}-16";
      $endDate    = $periode == 2 ? "{$date}-15"  : "{$date}-31";

      $obj = $obj->whereDate('created_at', '>=', $startDate)
      ->whereDate('created_at', '<=', $endDate);
    }

    return $obj;
  }

}
