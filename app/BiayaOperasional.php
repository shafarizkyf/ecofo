<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BiayaOperasional extends Model{

  public function komponen(){
    return $this->hasMany('App\KomponenHarga');
  }
  
  public function scopeActive($query){
    return $query->where('is_active', true);
  }
    
}
