@extends('app')

@section('title')
Data Mitra
@endsection

@section('title-page')
Data Mitra
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      
      @if(Auth::user()->hasPermission('mitra.store'))
      <a href="#custom-modal-add" class="btn btn-success pull-right waves-effect waves-light" data-animation="door" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a"><i class="ti ti-plus"></i> Mitra</a>
      @endif

      <h4 class="header-title m-t-0 m-b-30">Data Mitra</h4>
      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>KPH</th>
            <th>Mitra</th>
            <th>Penanggung Jawab</th>
            <th>Jabatan</th>
            <th>Kontak</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @php $no=1 @endphp
          @foreach($mitra as $o)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $o->kph->nama }}</td>
            <td>{{ $o->nama }}</td>
            <td>{{ $o->ketua }}</td>
            <td>{{ $o->jabatan }}</td>
            <td>{{ $o->kontak }}</td>
            <td>
              @if($o->is_pemda)
                <span class="badge badge-success">{{ $o->pemda }}</span>
              @else
                <span class="badge badge-info">{{ $o->pemda }}</span>
              @endif
            </td>
            <td>

              @if(Auth::user()->hasPermission('mitra.update'))
              <a href="#custom-modal-edit" class="btn btn-sm btn-warning waves-effect waves-light btn-edit m-b-5"
                data-kph="{{ $o->kph_id }}"
                data-unit-kerja="{{ $o->kph->unit_kerja_id }}"
                data-nama="{{ $o->nama }}"
                data-jabatan="{{ $o->jabatan }}"
                data-ketua="{{ $o->ketua }}"
                data-kontak="{{ $o->kontak }}"
                data-pemda="{{ $o->is_pemda }}"
                data-route="{{ route('mitra.update', $o->id) }}"
                data-animation="door" 
                data-plugin="custommodal" 
                data-overlaySpeed="100" 
                data-overlayColor="#36404a">
                <i class="ti ti-pencil"></i> Edit
              </a>
              @endif

              @if(Auth::user()->hasPermission('mitra.delete'))
              <a type="button" class="btn btn-danger waves-effect waves-light btn-sm btn-delete m-b-5" data-route="{{ route('mitra.delete', $o->id) }}"><i class="ti ti-trash"></i> Delete</a>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div><!-- end col -->
</div>
@endsection

@section('modal')
  <!-- Modal add-->
  <div id="custom-modal-add" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
    <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah Data Mitra</h4>
    <div class="custom-modal-text">
      <form class="form-horizontal group-border-dashed" action="{{ route('mitra.store') }}" method="POST" id="form-add">
        {{ csrf_field() }}

        <div class="form-group m-r-5 m-l-5">
          <div class="col-sm-6">
            <select name="unit_kerja" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}">
              <option value="">Pilih Divisi</option>
              @foreach($unitKerja as $o)
              <option value="{{ $o->id }}">{{ $o->wilayah }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-6">
            <select name="kph" class="form-control kph">
              <option value="">Pilih KPH</option>
            </select>
          </div>
        </div>

        <div class="form-group m-r-5 m-l-5">
          <div class="col-sm-6">
            <input type="text" name="nama" class="form-control" placeholder="Nama Mitra" required>
          </div>

          <div class="col-sm-6">
            <input type="text" name="ketua" class="form-control" placeholder="Ketua/Penanggung Jawab" required>
          </div>
        </div>

        <div class="form-group m-r-5 m-l-5">
          <div class="col-sm-6">
            <input type="text" name="jabatan" class="form-control" placeholder="Jabatan">
          </div>

          <div class="col-sm-6">
            <input type="text" name="kontak" class="form-control" placeholder="Nomor telp/hp">
          </div>
        </div>

        <div class="form-group m-r-5">
          <div class="col-sm-4">
            <div class="checkbox checkbox-primary">
              <input id="pemda-check" class="pemda" name="pemda" type="checkbox">
              <label for="pemda-check">Kemitraan Pemda</label>
            </div>
          </div>
        </div>

        <div class="form-group m-b-0">
          <div class="col-sm-offset-6 col-sm-8 m-t-15">
            <button type="reset" class="btn btn-default waves-effect m-l-5">Reset</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <!-- Modal edit-->
  <div id="custom-modal-edit" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
    <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Edit Data Mitra</h4>
    <div class="custom-modal-text">
      <form class="form-horizontal group-border-dashed" action="" method="post" id="form-edit">
        {{ csrf_field() }}

        <div class="form-group m-l-5 m-r-5">
          <div class="col-sm-6">
            <select name="unit_kerja" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}">
              <option value="">Pilih Divisi</option>
              @foreach($unitKerja as $o)
              <option value="{{ $o->id }}">{{ $o->wilayah }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-6">
            <select name="kph" class="form-control kph">
              <option value="">Pilih KPH</option>
            </select>
          </div>
        </div>

        <div class="form-group m-l-5 m-r-5">
          <div class="col-sm-6">
            <input type="text" name="nama" class="form-control" placeholder="Nama Mitra" required>
          </div>

          <div class="col-sm-6">
            <input type="text" name="ketua" class="form-control" placeholder="Ketua/Penanggung Jawab" required>
          </div>
        </div>
        
        <div class="form-group m-l-5 m-r-5">
          <div class="col-sm-6">
            <input type="text" name="jabatan" class="form-control" placeholder="Jabatan">
          </div>

          <div class="col-sm-6">
            <input type="text" name="kontak" class="form-control" placeholder="Nomor telp/hp">
          </div>          
        </div>
        
        <div class="form-group m-r-5">
          <div class="col-sm-4">
            <div class="checkbox checkbox-primary">
              <input id="pemda-check2" class="pemda" name="pemda" type="checkbox" checked>
              <label for="pemda-check2">Kemitraan Pemda</label>
            </div>
          </div>
        </div>

        <div class="form-group m-b-0">
          <div class="col-sm-offset-6 col-sm-8 m-t-15">
            <button type="reset" class="btn btn-default waves-effect m-l-5">Reset</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection 

@section('css-top')
  <!-- DataTables -->
  <link href="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- datepicker -->
  <link href="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <!-- select2 -->
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
  <!-- Sweet Alert css -->
  <link href="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
  <!-- Custom box css -->
  <link href="{{ asset('template/assets/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('js-top')
  <!-- Datatables-->
  <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- Datepicker -->
  <script src="{{ asset('template/assets/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- select2 -->
  <script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
  <!-- Modal-Effect -->
  <script src="{{ asset('template/assets/plugins/custombox/dist/custombox.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/custombox/dist/legacy.min.js') }}"></script>
  <!-- Sweet Alert js -->
  <script src="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
@endsection

@section('js-bottom')
  <script type="text/javascript">
    $(document).ready(function () {
      
      $('.select2').select2();

      jQuery('#date-range').datepicker({
        toggleActive: true
      });

      TableManageButtons.init();
      
      $('.unit-kerja').on('change', function(){ 
        var id_unit_kerja = $(this).val();
        var kphAx = $(this).data('list-kph');
        var kphEl = $(this).parent().parent().find('.kph');
        
        kphEl.find('option').remove().end().append('<option>Pilih KPH</option>')
        $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
          $.each(data, function(index, value){
            var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
            kphEl.append(option);
          });//end each
        });//end ajax
      });//end onchange
          
      $('body').delegate('.btn-edit', 'click', function(){
        var id_unit_kerja = $(this).data('unit-kerja');
        var pemda = $(this).data('pemda');
        var jabatan = $(this).data('jabatan');
        var nama = $(this).data('nama');
        var kph = $(this).data('kph');
        var ketua = $(this).data('ketua');
        var kontak = $(this).data('kontak');

        var route = $(this).data('route');
        var _token = $('meta[name=csrf-token]').attr('content');
        
        var kphAx = $('#form-edit select[name=unit_kerja]').data('list-kph');
        var kphEl = $('#form-edit select[name=kph]');

        kphEl.find('option').remove().end().append('<option>Pilih KPH</option>')
        $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
          $.each(data, function(index, value){
            var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
            kphEl.append(option);
            if(kph == value.id){
              kphEl.val(kph);
            }
          });//end each 
        });//end ajax
        
        $('#form-edit input[name=pemda]').prop('checked', pemda);
        $('#form-edit select[name=unit_kerja]').val(id_unit_kerja);
        $('#form-edit input[name=jabatan]').val(jabatan);
        $('#form-edit input[name=nama]').val(nama);
        $('#form-edit input[name=ketua]').val(ketua);
        $('#form-edit input[name=kontak]').val(kontak);
        $('#form-edit').attr('action', route);
      });

      $('body').delegate('.btn-delete', 'click', function(){
        var route = $(this).data('route');
        var _token = $('meta[name=csrf-token]').attr('content');

        swal({
          title: "Apakah benar data akan dihapus?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: false,
          closeOnCancel: false
        },function (isConfirm) {
          if (isConfirm) {
            $.post(route, {_token}).done(function(data){
              if(data.success){
                swal({title: "Dihapus", text: "Data berhasil dihapus!", type: "success"}, function(){
                  location.reload();
                });
              }else{
                swal("Error", data.msg, "error");
              }
            }).fail(function(error){
              console.log(error);
            });
          }else{
            swal("Dibatalkan", "Data tidak dihapus", "error");
          }//if confirm
        });//swall
      });//ondelete

    });

  </script>
@endsection