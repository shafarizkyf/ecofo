@extends('app')

@section('title')
Detail Lokasi
@endsection

@section('title-page')
Detail Lokasi
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card-box">

      <div class="row">
        <div class="col-md-6">
          <h4 class="header-title m-t-0 text-muted">Data Lokasi</h4>
          <dl class="row">
            <dt class="col-sm-3">Nama</dt>
            <dd class="col-sm-9">{{ $lokasi->nama }}</dd>
            <dt class="col-sm-3">Kode</dt>
            <dd class="col-sm-9">{{ $lokasi->kode }}</dd>
            <dt class="col-sm-3">KPH</dt>
            <dd class="col-sm-9">{{ $lokasi->kph->nama }}</dd>
            <dt class="col-sm-3">Unit Kerja</dt>
            <dd class="col-sm-9">{{ $lokasi->kph->unitKerja->wilayah }}</dd>
          </dl>
        </div>

        <div class="col-md-6">
          <h4 class="header-title m-t-0 text-muted">Mitra Lokasi</h4>
          <dl class="row">
            @php $no=1; @endphp
            @foreach($lokasi->mitra as $o)
            <dt class="col-sm-4">Mitra</dt>
            <dd class="col-sm-8">{{ $o->mitra->nama }}</dd>
            <dt class="col-sm-4">Penanggung Jawab</dt>
            <dd class="col-sm-8">{{ $o->mitra->ketua }}</dd>
            <dt class="col-sm-4">Kontak</dt>
            <dd class="col-sm-8">{{ $o->mitra->kontak }}</dd>
            <dt class="col-sm-4">Status</dt>
            <dd class="col-sm-8 m-b-10">
              @if($o->mitra->is_pemda)
                <span class="badge badge-success">{{ $o->mitra->pemda }}</span>
              @else
                <span class="badge badge-info">{{ $o->mitra->pemda }}</span>
              @endif
            </dd>
            @endforeach
          </dl>
        </div>
      </div>

      <hr>

      <div class="row">
        <div class="col-md-12">
          <h4 class="header-title m-t-20 m-b-10">Pendapatan Lokasi</h4>
          <div class="m-t-20">
            <table id="datatable-buttons" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Tanggal</th>
                  <th>Jenis Tiket</th>
                  <th>Harga</th>
                  <th>Pengunjung</th>
                  <th>Pendapatan</th>
                </tr>
              </thead>
              <tbody>
                @php $no=1; @endphp
                @foreach($transaksi as $o)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $o->created_at->format('d-m-Y') }}</td>
                  <td>{{ $o->tiketWisata->jenisTiket->nama }}</td>
                  <td>{{ number_format($o->tiketWisata->harga) }}</td>
                  <td>{{ number_format($o->total) }}</td>
                  <td>{{ number_format($o->total * $o->tiketWisata->harga)  }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection

@section('css-top')
  <!-- DataTables -->
  <link href="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- datepicker -->
  <link href="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <!-- select2 -->
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
  <!-- Sweet Alert css -->
  <link href="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
  <!-- Custom box css -->
  <link href="{{ asset('template/assets/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('js-top')
  <!-- Datatables-->
  <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- Datepicker -->
  <script src="{{ asset('template/assets/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- select2 -->
  <script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
  <!-- Modal-Effect -->
  <script src="{{ asset('template/assets/plugins/custombox/dist/custombox.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/custombox/dist/legacy.min.js') }}"></script>
  <!-- Sweet Alert js -->
  <script src="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
  <!-- Repeater -->
  <script src="{{ asset('template/assets/plugins/repeater/jquery.repeater.min.js') }}"></script>
@endsection


@section('js-bottom')
<script type="text/javascript">
  $(document).ready(function(){
    TableManageButtons.init();
  });
</script>
@endsection