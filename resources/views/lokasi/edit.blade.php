@extends('app')

@section('title')
Edit Lokasi
@endsection

@section('title-page')
Edit Lokasi
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <h4 class="header-title">Form Edit Lokasi</h4>
      <form class="form-horizontal group-border-dashed" action="{{ route('lokasi.update', $lokasi->id) }}" method="POST" id="form-add">
        {{ csrf_field() }}
        <div class="form-group">
          <div class="col-sm-6 m-b-10">
            <select name="unit_kerja" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}" required>
              <option value="">Pilih Divisi</option>
              @foreach($unitKerja as $o)
              <option value="{{ $o->id }}" {{ $o->id == $lokasi->kph->unit_kerja_id ? 'selected' : null }}>{{ $o->wilayah }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-6 m-b-10">
            <select name="kph" class="form-control kph" data-list-mitra="{{ route('mitra.axIndex') }}" required>
              <option value="">Pilih KPH</option>
              @foreach($kph as $o)
              <option value="{{ $o->id }}" {{ $o->id == $lokasi->kph_id ? 'selected' : null }}>{{ $o->nama }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-6 m-b-10">
            <input type="text" name="nama" class="form-control" placeholder="Nama Lokasi" value="{{ $lokasi->nama }}" required>
          </div>

          <div class="col-sm-6 m-b-10">
            <input type="text" name="kode" class="form-control" placeholder="Kode Lokasi" value="{{ $lokasi->kode }}" required>
          </div>
        </div>
    
        <hr>

        <fieldset>
          <div class="repeater-custom-show-hide">
            <div data-repeater-list="mitra">
              @if(count($lokasi->mitra))
                @foreach($lokasi->mitra as $o)
                  <div data-repeater-item="" class="row m-b-10 row-repeater">
                    <div class="col-sm-11 m-b-10">
                      <select name="mitra[0][id]" class="form-control mitra" required>
                        @foreach($mitra as $m)
                        <option value="{{ $m->id }}" {{ $m->id == $o->mitra_id ? 'selected' : null }}>{{ $m->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-sm-1 m-t-5">
                      <span data-repeater-delete="" class="btn btn-danger btn-trans btn-rounded btn-xs">
                        <span class="fa fa-times">
                      </span>
                    </div>
                  </div>
                @endforeach
              @else
                <div data-repeater-item="" class="row m-b-10 row-repeater">
                  <div class="col-sm-11 m-b-10">
                    <select name="mitra[0][id]" class="form-control mitra" required>
                      <option value="">Pilih Mitra</option>
                      @foreach($mitra as $m)
                      <option value="{{ $m->id }}">{{ $m->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-sm-1 m-t-5">
                    <span data-repeater-delete="" class="btn btn-danger btn-trans btn-rounded btn-xs">
                      <span class="fa fa-times">
                    </span>
                  </div>
                </div>
              @endif
            </div>

            <div class="form-group row m-b-5">
              <div class="col-sm-4">
                <span data-repeater-create="" class="btn btn-primary btn-add-mitra btn-trans btn-rounded btn-sm">
                  <span class="ti ti-plus"></span> Tambah Mitra
                </span>
              </div>
            </div>
          </div>
        </fieldset>

        <div class="row">
          <div class="col-md-12">
            <button class="btn btn-success pull-right">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection


@section('js-top')
  <!-- Repeater -->
  <script src="{{ asset('template/assets/plugins/repeater/jquery.repeater.min.js') }}"></script>
@endsection

@section('js-bottom')
<script type="text/javascript">
  $(document).ready(function () {
    
    $('.repeater-custom-show-hide').repeater({
      show: function () {
        $(this).slideDown();
      },
      hide: function (remove) {
        if (confirm('Are you sure you want to remove this item?')) {
          $(this).slideUp(remove);
        }
      }
    });
    
    $('.unit-kerja').on('change', function(){ 
      var id_unit_kerja = $(this).val();
      var kphAx = $(this).data('list-kph');
      var kphEl = $(this).parent().parent().find('.kph');
      
      kphEl.find('option').remove().end().append('<option>Pilih KPH</option>')
      $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          kphEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

    $('.kph').on('change', function(){ 
      var id_kph = $(this).val();
      var mitraAx = $(this).data('list-mitra');
      var mitraEl = $(this).parent().parent().parent().find('.mitra').first();

      mitraEl.find('option').remove().end().append('<option>Pilih Mitra</option>')
      $.getJSON(mitraAx, { id_kph }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          mitraEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

    $('.btn-add-mitra').on('click', function(){
      var mitraOptions = $('.mitra').first().find('option').clone();
      var count = $('.mitra').length;
      $('.mitra').eq(count-1).find('option').remove().end().append(mitraOptions);
    });

  });

</script>
@endsection