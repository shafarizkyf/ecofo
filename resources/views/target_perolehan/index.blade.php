@extends('app')

@section('title')
Data Target Perolehan
@endsection

@section('title-page')
Data Target Perolehan
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">

      @if(Auth::user()->hasPermission('target_perolehan.store'))
      <a href="#custom-modal-add" class="btn btn-success pull-right waves-effect waves-light" data-animation="door" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a"><i class="ti ti-plus"></i> Target Perolehan</a>
      @endif

      <h4 class="header-title m-t-0 m-b-30">Data Target Perolehan</h4>
      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Tahun</th>
            <th>KPH</th>
            <th>Lokasi</th>
            <th>Pendapatan</th>
            <th>Pengunjung</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @php $no=1 @endphp
          @foreach($targetPerolehan as $o)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $o->tahun }}</td>
            <td>{{ $o->lokasi->kph->nama }}</td>
            <td>{{ $o->lokasi->nama }}</td>
            <td>{{ $o->pendapatan_format }}</td>
            <td>{{ $o->pengunjung_format }}</td>
            <td>

              @if(Auth::user()->hasPermission('target_perolehan.update'))
              <a href="#custom-modal-edit" class="btn btn-sm btn-warning waves-effect waves-light btn-edit"
                data-tahun="{{ $o->tahun }}"
                data-unit-kerja="{{ $o->lokasi->kph->unit_kerja_id }}"
                data-kph="{{ $o->lokasi->kph_id }}"
                data-lokasi="{{ $o->lokasi_id }}"
                data-pendapatan="{{ $o->pendapatan }}"
                data-pengunjung="{{ $o->pengunjung }}"
                data-pengunjung-wni="{{ $o->pengunjung_wni }}"
                data-pengunjung-wna="{{ $o->pengunjung_wna }}"
                data-route="{{ route('target_perolehan.update', $o->id) }}"
                data-animation="door" 
                data-plugin="custommodal" 
                data-overlaySpeed="100" 
                data-overlayColor="#36404a">
                <i class="ti ti-pencil"></i> Edit
              </a>
              @endif

              @if(Auth::user()->hasPermission('target_perolehan.delete'))
              <a type="button" class="btn btn-danger waves-effect waves-light btn-sm btn-delete" data-route="{{ route('target_perolehan.delete', $o->id) }}"><i class="ti ti-trash"></i> Delete</a>
              @endif
              
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div><!-- end col -->
</div>
@endsection

@section('modal')
  <!-- Modal add-->
  <div id="custom-modal-add" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
    <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah Target Perolehan</h4>
    <div class="custom-modal-text">
      <form class="form-horizontal group-border-dashed" action="{{ route('target_perolehan.store') }}" method="POST" id="form-add">
        {{ csrf_field() }}

        <div class="form-group m-l-5 m-r-5">
          <div class="col-sm-4">
            <select name="tahun" class="form-control" required>
              <option value="">Pilih Tahun</option>
              @foreach($tahun as $o)
              <option value="{{ $o }}">{{ $o }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-4">
            <select name="unit_kerja" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}" required>
              <option value="">Pilih Unit Kerja</option>
              @foreach($unitKerja as $o)
              <option value="{{ $o->id }}">{{ $o->wilayah }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-4">
            <select name="kph" class="form-control kph"  data-list-lokasi="{{ route('lokasi.axIndex') }}" required>
              <option value="">Pilih KPH</option>
            </select>
          </div>
        </div>

        <div class="form-group m-l-5 m-r-5">
          <div class="col-sm-6">
            <select name="lokasi" class="form-control lokasi" required>
              <option value="">Pilih Lokasi</option>
            </select>
          </div>          

          <div class="col-sm-6">
            <input type="text" name="pendapatan" class="form-control" placeholder="Target pendapatan" required>
          </div>  
        </div>

        <div class="form-group m-l-5 m-r-5">
          <div class="col-sm-6">
            <input type="text" name="pengunjung_wna" class="form-control" placeholder="Target Pengunjung WNA" required>
          </div>    
          <div class="col-sm-6">
            <input type="text" name="pengunjung_wni" class="form-control" placeholder="Target Pengunjung WNI" required>
          </div>  
        </div>

        <div class="form-group m-r-5 m-b-0">
          <div class="col-sm-offset-6 col-sm-8 m-t-15">
            <button type="reset" class="btn btn-default waves-effect m-l-5">Reset</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <!-- Modal edit-->
  <div id="custom-modal-edit" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
    <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Edit Target Perolehan</h4>
    <div class="custom-modal-text">
      <form class="form-horizontal group-border-dashed" action="" method="post" id="form-edit">
        {{ csrf_field() }}

        <div class="form-group m-l-5 m-r-5">
          <div class="col-sm-4">
            <select name="tahun" class="form-control" required>
              <option value="">Pilih Tahun</option>
              @foreach($tahun as $o)
              <option value="{{ $o }}">{{ $o }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-4">
            <select name="unit_kerja" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}" required>
              <option value="">Pilih Unit Kerja</option>
              @foreach($unitKerja as $o)
              <option value="{{ $o->id }}">{{ $o->wilayah }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-4">
            <select name="kph" class="form-control kph"  data-list-lokasi="{{ route('lokasi.axIndex') }}" required>
              <option value="">Pilih KPH</option>
            </select>
          </div>
        </div>

        <div class="form-group m-l-5 m-r-5">
          <div class="col-sm-6">
            <select name="lokasi" class="form-control lokasi" required>
              <option value="">Pilih Lokasi</option>
            </select>
          </div>          

          <div class="col-sm-6">
            <input type="text" name="pendapatan" class="form-control" placeholder="Target pendapatan" required>
          </div>
        </div>

        <div class="form-group m-l-5 m-r-5">
          <div class="col-sm-6">
            <input type="text" name="pengunjung_wna" class="form-control" placeholder="Target Pengunjung WNA" required>
          </div>
          <div class="col-sm-6">
            <input type="text" name="pengunjung_wni" class="form-control" placeholder="Target Pengunjung WNI" required>
          </div>  
        </div>

        <div class="form-group m-r-5 m-b-0">
          <div class="col-sm-offset-6 col-sm-8 m-t-15">
            <button type="reset" class="btn btn-default waves-effect m-l-5">Reset</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
          </div>
        </div>

      </form>
    </div>
  </div>
@endsection 

@section('css-top')
  <!-- DataTables -->
  <link href="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- datepicker -->
  <link href="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <!-- select2 -->
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
  <!-- Sweet Alert css -->
  <link href="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
  <!-- Custom box css -->
  <link href="{{ asset('template/assets/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('js-top')
  <!-- Datatables-->
  <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- Datepicker -->
  <script src="{{ asset('template/assets/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- select2 -->
  <script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
  <!-- Modal-Effect -->
  <script src="{{ asset('template/assets/plugins/custombox/dist/custombox.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/custombox/dist/legacy.min.js') }}"></script>
  <!-- Sweet Alert js -->
  <script src="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
@endsection

@section('js-bottom')
  <script type="text/javascript">
    $(document).ready(function () {
      
      $('.select2').select2();

      jQuery('#date-range').datepicker({
        toggleActive: true
      });

      TableManageButtons.init();
      
      $('.unit-kerja').on('change', function(){ 
        var id_unit_kerja = $(this).val();
        var kphAx = $(this).data('list-kph');
        var kphEl = $(this).parent().parent().find('.kph');
        
        kphEl.find('option').remove().end().append('<option>Pilih KPH</option>')
        $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
          $.each(data, function(index, value){
            var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
            kphEl.append(option);
          });//end each
        });//end ajax
      });//end onchange


      $('.kph').on('change', function(){
        var id_kph = $(this).val();
        var lokasiAx = $(this).data('list-lokasi');
        var lokasiEl = $(this).parent().parent().parent().find('.lokasi');
        
        lokasiEl.find('option').remove().end().append('<option>Pilih Lokasi</option>')
        $.getJSON(lokasiAx, { id_kph }).then(function(data){
          $.each(data, function(index, value){
            var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
            lokasiEl.append(option);
          });//end each
        });//end ajax
      });//end onchange


      $('body').delegate('.btn-edit', 'click', function(){
        var tahun = $(this).data('tahun');
        var kph = $(this).data('kph');
        var lokasi = $(this).data('lokasi');
        var id_unit_kerja = $(this).data('unit-kerja');
        var pendapatan = $(this).data('pendapatan');
        var pengunjung = $(this).data('pengunjung');
        var pengunjung_wni = $(this).data('pengunjung-wni');
        var pengunjung_wna = $(this).data('pengunjung-wna');
        
        var route = $(this).data('route');
        var _token = $('meta[name=csrf-token]').attr('content');

        var kphAx = $('#form-edit select[name=unit_kerja]').data('list-kph');
        var kphEl = $('#form-edit .kph');

        var lokasiAx = $('#form-edit select[name=kph]').data('list-lokasi');
        var lokasiEl = $('#form-edit .lokasi');
        
        $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
          $.each(data, function(index, value){
            var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
            kphEl.append(option);
            if(kph == value.id){
              kphEl.val(kph);
            }//selected
          });//end each
        });//end ajax

        $.getJSON(lokasiAx, { id_kph:kph }).then(function(data){
          $.each(data, function(index, value){
            var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
            lokasiEl.append(option);
            if(lokasi == value.id){
              lokasiEl.val(lokasi);
            }//selected
          });//end each
        });//end ajax

        $('#form-edit select[name=unit_kerja]').val(id_unit_kerja);
        $('#form-edit select[name=tahun]').val(tahun);
        $('#form-edit select[name=kph]').val(kph);
        $('#form-edit input[name=pendapatan]').val(pendapatan);
        $('#form-edit input[name=pengunjung]').val(pengunjung);
        $('#form-edit input[name=pengunjung_wna]').val(pengunjung_wna);
        $('#form-edit input[name=pengunjung_wni]').val(pengunjung_wni);
        $('#form-edit').attr('action', route);
      });

      $('body').delegate('.btn-delete', 'click', function(){
        var route = $(this).data('route');
        var _token = $('meta[name=csrf-token]').attr('content');

        swal({
          title: "Apakah benar data akan dihapus?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: false,
          closeOnCancel: false
        },function (isConfirm) {
          if (isConfirm) {
            $.post(route, {_token}).done(function(data){
              if(data.success){
                swal({title: "Dihapus", text: "Data berhasil dihapus!", type: "success"}, function(){
                  location.reload();
                });
              }else{
                swal("Error", data.msg, "error");
              }
            }).fail(function(error){
              console.log(error);
            });
          }else{
            swal("Dibatalkan", "Data tidak dihapus", "error");
          }//if confirm
        });//swall
      });//ondelete

    });

  </script>
@endsection