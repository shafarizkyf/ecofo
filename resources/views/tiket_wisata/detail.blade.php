@extends('app')

@section('title')
Detail Tiket
@endsection

@section('title-page')
Detail Tiket
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card-box">

      <div class="row">
        <div class="col-sm-6">
          <h4 class="header-title m-t-0 text-muted">Data Tiket</h4>
          <dl class="row">
            <dt class="col-sm-4">Lokasi</dt>
            <dd class="col-sm-8 m-b-5">{{ $tiket->lokasi->nama }}</dd>
            <dt class="col-sm-4">Jenis Tiket</dt>
            <dd class="col-sm-8 m-b-5">{{ $tiket->jenisTiket->nama }}</dd>
            <dt class="col-sm-4">Loket</dt>
            <dd class="col-sm-8 m-b-5">{{ $tiket->loket->nama }}</dd>
            <dt class="col-sm-4">Harga</dt>
            <dd class="col-sm-8 m-b-5">{{ number_format($tiket->harga) }}</dd>
            <dt class="col-sm-4">Dibuat pada</dt>
            <dd class="col-sm-8">{{ $tiket->created_at->format('d F Y') }}</dd>
          </dl>
        </div>

        <div class="col-sm-6">
          <div class="row">
            <h4 class="header-title m-t-0 text-muted">Komponen Tiket</h4>
            @foreach($tiket->komponenHarga as $o)
            <dt class="col-sm-5">{{ $o->biayaOperasional->nama }}</dt>
            <dd class="col-sm-7 m-b-5">{{ number_format($o->biaya) }}</dd>
            @endforeach
            <h4 class="header-title m-t-5 text-muted">Bagi Hasil</h4>
            @foreach($tiket->bagiHasil as $o)
            <dt class="col-sm-6">{{ $o->mitra->nama }}</dt>
            <dd class="col-sm-6 m-b-5">{{ number_format($o->bagi_mitra) }}</dd>
            @endforeach
            <dt class="col-sm-6">Perhutani</dt>
            <dd class="col-sm-6 m-b-5">{{ number_format($tiket->bagiHasil->sum('bagi_perhutani')) }}</dd>
          </div>
        </div>
      </div> <!-- end row -->

      <hr>

      <div class="row">
        <div class="col-md-12">

          <form action="{{ route('laporan.berita_acara') }}" method="GET">
            <input type="hidden" name="id" value="{{ $tiket->id }}">
            <div class="row">
              <div class="col-md-4 m-b-10">
                <div class="input-daterange input-group" id="date-range">
                  <input type="text" class="form-control" name="tanggal_dari" placeholder="Tanggal dari" required />
                  <span class="input-group-addon bg-success b-0 text-white">to</span>
                  <input type="text" class="form-control" name="tanggal_sampai" placeholder="Tanggal Sampai" required />
                </div>
              </div>
              <div class="col-md-4 m-b-10">
                <button class="btn btn-success"><i class="ti ti-eye"></i> Lihat Laporan</button>
              </div>
            </div>            
          </form>

          <h4 class="header-title m-t-20 m-b-10">Data Transaksi Tiket</h4>
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Tanggal</th>
                <th>No Karcis Awal</th>
                <th>No Karcis Akhir</th>
                <th>Jumlah Lembar</th>
                <th>Perolehan</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @php $no=1 @endphp
              @foreach($transaksi as $o)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $o->created_at->format('d-m-Y') }}</td>
                <td>{{ $o->min }}</td>
                <td>{{ $o->max }}</td>
                <td>{{ number_format($o->total) }}</td>
                <td>{{ number_format($tiket->harga * $o->total) }}</td>
                <td>
                  <a href="{{ route('transaksi_tiket.index', ['lokasi'=>$tiket->lokasi_id, 'jenis_tiket'=>$tiket->jenis_tiket_id, 'date'=>$o->created_at->format('Y-m-d')]) }}" class="btn btn-primary btn-sm"><i class="ti ti-eye"></i> Detail</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

    </div><!-- end card box -->
  </div><!-- end col -->
</div>
@endsection

@section('css-top')
  <!-- DataTables -->
  <link href="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- datepicker -->
  <link href="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <!-- select2 -->
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
  <!-- Sweet Alert css -->
  <link href="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
  <!-- Custom box css -->
  <link href="{{ asset('template/assets/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('js-top')
  <!-- Datatables-->
  <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- Datepicker -->
  <script src="{{ asset('template/assets/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- select2 -->
  <script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
  <!-- Modal-Effect -->
  <script src="{{ asset('template/assets/plugins/custombox/dist/custombox.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/custombox/dist/legacy.min.js') }}"></script>
  <!-- Sweet Alert js -->
  <script src="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
@endsection

@section('js-bottom')
<script>
  $(document).ready(function(){
    jQuery('#date-range').datepicker({
      toggleActive: true,
      format: 'yyyy-mm-dd'
    });
    TableManageButtons.init();
  });
</script>
@endsection