@extends('app')

@section('title')
Tambah Tiket
@endsection

@section('title-page')
Tambah Tiket
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <form action="{{ route('tiket.store') }}" method="post" id="addTiket">
        {{ csrf_field() }}

        <div class="form-group row">
          <div class="col-sm-3">
            <label class="control-label">Unit Kerja</label>
            <select name="unit_kerja" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}" required>
              <option value="">Pilih Unit Kerja</option>
              @foreach($unitKerja as $o)
              <option value="{{ $o->id }}">{{ $o->wilayah }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-3">
            <label class="control-label">KPH</label>
            <select name="kph" class="form-control kph" data-list-lokasi="{{ route('lokasi.axIndex') }}" required>
              <option value="">Pilih KPH</option>
            </select>
          </div>

        </div>        

        <div class="form-group row">
          <div class="col-sm-3">
            <label class="control-label">Lokasi</label>
            <select name="lokasi" class="form-control lokasi" data-list-loket="{{ route('loket.axIndex') }}" data-list-mitra="{{ route('mitra.axIndex') }}" required>
              <option value="">Pilih Lokasi</option>
            </select>
          </div>

          <div class="col-sm-3">
            <label class="control-label">Loket</label>
            <select name="loket" class="form-control loket" required>
              <option value="">Pilih Loket</option>
            </select>
          </div>
            
          <div class="col-sm-3">
            <label class="control-label">Jenis Tiket</label>
            <select name="jenis_tiket" class="form-control" required>
              <option value="">Pilih Jenis Tiket</option>
              @foreach($jenisTiket as $o)
              <option value="{{ $o->id }}">{{ $o->nama }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-3">
            <label class="control-label">Harga</label>
            <input type="text" name="harga" class="form-control harga-tiket" placeholder="Harga Tiket" required>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-3">
            <label class="control-label">Waktu Tiket</label>
            <select name="jenis_waktu" class="form-control" required>
              <option value="">Pilih Waktu</option>
              <option value="all">Semua Hari</option>
              <option value="weekend">Weekend</option>
              <option value="weekday">Weekday</option>
            </select>
          </div>
        </div>

        <fieldset>
          <p class="m-t-30 control-label"><strong>Komponen Harga</strong></p>
          <div class="repeater-custom-show-hide">
            <div data-repeater-list="komponen">
              <div data-repeater-item="">
                <div class="form-group row d-flex align-items-end">
                  
                  <div class="col-md-3">
                    <select name="komponen[0][operasional]" id="" class="form-control" required>
                      <option value="">Pilih Komponen</option>
                      @foreach($komponen as $o)
                      <option value="{{ $o->id }}">{{ $o->nama }}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="col-md-3">
                    <input type="text" name="komponen[0][harga]" class="form-control komponen-harga" placeholder="Harga" required>
                  </div>

                  <div class="col-sm-1 m-t-5">
                    <span data-repeater-delete="" class="btn btn-danger btn-trans btn-rounded btn-xs">
                      <span class="fa fa-times">
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row mb-0">
              <div class="col-sm-12">
                <span data-repeater-create="" class="btn btn-primary btn-trans btn-rounded btn-sm">
                  <span class="ti ti-plus"></span> Komponen
                </span>
                <span class="label label-success" id="komponen-harga-message" style="display:none">Harga setelah komponen: <span id="harga-after-komponen"></span></span>
              </div>
            </div>
          </div>
        </fieldset>
        
        <fieldset>
          <p class="m-t-30 control-label"><strong>Bagi Hasil</strong></p>
          <div class="repeater-custom-show-hide">
            <div data-repeater-list="bagi">
              <div data-repeater-item="">
                <div class="form-group row  d-flex align-items-end">
                  
                  <div class="col-md-3">
                    <select name="bagi[0][id_mitra]"class="form-control mitra" required>
                      <option value="">Pilih Mitra</option>
                    </select>
                  </div>

                  <div class="col-md-3">
                    <select name="bagi[0][operasi]" id="" class="form-control jenis-pembagian" required>
                      <option value="">Jenis Pembagian</option>
                      <option value="persen">Persentase (%)</option>
                      <option value="rupiah">Nilai (Rp)</option>
                    </select>
                  </div>

                  <div class="col-md-2">
                    <input type="text" name="bagi[0][mitra]" class="form-control bagi-mitra" placeholder="Bagi Mitra" required>
                  </div>

                  <div class="col-md-2">
                    <input type="text" name="bagi[0][perhutani]" class="form-control bagi-perhutani" placeholder="Bagi Perhutani" required>
                  </div>

                  <div class="col-sm-1 m-t-5">
                    <span data-repeater-delete="" class="btn btn-danger btn-trans btn-rounded btn-xs">
                      <span class="fa fa-times">
                    </span>
                  </div>

                </div>
              </div>
            </div>
            <div class="form-group row mb-0">
              <div class="col-sm-12">
                <span data-repeater-create="" class="btn btn-primary btn-trans btn-add-mitra btn-rounded btn-sm">
                  <span class="ti ti-plus"></span> Mitra
                </span>
                <span class="label label-danger" id="bagi-hasil-message"></span>
              </div>
            </div>
          </div>
        </fieldset>

        <button class="btn btn-success btn-lg mt-5" id="btn-add-tiket">Simpan</button>
      </form>
    </div>
  </div>
</div>

@endsection

@section('css-top')
<link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
<!-- Sweet Alert css -->
<link href="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js-top')
<script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('template/assets/plugins/repeater/jquery.repeater.min.js') }}"></script>
<!-- Sweet Alert js -->
<script src="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
@endsection

@section('js-bottom')
<script>
  $(document).ready(function(){
    $('.select2').select2();
    $('.repeater-custom-show-hide').repeater({
      show: function () {
        $(this).slideDown();
      },
      hide: function (remove) {
        if (confirm('Are you sure you want to remove this item?')) {
          $(this).slideUp(remove);
        }
      }
    });

    $('.unit-kerja').on('change', function(){ 
      var id_unit_kerja = $(this).val();
      var kphAx = $(this).data('list-kph');
      var kphEl = $(this).parent().parent().find('.kph');
      
      kphEl.find('option').remove().end().append('<option>Pilih KPH</option>')
      $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          kphEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

    
    $('.kph').on('change', function(){
      var id_kph = $(this).val();
      var lokasiAx = $(this).data('list-lokasi');
      var lokasiEl = $(this).parent().parent().parent().find('.lokasi');

      lokasiEl.find('option').remove().end().append('<option>Pilih Lokasi</option>')
      $.getJSON(lokasiAx, { id_kph }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          lokasiEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

    $('.lokasi').on('change', function(){
      var id_lokasi = $(this).val();
      var loketAx = $(this).data('list-loket');
      var loketEl = $(this).parent().parent().find('.loket');
      
      var mitraAx = $(this).data('list-mitra');
      var mitraEl = $(this).parents().find('.mitra').first();
      
      mitraEl.find('option').remove().end().append('<option>Pilih Mitra</option>')
      $.getJSON(mitraAx, { id_lokasi }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          mitraEl.append(option);
        });//end each
      });//end ajax

      loketEl.find('option').remove().end().append('<option>Pilih Loket</option>')
      $.getJSON(loketAx, { id_lokasi }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          loketEl.append(option);
        });//end each
      });//end ajax
    });//end onchange
    
    var hargaTiket = 0;
    var hargaAfterPengurang = 0;
    var jenisPembagian = '';
    var bagiMitra = 0;
    var bagiPerhutani = 0;

    $('.harga-tiket').on('change', function(){
      hargaTiket = $(this).val();
    });

    $('body').delegate('.komponen-harga').on('change', function(){
      var komponenHarga = 0;
      $('.komponen-harga').each(function(){
        komponenHarga += Number($(this).val());
      });

      hargaAfterPengurang = hargaTiket - komponenHarga;
      $('#komponen-harga-message').show();
      $('#harga-after-komponen').text(hargaAfterPengurang);
    });

    $('body').delegate('.bagi-mitra, .bagi-perhutani, .jenis-pembagian', 'change', function(){
      bagiMitra = 0;
      bagiPerhutani = 0;
      
      if($(this).hasClass('jenis-pembagian')){
        jenisPembagian = $(this).val();
      }else if(jenisPembagian == ''){
        jenisPembagian = 'rupiah';
      }

      resetJenisPembagianOption();
      
      $('.bagi-mitra').each(function(){
        bagiMitra += Number($(this).val());
      });

      $('.bagi-perhutani').each(function(){
        bagiPerhutani += Number($(this).val());
      });
  
      var isValid = true;
      bagiHasil = bagiMitra + bagiPerhutani;

      if(jenisPembagian == 'rupiah' && bagiHasil > hargaAfterPengurang){
        isValid = false;
        swal('Ups!', 'Nilai bagi hasil melebihi harga tiket setelah dikurangi total biaya komponen tiket', 'warning');
      }else if(jenisPembagian == 'persen' && bagiHasil > 100){
        isValid = false;
        swal('Ups!', 'Total presentase melebihi 100%', 'warning');
      }

      var text = null;
      if(jenisPembagian == 'rupiah' && bagiHasil != hargaAfterPengurang){
        isValid = false;
        text = 'Total nilai bagi hasil tidak sama dengan harga tiket setelah dikurangi biaya komponen tiket';
      }else if(jenisPembagian == 'persen' && bagiHasil != 100){
        isValid = false;
        text = 'Total presentase tidak berjumlah 100%';
      }
      
      $('#btn-add-tiket').attr('disabled', !isValid);
      $('#bagi-hasil-message').text(text);

    });
    
    $('.btn-add-mitra').on('click', function(){
      resetJenisPembagianOption();
      var mitraOptions = $('.mitra').first().find('option').clone();

      $('.mitra').each(function(index){        
        $(this).append(mitraOptions);
      });
    });

    function resetJenisPembagianOption(){
      var jenisPembagianEl = $('#addTiket').find('.jenis-pembagian');
      jenisPembagianEl.each(function(){
        $(this).val(jenisPembagian);
      });
    }
    
  });
</script>
@endsection
