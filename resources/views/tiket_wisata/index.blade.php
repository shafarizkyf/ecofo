@extends('app')

@section('title')
Data Tiket
@endsection

@section('title-page')
Data Tiket
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      
      @if(Auth::user()->hasPermission('tiket.store'))
      <a href="{{ route('tiket.add') }}" class="btn btn-success pull-right"><i class="ti ti-plus"></i> Tiket</a>
      @endif
      
      <h4 class="header-title m-t-0 m-b-30">Data Tiket</h4>
      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Lokasi</th>
            <th>Tiket</th>
            <th>Jenis</th>
            <th>Loket</th>
            <th>Harga</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @php $no=1 @endphp
          @foreach($tiketWisata as $o)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $o->lokasi->nama }}</td>
            <td>{{ $o->jenisTiket->nama }}</td>
            <td>{{ $o->jenis_waktu }}</td>
            <td>{{ $o->loket->nama }}</td>
            <td>{{ number_format($o->harga) }}</td>
            <td>
              @if($o->is_active)
                <span class="badge badge-info">{{ $o->active }}</span>
              @else
                <span class="badge badge-danger">{{ $o->active }}</span>
              @endif
            </td>
            <td>

              @if(Auth::user()->hasPermission('tiket.detail'))
              <a href="{{ route('tiket.detail', $o->id) }}" class="btn btn-info btn-sm m-b-5"><i class="ti ti-eye"></i> Detail</a>
              @endif

              @if(Auth::user()->hasPermission('tiket.update') && !$o->transaksi->count())
              <a href="{{ route('tiket.edit', $o->id) }}" class="btn btn-warning btn-sm m-b-5"><i class="ti ti-pencil"></i> Edit</a>
              @endif

              @if(Auth::user()->hasPermission('tiket.delete'))
              <a type="button" class="btn btn-danger waves-effect waves-light btn-sm btn-delete m-b-5" data-route="{{ route('tiket.delete', $o->id) }}">
                <i class="ti ti-trash"></i> {{ $o->transaksi->count() ? 'Disable' : 'Delete' }}
              </a>
              @endif
              
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div><!-- end col -->
</div>
@endsection

@section('css-top')
  <!-- DataTables -->
  <link href="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- datepicker -->
  <link href="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <!-- select2 -->
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
  <!-- Sweet Alert css -->
  <link href="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
  <!-- Custom box css -->
  <link href="{{ asset('template/assets/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('js-top')
  <!-- Datatables-->
  <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- Datepicker -->
  <script src="{{ asset('template/assets/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- select2 -->
  <script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
  <!-- Sweet Alert js -->
  <script src="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
@endsection

@section('js-bottom')
<script type="text/javascript">
  $(document).ready(function () {
    $('.select2').select2();
    jQuery('#date-range').datepicker({
      toggleActive: true
    });
    TableManageButtons.init();

    $('body').delegate('.btn-delete', 'click', function(){
      var route = $(this).data('route');
      var _token = $('meta[name=csrf-token]').attr('content');

      swal({
        title: "Apakah benar data akan dihapus?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
      },function (isConfirm) {
        if (isConfirm) {
          $.post(route, {_token}).done(function(data){
            if(data.success){
              swal({title: "Dihapus", text: "Data berhasil dihapus!", type: "success"}, function(){
                location.reload();
              });
            }else{
              swal("Error", data.msg, "error");
            }
          }).fail(function(error){
            console.log(error);
          });
        }else{
          swal("Dibatalkan", "Data tidak dihapus", "error");
        }//if confirm
      });//swall
    });//ondelete


  });
</script>
@endsection