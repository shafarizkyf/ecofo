@extends('app')

@section('title')
Jenis Tiket
@endsection

@section('title-page')
Jenis Tiket
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      
      @if(Auth::user()->hasPermission('jenis_tiket.store'))
      <a href="#custom-modal-add" class="btn btn-success pull-right waves-effect waves-light" data-animation="door" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a"><i class="ti ti-plus"></i> Jenis Tiket</a>
      @endif

      <h4 class="header-title m-t-0 m-b-30">Data Jenis Tiket</h4>
      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @php $no=1 @endphp
          @foreach($jenisTiket as $o)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $o->nama }}</td>
            <td>

              @if(Auth::user()->hasPermission('jenis_tiket.update'))
              <a href="#custom-modal-edit" class="btn btn-sm btn-warning waves-effect waves-light btn-edit"
                data-nama="{{ $o->nama }}"
                data-route="{{ route('jenis_tiket.update', $o->id) }}"
                data-jenis="{{ $o->jenis }}"
                data-animation="door" 
                data-plugin="custommodal" 
                data-overlaySpeed="100" 
                data-overlayColor="#36404a">
                <i class="ti ti-pencil"></i> Edit
              </a>
              @endif

              @if(Auth::user()->hasPermission('jenis_tiket.delete'))
              <a type="button" class="btn btn-danger waves-effect waves-light btn-sm btn-delete" data-route="{{ route('jenis_tiket.delete', $o->id) }}"><i class="ti ti-trash"></i> Delete</a>
              @endif

            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div><!-- end col -->
</div>
@endsection

@section('modal')
  <!-- Modal add-->
  <div id="custom-modal-add" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
    <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah Jenis Tiket</h4>
    <div class="custom-modal-text">
      <form class="form-horizontal group-border-dashed" action="{{ route('jenis_tiket.store') }}" method="POST" id="form-add">
        {{ csrf_field() }}

        <div class="form-group">
          <label class="col-sm-3 control-label">Nama</label>
          <div class="col-sm-6">
            <input type="text" name="nama" class="form-control" placeholder="Jenis Tiket" required>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 control-label">Jenis</label>
          <div class="col-sm-6">
            <select name="jenis" class="form-control" required>
              <option value="">Pilih Jenis Tiket</option>
              <option value="wni">WNI</option>
              <option value="wna">WNA</option>
            </select>
          </div>
        </div>

        <div class="form-group m-b-0">
          <div class="col-sm-offset-3 col-sm-9 m-t-15">
            <button type="reset" class="btn btn-default waves-effect m-l-5">Cancel</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <!-- Modal edit-->
  <div id="custom-modal-edit" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
    <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Edit Jenis Tiket</h4>
    <div class="custom-modal-text">
      <form class="form-horizontal group-border-dashed" action="" method="post" id="form-edit">

        {{ csrf_field() }}

        <div class="form-group">
          <label class="col-sm-3 control-label">Nama</label>
          <div class="col-sm-6">
            <input type="text" name="nama" class="form-control" placeholder="Jenis Tiket" required>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Jenis</label>
          <div class="col-sm-6">
            <select name="jenis" class="form-control" required>
              <option value="">Pilih Jenis Tiket</option>
              <option value="wni">WNI</option>
              <option value="wna">WNA</option>
            </select>
          </div>
        </div>

        <div class="form-group m-b-0">
          <div class="col-sm-offset-3 col-sm-9 m-t-15">
            <button type="reset" class="btn btn-default waves-effect m-l-5">Reset</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection 

@section('css-top')
  <!-- DataTables -->
  <link href="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- datepicker -->
  <link href="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <!-- select2 -->
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
  <!-- Sweet Alert css -->
  <link href="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
  <!-- Custom box css -->
  <link href="{{ asset('template/assets/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('js-top')
  <!-- Datatables-->
  <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- Datepicker -->
  <script src="{{ asset('template/assets/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- select2 -->
  <script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
  <!-- Modal-Effect -->
  <script src="{{ asset('template/assets/plugins/custombox/dist/custombox.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/custombox/dist/legacy.min.js') }}"></script>
  <!-- Sweet Alert js -->
  <script src="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
@endsection

@section('js-bottom')
<script type="text/javascript">
  $(document).ready(function () {
    
    $('.select2').select2();

    jQuery('#date-range').datepicker({
      toggleActive: true
    });

    TableManageButtons.init();
    
    $('body').delegate('.btn-edit', 'click', function(){
      var nama = $(this).data('nama');
      var jenis = $(this).data('jenis');
      var route = $(this).data('route');
      var _token = $('meta[name=csrf-token]').attr('content');
      
      $('#form-edit select[name=jenis]').val(jenis);
      $('#form-edit input[name=nama]').val(nama);
      $('#form-edit').attr('action', route);
    });

    $('body').delegate('.btn-delete', 'click', function(){
      var route = $(this).data('route');
      var _token = $('meta[name=csrf-token]').attr('content');

      swal({
        title: "Apakah benar data akan dihapus?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
      },function (isConfirm) {
        if (isConfirm) {
          $.post(route, {_token}).done(function(data){
            if(data.success){
              swal({title: "Dihapus", text: "Data berhasil dihapus!", type: "success"}, function(){
                location.reload();
              });
            }else{
              swal("Error", data.msg, "error");
            }
          }).fail(function(error){
            console.log(error);
          });
        }else{
          swal("Dibatalkan", "Data tidak dihapus", "error");
        }//if confirm
      });//swall
    });//ondelete

  });

</script>
@endsection