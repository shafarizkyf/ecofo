@extends('app')

@section('title')
Edit Role
@endsection

@section('title-page')
Edit Role
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <form action="{{ route('role.update', $role->id) }}" method="post">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-md-12 m-b-20">
            <input type="text" class="form-control" name="nama" placeholder="Nama Role" value="{{ $role->nama }}" required>
          </div>
          @foreach($modul as $o)
          <div class="col-md-4">
            <h4 class="header-title">Modul {{ $o->name }}</h4>
            @foreach($o->permission as $j)
            <div class="form-group">
              <div class="col-sm-12">
                <div class="checkbox checkbox-primary">
                  <input id="{{ $j->id }}" name="permission[{{ $j->id }}]" type="checkbox" {{ $role->hasPermission($j->id) ? 'checked' : null }}>
                  <label for="{{ $j->id }}">
                    {{ $j->nama }}
                  </label>
                </div>
              </div>
            </div>
            @endforeach
          </div>
          @endforeach
          <div class="col-md-12 m-t-20">
            <button class="btn btn-primary pull-right">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
