@extends('app')

@section('title')
Dashboard
@endsection

@section('title-page')
Dashboard
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <form action="" id="filterForm" data-route="{{ route('dashboard.axIndex') }}">
        <div class="row">
          <h5 class="m-t-0 m-l-10">Filter Data</h5>
          <div class="col-md-4 m-b-10">
            <select name="tahun" class="form-control">
              <option value="">Pilih Tahun</option>
              @foreach($tahun as $o)
              <option value="{{ $o->tahun }}">{{ $o->tahun }}</option>
              @endforeach
            </select>  
          </div>
              
          <div class="col-md-4 m-b-10">
            <select name="unit_kerja" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}">
              <option value="">Pilih Unit Kerja</option>
              @foreach($unitKerja as $o)
              <option value="{{ $o->id }}">{{ $o->wilayah }}</option>
              @endforeach
            </select>  
          </div>

          <div class="col-md-4 m-b-10">
            <select name="kph" class="form-control kph" data-list-lokasi="{{ route('lokasi.axIndex') }}">
              <option value="">Pilih KPH</option>
            </select>  
          </div>

          <div class="col-md-4 m-b-10">
            <select name="lokasi" class="form-control lokasi">
              <option value="">Pilih Lokasi</option>
            </select>  
          </div>

          <div class="col-md-4 m-b-10">
            <button class="btn btn-primary" id="filter">Filter</button>
          </div>

        </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="mini-stat clearfix bg-facebook rounded">
      <span class="mini-stat-icon"><i class="fas fa-child fg-facebook"></i></span>
      <div class="mini-stat-info">
        <span id="todayVisitor"></span>
        Pengunjung Hari Ini
      </div>
    </div>
  </div>       
  <div class="col-md-6">
    <div class="mini-stat clearfix bg-googleplus rounded">
      <span class="mini-stat-icon"><i class="fas fa-money-bill-wave fg-googleplus"></i></span>
      <div class="mini-stat-info">
        <span id="todayRevenue"></span>
        Pendapatan Hari ini
      </div>
    </div>
  </div>       
</div>

<div class="row">
  <div class="col-md-6">
    <div class="card-box">
      <canvas id="pendapatan"></canvas>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card-box">
      <canvas id="pengunjung"></canvas>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="card-box">
      <canvas id="pendapatan-by-lokasi"></canvas>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card-box">
      <canvas id="pengunjung-by-lokasi"></canvas>
    </div>
  </div>
</div>
@endsection

@section('css-bottom')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('template/assets/css/mycustom.css') }}">
@endsection

@section('js-top')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
@endsection

@section('js-bottom')
<script>
  $(document).ready(function(){

    var dataPendapatan = {};
    var dataPengunjung = {};
    var dataTopPendapatan = {};
    var dataTopPengunjung = {};
    var dataTopPendapatanLabel = {};
    var dataTopPengunjungLabel = {};
    var dataBulan = {};

    updateChart();

    $('body').delegate('.unit-kerja', 'change', function(){ 
      var id_unit_kerja = $(this).val();
      var kphAx = $(this).data('list-kph');
      var kphEl = $(this).parent().parent().find('.kph');
      
      kphEl.find('option').remove().end().append('<option>Pilih KPH</option>')
      $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          kphEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

    $('.kph').on('change', function(){
      var id_kph = $(this).val();
      var lokasiAx = $(this).data('list-lokasi');
      var lokasiEl = $(this).parent().parent().parent().find('.lokasi');

      lokasiEl.find('option').remove().end().append('<option>Pilih Lokasi</option>')
      $.getJSON(lokasiAx, { id_kph }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          lokasiEl.append(option);
        });//end each
      });//end ajax
    });//end onchangeW

    var pendapatan = $('#pendapatan')
    var chartPendapatan = new Chart(pendapatan, {
      type: 'line',
      data: {
        datasets:[
          {
            label: 'Pendapatan Tahun Ini',
            backgroundColor: 'rgba(255, 223, 109, 0.8)',
            borderColor: 'rgba(255, 223, 109, 1)'
          },
        ]
      },
    });

    var pengunjung = $('#pengunjung')
    var chartPengunjung = new Chart(pengunjung, {
      type: 'line',
      data: {
        datasets:[
          {
            label: 'Pengunjung Tahun Ini',
            backgroundColor: 'rgba(174, 112, 255, 0.8)',
            borderColor: 'rgba(174, 112, 255, 1)'
          },
        ]
      },
    });

    var pendapatanByLokasi = $('#pendapatan-by-lokasi');
    pendapatanByLokasi.height = 500;
    var chartTopPendapatan = new Chart(pendapatanByLokasi, {
      type: 'bar',
      data: {
        datasets:[
          {
            label: 'Pendapatan Lokasi',
            backgroundColor: 'rgba(61, 179, 249, 0.8)',
            borderColor: 'rgba(61, 179, 249, 1)'
          },
        ]
      },
    });

    var pendapatanByLokasi = $('#pengunjung-by-lokasi');
    pendapatanByLokasi.height = 500;
    var chartTopPengunjung = new Chart(pendapatanByLokasi, {
      type: 'bar',
      data: {
        datasets:[
          {
            label: 'Pengunjung Lokasi',
            backgroundColor: 'rgba(255, 103, 155, 0.8)',
            borderColor: 'rgba(255, 103, 155, 1)'
          },
        ]
      },
    });


    $('#filter').on('click', function(e){
      e.preventDefault();
      updateChart();
    });

    function updateChart(){

      var tahun = $('#filterForm select[name=tahun]').val();
      var id_unit_kerja = $('#filterForm select[name=unit_kerja]').val();
      var id_kph = $('#filterForm select[name=kph]').val();
      var id_lokasi = $('#filterForm select[name=lokasi]').val();
      var route = $('#filterForm').data('route');

      $.getJSON(route, {id_unit_kerja, id_kph, id_lokasi, tahun}).then(function(data){
        
        dataPendapatan = data.transaksi.map(function(obj, index){
          return obj.pendapatan;
        });

        dataPengunjung = data.transaksi.map(function(obj, index){
          return obj.total;
        });

        dataTopPendapatan = data.top_pendapatan.map(function(obj, index){
          return obj.pendapatan;
        });

        dataTopPendapatanLabel = data.top_pendapatan.map(function(obj, index){
          return obj.nama_lokasi;
        });

        dataTopPengunjung = data.top_pengunjung.map(function(obj, index){
          return obj.total;
        });

        dataTopPengunjungLabel = data.top_pengunjung.map(function(obj, index){
          return obj.nama_lokasi;
        });

        dataBulan = data.transaksi.map(function(obj, index){
          return obj.bulan;
        });
        
        chartPendapatan.data.datasets[0].data = dataPendapatan;
        chartPendapatan.data.labels = dataBulan;
        chartPendapatan.update();
        
        chartPengunjung.data.datasets[0].data = dataPengunjung;
        chartPengunjung.data.labels = dataBulan;
        chartPengunjung.update();
  
        chartTopPendapatan.data.datasets[0].data = dataTopPendapatan;
        chartTopPendapatan.data.labels = dataTopPendapatanLabel;
        chartTopPendapatan.update();

        chartTopPengunjung.data.datasets[0].data = dataTopPengunjung;
        chartTopPengunjung.data.labels = dataTopPengunjungLabel;
        chartTopPengunjung.update();
        
        $('#todayVisitor').html(data.today_perolehan.pengunjung);
        $('#todayRevenue').html(data.today_perolehan.pendapatan);
      });
    }

  });
</script>
@endsection
