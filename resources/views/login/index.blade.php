<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Efoco System | Sistem eTicketing Wisata Perum Perhutani">
  <meta name="author" content="shafarizkyf">

  <!-- App Favicon -->
  <link rel="shortcut icon" href="{{ asset('template/assets/images/favicon.ico') }}">

  <!-- App title -->
  <title>Efoco | Login</title>

  <!-- App CSS -->
  <link href="{{ asset('template/assets/css/bootstrap.min.css') }} " rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/core.css') }} " rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/components.css') }} " rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/icons.css') }} " rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/pages.css') }} " rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/menu.css') }} " rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/responsive.css') }} " rel="stylesheet" type="text/css" />

  <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
  
  <script src="{{ asset('template/assets/js/modernizr.min.js') }}"></script>
</head>
<body>
  
  <div class="account-pages"></div>
  <div class="clearfix"></div>
  <div class="wrapper-page">
    <div class="text-center">
        <a href="{{ route('login') }}" class="logo"><span>Eco<span>fo</span></span></a>
    </div>
    <div class="m-t-40 card-box">
      <div class="text-center">
        <h4 class="text-uppercase font-bold m-b-0">Selamat Datang</h4>
      </div>
      <div class="panel-body">

        @if(Session::has('danger'))
        <div class="row">
          <div class="col-sm-12">
            <div class="card m-b-0 bg-danger text-xs-center">
              <div class="card-body">
                <blockquote class="card-bodyquote m-b-0">
                  <h5 class="text-white m-0"><strong><i class="fa fa-close"></i> Ups!</strong> {{ Session::get('danger') }}</h5>
                </blockquote>
              </div>                  
            </div>
          </div>
        </div>
        @endif

        <form class="form-horizontal m-t-20" action="{{ route('login.login') }}" method="post">
          {{ csrf_field() }}
          <div class="form-group ">
            <div class="col-xs-12">
              <input class="form-control" type="text" name="username" required="" placeholder="Username">
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <input class="form-control" type="password" name="password" required="" placeholder="Password">
            </div>
          </div>    
          <div class="form-group text-center m-t-30">
            <div class="col-xs-12">
              <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Login</button>
            </div>
          </div>          
        </form>
      </div>
    </div>
    <!-- end card-box-->    
  </div>
  <!-- end wrapper page -->

  <script>
    var resizefunc = [];
  </script>
  
  <!-- jQuery  -->
  <script src="{{ asset('template/assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('template/assets/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/js/detect.js') }}"></script>
  <script src="{{ asset('template/assets/js/fastclick.js') }}"></script>
  <script src="{{ asset('template/assets/js/jquery.slimscroll.js') }}"></script>
  <script src="{{ asset('template/assets/js/jquery.blockUI.js') }}"></script>
  <script src="{{ asset('template/assets/js/waves.js') }}"></script>
  <script src="{{ asset('template/assets/js/wow.min.js') }}"></script>
  <script src="{{ asset('template/assets/js/jquery.nicescroll.js') }}"></script>
  <script src="{{ asset('template/assets/js/jquery.scrollTo.min.js') }}"></script>
  
  <!-- App js -->
  <script src="{{ asset('template/assets/js/jquery.core.js') }}"></script>
  <script src="{{ asset('template/assets/js/jquery.app.js') }}"></script>
  
</body>
</html>