<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Efoco System | Sistem eTicketing Wisata Perum Perhutani">
  <meta name="author" content="shafarizkyf">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- App Favicon -->
  <link rel="shortcut icon" href="{{ asset('template/assets/images/favicon.ico') }}">
  <!-- App title -->
  <title>Ecofo | @yield('title')</title>
  
  @yield('css-top')
  
  <!-- App CSS -->
  <link href="{{ asset('template/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/menu.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />

  @yield('css-bottom')

  <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->    
  <script src="{{ asset('template/assets/js/modernizr.min.js') }}"></script>
    
  </head>
  
  <body class="fixed-left">  
    <!-- Begin page -->
    <div id="wrapper">
      <!-- Top Bar Start -->
      <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
          <a href="index.html" class="logo"><span>Eco<span>fo</span></span><i class="zmdi zmdi-layers"></i></a>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
          <div class="container">
            <!-- Page title -->
            <ul class="nav navbar-nav navbar-left">
              <li>
                <button class="button-menu-mobile open-left">
                  <i class="zmdi zmdi-menu"></i>
                </button>
              </li>
              <li>
                <h4 class="page-title">@yield('title-page')</h4>
              </li>
            </ul>
            
          </div><!-- end container -->
        </div><!-- end navbar -->
      </div>
      <!-- Top Bar End -->
      
      <!-- ========== Left Sidebar Start ========== -->
      <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
          <!-- User -->
          <div class="user-box">
            <div class="user-img">
              <img src="{{ Auth::user()->foto_path }}" alt="user-img" title="{{ Auth::user()->nama }}" class="img-circle img-thumbnail img-responsive">
            </div>
            <h5><a href="#">{{ Auth::user()->nama }}</a> </h5>
            <ul class="list-inline">
              <li>
                <a href="{{ route('logout.logout') }}" class="text-custom">
                  <i class="zmdi zmdi-power"></i>
                </a>
              </li>              
            </ul>
          </div>
          <!-- End User -->
          <!--- Sidebar -->
          <div id="sidebar-menu">
            <ul>
              <li class="text-muted menu-title">Main Menu</li>
              
              @if(Auth::user()->hasPermission('dashboard.index'))
              <li>
                <a href="{{ route('dashboard.index') }}" class="waves-effect"><i class="ti ti-dashboard"></i> <span>Dashboard</span></a>
              </li>
              @endif

              @if(Auth::user()->hasPermission('transaksi_tiket.index'))
              <li>
                <a href="{{ route('transaksi_tiket.index') }}" class="waves-effect"><i class="ti ti-receipt"></i> <span>Transaksi</span></a>
              </li>
              @endif
              
              @if(Auth::user()->hasPermission('laporan.realisasi') || Auth::user()->hasPermission('laporan.pendapatan'))
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti ti-agenda"></i><span> Laporan</span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                  @if(Auth::user()->hasPermission('laporan.realisasi'))    
                  <li><a href="{{ route('laporan.realisasi') }}">Realisasi</a></li>
                  @endif
                  
                  @if(Auth::user()->hasPermission('laporan.perolehan'))    
                  <li><a href="{{ route('laporan.perolehan') }}">Perolehan</a></li>
                  @endif
                </ul>
              </li>
              @endif

              @if(
                Auth::user()->hasPermission('tiket.index') || 
                Auth::user()->hasPermission('loket.index') ||
                Auth::user()->hasPermission('lokasi.index') ||
                Auth::user()->hasPermission('jenis_tiket.index'))
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti ti-ticket"></i><span> Tiket Wisata</span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                  
                  @if(Auth::user()->hasPermission('tiket.index'))                  
                  <li><a href="{{ route('tiket.index') }}">Tiket</a></li>
                  @endif
                  
                  @if(Auth::user()->hasPermission('loket.index'))                  
                  <li><a href="{{ route('loket.index') }}">Loket</a></li>
                  @endif
                  
                  @if(Auth::user()->hasPermission('lokasi.index'))                  
                  <li><a href="{{ route('lokasi.index') }}">Lokasi</a></li>
                  @endif

                  @if(Auth::user()->hasPermission('jenis_tiket.index'))                  
                  <li><a href="{{ route('jenis_tiket.index') }}">Jenis Tiket</a></li>
                  @endif
                </ul>
              </li>
              @endif
              
              @if(
                Auth::user()->hasPermission('target_perolehan.index') || 
                Auth::user()->hasPermission('biaya_operasional.index') ||
                Auth::user()->hasPermission('mitra.index') ||
                Auth::user()->hasPermission('kph.index'))
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti ti-book"></i><span> Master Data </span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">

                  @if(Auth::user()->hasPermission('target_perolehan.index'))
                  <li><a href="{{ route('target_perolehan.index') }}">Target Perolehan</a></li>
                  @endif

                  @if(Auth::user()->hasPermission('biaya_operasional.index'))
                  <li><a href="{{ route('biaya_operasional.index') }}">Komponen Biaya</a></li>
                  @endif

                  @if(Auth::user()->hasPermission('mitra.index'))
                  <li><a href="{{ route('mitra.index') }}">Mitra</a></li>
                  @endif

                  @if(Auth::user()->hasPermission('kph.index'))
                  <li><a href="{{ route('kph.index') }}">KPH</a></li>
                  @endif

                </ul>
              </li>
              @endif

              @if(Auth::user()->hasPermission('user.index') || Auth::user()->hasPermission('role.index'))
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti ti-user"></i><span> Managemen Pengguna </span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">

                  @if(Auth::user()->hasPermission('user.index'))
                  <li><a href="{{ route('user.index') }}">Pengguna</a></li>
                  @endif

                  @if(Auth::user()->hasPermission('role.index'))
                  <li><a href="{{ route('role.index') }}">Role</a></li>
                  @endif

                </ul>
              </li>
              @endif


            </ul>
            <div class="clearfix"></div>
          </div>
          <!-- Sidebar -->
          <div class="clearfix"></div>              
        </div>            
      </div>
      <!-- Left Sidebar End -->
                                    
      <!-- ============================================================== -->
      <!-- Start right Content here -->
      <!-- ============================================================== -->
      <div class="content-page">
        <!-- Start content -->
        <div class="content">
          <div class="container">

            @if(Session::has('success'))
            <div class="row">
              <div class="col-sm-12">
                <div class="card m-b-0 bg-success text-xs-center">
                  <div class="card-body">
                    <blockquote class="card-bodyquote">
                      <h5 class="text-white m-0"><strong><i class="fa fa-check"></i> Oke!</strong> {{ Session::get('success') }}</h5>
                    </blockquote>
                  </div>                  
                </div>
              </div>
            </div>
            @endif

            @if(Session::has('warning'))
            <div class="row">
              <div class="col-sm-12">
                <div class="card m-b-0 bg-warning text-xs-center">
                  <div class="card-body">
                    <blockquote class="card-bodyquote">
                      <h5 class="text-white m-0"><strong><i class="fa fa-info"></i> Ups!</strong> {{ Session::get('warning') }}</h5>
                    </blockquote>
                  </div>                  
                </div>
              </div>
            </div>
            @endif

            @if(Session::has('danger'))
            <div class="row">
              <div class="col-sm-12">
                <div class="card m-b-0 bg-danger text-xs-center">
                  <div class="card-body">
                    <blockquote class="card-bodyquote">
                      <h5 class="text-white m-0"><strong><i class="fa fa-info"></i> Ups!</strong> {{ Session::get('danger') }}</h5>
                    </blockquote>
                  </div>                  
                </div>
              </div>
            </div>
            @endif

            @yield('content')

          </div> <!-- container -->
        </div> <!-- content -->
        

        <footer class="footer">
          © {{ date('Y') }} Perum Perhutani
        </footer>

        @yield('modal')
        
      </div>
      <!-- End content-page -->
      
      <!-- ============================================================== -->
      <!-- End Right content here -->
      <!-- ============================================================== -->
      <!-- Right Sidebar -->
      <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle">
          <i class="zmdi zmdi-close-circle-o"></i>
        </a>
      </div>
      <!-- /Right-bar -->      
    </div>

    <!-- END wrapper -->
    <script>var resizefunc = [];</script>    
    <!-- jQuery  -->
    <script src="{{ asset('template/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/detect.js') }}"></script>
    <script src="{{ asset('template/assets/js/fastclick.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.blockUI.js') }}"></script>
    <script src="{{ asset('template/assets/js/waves.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.scrollTo.min.js') }}"></script>

    @yield('js-top')
    
    <!-- App js -->
    <script src="{{ asset('template/assets/js/jquery.core.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.app.js') }}"></script> 
    
    @yield('js-bottom')
    
</body>
</html>