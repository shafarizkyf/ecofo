@extends('app')

@section('title')
Edit Loket
@endsection

@section('title-page')
Edit Loket
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <h4 class="header-title">Form Edit Loket</h4>
      <form class="form-horizontal group-border-dashed" action="{{ route('loket.update', $loket->id) }}" method="POST" id="form-add">
        {{ csrf_field() }}
        <div class="form-group">
          <div class="col-sm-6 m-b-10">
            <select name="unit_kerja" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}" required>
              <option value="">Pilih Divisi</option>
              @foreach($unitKerja as $o)
              <option value="{{ $o->id }}" {{ $o->id == $loket->lokasi->kph->unit_kerja_id ? 'selected' : null }}>{{ $o->wilayah }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-6 m-b-10">
            <select name="kph" class="form-control kph" data-list-lokasi="{{ route('lokasi.axIndex') }}" required>
              <option value="">Pilih KPH</option>
              @foreach($kph as $o)
              <option value="{{ $o->id }}" {{ $o->id == $loket->lokasi->kph_id ? 'selected' : null }}>{{ $o->nama }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-6 m-b-10">
            <label class="control-label">Lokasi</label>
            <select name="lokasi" class="form-control lokasi" required>
              <option value="">Pilih Lokasi</option>
              @foreach($lokasi as $o)
              <option value="{{ $o->id }}" {{ $o->id == $loket->lokasi_id ? 'selected' : null }}>{{ $o->nama }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-6 m-b-10">
            <label class="control-label">Loket</label>
            <input type="text" name="nama" class="form-control" placeholder="Nama Lokasi" value="{{ $loket->nama }}" required>
          </div>
        </div>
    
        <hr>

        <fieldset>
          <div class="repeater-custom-show-hide">
            <div data-repeater-list="petugas">
              @foreach($loket->userLoket as $o)
                <div data-repeater-item="" class="row m-b-10 row-repeater">
                  <div class="col-sm-11 m-b-10">
                    <select name="petugas[0][id]" class="form-control" required>
                      <option value="">Pilih Petugas</option>
                      @foreach($users as $m)
                      <option value="{{ $m->id }}" {{ $m->id == $o->user_id ? 'selected' : null }}>{{ $m->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-sm-1 m-t-5">
                    <span data-repeater-delete="" class="btn btn-danger btn-trans btn-rounded btn-xs">
                      <span class="fa fa-times">
                    </span>
                  </div>
                </div>
              @endforeach
            </div>

            <div class="form-group row m-b-5">
              <div class="col-sm-4">
                <span data-repeater-create="" class="btn btn-primary btn-add-mitra btn-trans btn-rounded btn-sm">
                  <span class="ti ti-plus"></span> Tambah Petugas
                </span>
              </div>
            </div>
          </div>
        </fieldset>

        <div class="row">
          <div class="col-md-12">
            <button class="btn btn-success pull-right">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection


@section('js-top')
  <!-- Repeater -->
  <script src="{{ asset('template/assets/plugins/repeater/jquery.repeater.min.js') }}"></script>
@endsection

@section('js-bottom')
<script type="text/javascript">
  $(document).ready(function () {
    
    $('.repeater-custom-show-hide').repeater({
      show: function () {
        $(this).slideDown();
      },
      hide: function (remove) {
        if (confirm('Are you sure you want to remove this item?')) {
          $(this).slideUp(remove);
        }
      }
    });
    
    $('.unit-kerja').on('change', function(){ 
      var id_unit_kerja = $(this).val();
      var kphAx = $(this).data('list-kph');
      var kphEl = $(this).parent().parent().find('.kph');
      
      kphEl.find('option').remove().end().append('<option>Pilih KPH</option>')
      $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          kphEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

    $('.kph').on('change', function(){
      var id_kph = $(this).val();
      var lokasiAx = $(this).data('list-lokasi');
      var lokasiEl = $(this).parent().parent().parent().find('.lokasi');

      lokasiEl.find('option').remove().end().append('<option>Pilih Lokasi</option>')
      $.getJSON(lokasiAx, { id_kph }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          lokasiEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

  });

</script>
@endsection