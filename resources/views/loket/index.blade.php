@extends('app')

@section('title')
Data Loket
@endsection

@section('title-page')
Data Loket
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      
      @if(Auth::user()->hasPermission('loket.store'))
      <a href="#custom-modal-add" class="btn btn-success pull-right waves-effect waves-light" data-animation="door" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a"><i class="ti ti-plus"></i> Loket</a>
      @endif

      <h4 class="header-title m-t-0 m-b-30">Data Loket</h4>
      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>KPH</th>
            <th>Lokasi</th>
            <th>Loket</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @php $no=1 @endphp
          @foreach($loket as $o)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $o->lokasi->kph->nama }}</td>
            <td>{{ $o->lokasi->nama }}</td>
            <td>{{ $o->nama }}</td>
            <td>

              @if(Auth::user()->hasPermission('loket.update'))
              <a href="{{ route('loket.detail', $o->id) }}" class="btn btn-info btn-sm"><i class="ti ti-eye"></i> Detail</a>
              @endif

              @if(Auth::user()->hasPermission('loket.update'))
              <a href="{{ route('loket.edit', $o->id) }}" class="btn btn-warning btn-sm"><i class="ti ti-pencil"></i> Edit</a>
              @endif

              @if(Auth::user()->hasPermission('loket.delete'))
              <a type="button" class="btn btn-danger waves-effect waves-light btn-sm btn-delete" data-route="{{ route('loket.delete', $o->id) }}"><i class="ti ti-trash"></i> Delete</a>
              @endif

            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div><!-- end col -->
</div>
@endsection

@section('modal')
  <!-- Modal add-->
  <div id="custom-modal-add" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
    <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah Data Loket</h4>
    <div class="custom-modal-text">
      <form class="form-horizontal group-border-dashed" action="{{ route('loket.store') }}" method="POST" id="form-add">
        {{ csrf_field() }}
        <div class="form-group">
          <div class="col-sm-4">
            <select name="unit_kerja" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}" required>
              <option value="">Pilih Unit Kerja</option>
              @foreach($unitKerja as $o)
              <option value="{{ $o->id }}">{{ $o->wilayah }}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-4">
            <select name="kph" class="form-control kph" data-list-lokasi="{{ route('lokasi.axIndex') }}" required>
              <option value="">Pilih KPH</option>
            </select>
          </div>

          <div class="col-sm-4">
            <select name="lokasi" class="form-control lokasi" required>
              <option value="">Pilih Lokasi</option>
            </select>
          </div>          
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            <input type="text" name="nama" class="form-control" placeholder="Nama Loket" required>
          </div>
        </div>

        <fieldset>
          <div class="repeater-custom-show-hide">
            <div data-repeater-list="petugas">
              <div data-repeater-item="" class="row m-b-10 row-repeater">
                <div class="col-sm-11 m-b-10">
                  <select name="petugas[0][id]" class="form-control" required>
                    <option value="">Pilih Petugas</option>
                    @foreach($user as $o)
                    <option value="{{ $o->id }}">{{ $o->nama }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-sm-1 m-t-5">
                  <span data-repeater-delete="" class="btn btn-danger btn-trans btn-rounded btn-xs">
                    <span class="fa fa-times">
                  </span>
                </div>
              </div>
            </div>
            <div class="form-group row m-b-5">
              <div class="col-sm-4">
                <span data-repeater-create="" class="btn btn-primary btn-add-mitra btn-trans btn-rounded btn-sm">
                  <span class="ti ti-plus"></span> Tambah Petugas
                </span>
              </div>
            </div>
          </div>
        </fieldset>

        <div class="form-group m-b-0">
          <div class="col-sm-offset-6 col-sm-8 m-t-15">
            <button type="reset" class="btn btn-default waves-effect m-l-5">Reset</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

@endsection 

@section('css-top')
  <!-- DataTables -->
  <link href="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- datepicker -->
  <link href="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <!-- select2 -->
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
  <!-- Sweet Alert css -->
  <link href="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
  <!-- Custom box css -->
  <link href="{{ asset('template/assets/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('js-top')
  <!-- Datatables-->
  <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- Datepicker -->
  <script src="{{ asset('template/assets/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- select2 -->
  <script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
  <!-- Modal-Effect -->
  <script src="{{ asset('template/assets/plugins/custombox/dist/custombox.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/custombox/dist/legacy.min.js') }}"></script>
  <!-- Sweet Alert js -->
  <script src="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
  <!-- Repeater -->
  <script src="{{ asset('template/assets/plugins/repeater/jquery.repeater.min.js') }}"></script>
@endsection

@section('js-bottom')
<script type="text/javascript">
  $(document).ready(function () {
    
    $('.select2').select2();
    $('.repeater-custom-show-hide').repeater({
      show: function () {
        $(this).slideDown();
      },
      hide: function (remove) {
        if (confirm('Are you sure you want to remove this item?')) {
          $(this).slideUp(remove);
        }
      }
    });

    jQuery('#date-range').datepicker({
      toggleActive: true
    });

    TableManageButtons.init();
    
    $('.unit-kerja').on('change', function(){ 
      var id_unit_kerja = $(this).val();
      var kphAx = $(this).data('list-kph');
      var kphEl = $(this).parent().parent().find('.kph');
      
      kphEl.find('option').remove().end().append('<option>Pilih KPH</option>')
      $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          kphEl.append(option);
        });//end each
      });//end ajax
    });//end onchange


    $('.kph').on('change', function(){
      var id_kph = $(this).val();
      var lokasiAx = $(this).data('list-lokasi');
      var lokasiEl = $(this).parents().find('.lokasi');
      
      lokasiEl.find('option').remove().end().append('<option>Pilih Lokasi</option>')
      $.getJSON(lokasiAx, { id_kph }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          lokasiEl.append(option);
        });//end each
      });//end ajax
    });//end onchange
    
    
    $('body').delegate('.btn-delete', 'click', function(){
      var route = $(this).data('route');
      var _token = $('meta[name=csrf-token]').attr('content');

      swal({
        title: "Apakah benar data akan dihapus?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
      },function (isConfirm) {
        if (isConfirm) {
          $.post(route, {_token}).done(function(data){
            if(data.success){
              swal({title: "Dihapus", text: "Data berhasil dihapus!", type: "success"}, function(){
                location.reload();
              });
            }else{
              swal("Error", data.msg, "error");
            }
          }).fail(function(error){
            console.log(error);
          });
        }else{
          swal("Dibatalkan", "Data tidak dihapus", "error");
        }//if confirm
      });//swall
    });//ondelete

  });

</script>
@endsection