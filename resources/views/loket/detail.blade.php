@extends('app')

@section('title')
Detail Loket
@endsection

@section('title-page')
Detail Loket
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card-box">

      <div class="row">
        <div class="col-md-6">
          <h4 class="header-title m-t-0 text-muted">Data Loket</h4>
          <dl class="row">
            <dt class="col-sm-3">Nama</dt>
            <dd class="col-sm-9">{{ $loket->nama }}</dd>
            <dt class="col-sm-3">Lokasi</dt>
            <dd class="col-sm-9">{{ $loket->lokasi->nama }}</dd>
            <dt class="col-sm-3">KPH</dt>
            <dd class="col-sm-9">{{ $loket->lokasi->kph->nama }}</dd>
            <dt class="col-sm-3">Unit Kerja</dt>
            <dd class="col-sm-9">{{ $loket->lokasi->kph->unitKerja->wilayah }}</dd>
          </dl>
        </div>

        <div class="col-md-6">
          <h4 class="header-title m-t-0 text-muted">Petugas</h4>
          <dl class="row">
            @php $no=1; @endphp
            @foreach($loket->userLoket as $o)
            <dt class="col-sm-1">{{$no++}}</dt>
            <dd class="col-sm-11">{{ $o->user->nama }}</dd>
            @endforeach
          </dl>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <h4 class="header-title m-t-20 m-b-10">Tiket di Loket Ini</h4>
      <div class="m-t-20">
        <table id="datatable-buttons" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th style="width:25px">#</th>
              <th>Tiket</th>
              <th>Waktu</th>
              <th>Harga</th>
            </tr>
          </thead>
          <tbody>
            @php $no=1; @endphp
            @foreach($loket->tiketWisata as $o)
            <tr>
              <td>{{ $no++ }}</td>
              <td>{{ $o->jenisTiket->nama }}</td>
              <td>{{ $o->jenis_waktu }}</td>
              <td>{{ number_format($o->harga) }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection

@section('css-top')
  <!-- DataTables -->
  <link href="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- datepicker -->
  <link href="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <!-- select2 -->
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
  <!-- Sweet Alert css -->
  <link href="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
  <!-- Custom box css -->
  <link href="{{ asset('template/assets/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('js-top')
  <!-- Datatables-->
  <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- Datepicker -->
  <script src="{{ asset('template/assets/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- select2 -->
  <script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
  <!-- Modal-Effect -->
  <script src="{{ asset('template/assets/plugins/custombox/dist/custombox.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/custombox/dist/legacy.min.js') }}"></script>
  <!-- Sweet Alert js -->
  <script src="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
  <!-- Repeater -->
  <script src="{{ asset('template/assets/plugins/repeater/jquery.repeater.min.js') }}"></script>
@endsection


@section('js-bottom')
<script type="text/javascript">
  $(document).ready(function(){
    TableManageButtons.init();
  });
</script>
@endsection