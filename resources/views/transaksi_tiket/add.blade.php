@extends('app')

@section('title')
Transaksi
@endsection

@section('title-page')
Transaksi
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <form action="{{ route('transaksi_tiket.store') }}" method="post">
        {{ csrf_field() }}
        <fieldset>
          <div class="repeater-custom-show-hide">
            <div data-repeater-list="data">

              <div data-repeater-item="" class="m-b-10">
                <div class="form-group row  d-flex align-items-end">
    
                  <div class="col-sm-3 m-b-10">
                    <label class="control-label">Pilih Unit Kerja</label>
                    <select name="data[0][unit_kerja]" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}">
                      <option value="">Pilih Unit Kerja</option>
                      @foreach($unitKerja as $o)
                      <option value="{{ $o->id }}">{{ $o->wilayah }}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="col-sm-3 m-b-10">
                    <label class="control-label">Pilih KPH</label>
                    <select name="data[0][kph]" class="form-control kph" data-list-lokasi="{{ route('lokasi.axIndex') }}">
                      <option value="">Pilih KPH</option>
                    </select>
                  </div>

                  <div class="col-sm-3 m-b-10">
                    <label class="control-label">Lokasi</label>
                    <select name="data[0][lokasi]" class="form-control lokasi" data-list-loket="{{ route('loket.axIndex') }}" required>
                      <option value="">Pilih Lokasi</option>
                    </select>
                  </div>
                  
                  <div class="col-sm-3 m-b-10">
                    <label class="control-label">Loket</label>
                    <select name="data[0][loket]" class="form-control loket" data-list-jenis-tiket="{{ route('tiket.axIndex') }}">
                      <option value="">Pilih Loket</option>
                    </select>
                  </div>
                  
                  <div class="col-sm-3 m-b-10">
                    <label class="control-label">Jenis Tiket</label>
                    <select name="data[0][jenis_tiket]" class="form-control jenis-tiket">
                      <option value="">Pilih Jenis Tiket</option>
                    </select>
                  </div>

                  <div class="col-sm-3 m-b-10">
                    <label class="control-label">Jumlah Pengunjung</label>
                    <input type="text" class="form-control" name="jumlah" placeholder="Jumlah Pengunjung">
                  </div>

                  <div class="col-sm-1 m-t-5">
                    <span data-repeater-delete="" class="btn btn-danger btn-trans btn-rounded btn-xs">
                      <span class="fa fa-times">
                    </span>
                  </div>
                </div>
              </div>
    
            </div>
            <div class="form-group row mb-0">
              <div class="col-sm-12">
                <span data-repeater-create="" class="btn btn-primary btn-trans btn-rounded btn-sm">
                  <span class="ti ti-plus"></span> Tambah Transaksi
                </span>
              </div>
            </div>
          </div>
        </fieldset>
        <button class="btn btn-success btn-lg mt-5">Simpan</button>
      </form>
    </div>
  </div>
</div>

@endsection

@section('css-top')
<link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('js-top')
<script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('template/assets/plugins/repeater/jquery.repeater.min.js') }}"></script>
@endsection

@section('js-bottom')
<script>
  $(document).ready(function(){
    $('.select2').select2();
    $('.repeater-custom-show-hide').repeater({
      show: function () {
        $(this).slideDown();
      },
      hide: function (remove) {
        if (confirm('Are you sure you want to remove this item?')) {
          $(this).slideUp(remove);
        }
      }
    });

    $('body').delegate('.unit-kerja', 'change', function(){ 
      var id_unit_kerja = $(this).val();
      var kphAx = $(this).data('list-kph');
      var kphEl = $(this).parent().parent().find('.kph');
      
      kphEl.find('option').remove().end().append('<option>Pilih KPH</option>')
      $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          kphEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

    $('body').delegate('.kph', 'change', function(){
      var id_kph = $(this).val();
      var lokasiAx = $(this).data('list-lokasi');
      var lokasiEl = $(this).parent().parent().parent().find('.lokasi');

      lokasiEl.find('option').remove().end().append('<option>Pilih Lokasi</option>')
      $.getJSON(lokasiAx, { id_kph, has_tiket:true }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          lokasiEl.append(option);
        });//end each
      });//end ajax
    });//end onchange
    
    var id_lokasi = null;
    $('body').delegate('.lokasi', 'change', function(){
      id_lokasi = $(this).val();
      var loketAx = $(this).data('list-loket');
      var loketEl = $(this).parent().parent().find('.loket');
      
      loketEl.find('option').remove().end().append('<option>Pilih Loket</option>')
      $.getJSON(loketAx, { id_lokasi }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          loketEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

    $('body').delegate('.loket', 'change', function(){
      var id_loket = $(this).val();
      var jenisTiketAx = $(this).data('list-jenis-tiket');
      var jenisTiketEl = $(this).parent().parent().find('.jenis-tiket');
      
      jenisTiketEl.find('option').remove().end().append('<option>Pilih Jenis Tiket</option>')
      $.getJSON(jenisTiketAx, { id_loket, id_lokasi }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id_jenis_tiket +"'>"+ value.jenis_tiket +"</option>";
          jenisTiketEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

  });
</script>
@endsection
