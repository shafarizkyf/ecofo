@extends('app')

@section('title')
Tambah Pengguna
@endsection

@section('title-page')
Tambah Pengguna
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="row m-b-20">

          <div class="col-md-4">
            <div class="row">
              <div class="form-group col-md-12">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Budi Santoso" required>
                @if($errors->has('nama'))
                <span class="text-danger">{{ $errors->first('nama') }}</span>
                @endif
              </div>
              <div class="form-group col-md-12">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" placeholder="budisantoro" required>
                @if($errors->has('username'))
                <span class="text-danger">{{ $errors->first('username') }}</span>
                @endif
              </div>
              <div class="form-group col-md-12">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" placeholder="user@gmail.com">
                @if($errors->has('email'))
                <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
              </div>
              <div class="form-group col-md-12">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" required>
                @if($errors->has('password'))
                <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
              </div>
              <div class="form-group col-md-12">
                <label for="repassword">Konfirmasi Password</label>
                <input type="password" class="form-control" name="repassword" placeholder="Konfirmasi Password" required>
                @if($errors->has('repassword'))
                <span class="text-danger">{{ $errors->first('repassword') }}</span>
                @endif
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="row">
              <div class="form-group col-md-12">
                <label for="unit_kerja">Pilih Unit Kerja</label>
                <select name="unit_kerja" class="form-control unit-kerja" data-list-kph="{{ route('kph.axIndex') }}">
                  <option value="">Pilih Unit Kerja</option>
                  @foreach($unitKerja as $o)
                  <option value="{{ $o->id }}">{{ $o->wilayah }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group col-md-12">
                <label for="kph">Pilih KPH</label>
                <select name="kph" class="form-control kph" required>
                  <option value="">Pilih KPH</option>
                </select>
                @if($errors->has('kph'))
                <span class="text-danger">{{ $errors->first('kph') }}</span>
                @endif
              </div>
              <div class="form-group col-md-12">
                <label for="level">Pilih Tingkat Akses</label>
                <select name="level" class="form-control" required>
                  <option value="">Pilih Tingkat Akses</option>
                  @foreach($level as $o)
                  <option value="{{ $o->id }}">{{ $o->nama }}</option>
                  @endforeach
                </select>
                @if($errors->has('level'))
                <span class="text-danger">{{ $errors->first('level') }}</span>
                @endif
              </div>
              <div class="form-group col-md-12">
                <label for="foto">Foto</label>
                <input type="file" class="dropify" name="foto" data-default-file="{{ asset('placeholder.jpg') }}"  />
              </div>
            </div>
          </div>
          
          <div class="col-md-4">
            <fieldset>
              <div class="repeater-custom-show-hide">
                <div data-repeater-list="data">
                  <div data-repeater-item="" class="m-b-10">
                    <div class="form-group row  d-flex align-items-end">
        
                      <div class="col-sm-10 m-b-10">
                        <label class="control-label">Pilih Role</label>
                        <select name="data[0][role]" class="form-control" required>
                          <option value="">Pilih Role</option>
                          @foreach($role as $o)
                          <option value="{{ $o->id }}">{{ $o->nama }}</option>
                          @endforeach
                        </select>
                      </div>
    
                      <div class="col-sm-1 m-t-5">
                        <span data-repeater-delete="" class="btn btn-danger btn-trans btn-rounded btn-xs">
                          <span class="fa fa-times">
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                @if($errors->has('data.*'))
                <span class="text-danger">{{ $errors->first('data.*') }}</span>
                @endif                
                <div class="form-group row mb-0">
                  <div class="col-sm-12">
                    <span data-repeater-create="" class="btn btn-primary btn-trans btn-rounded btn-sm">
                      <span class="ti ti-plus"></span> Tambah Role
                    </span>
                  </div>
                </div>
              </div>
            </fieldset>
          </div>

          <div class="col-md-12">
            <button class="btn btn-success pull-right mt-5">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('css-top')
<link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('template/assets/plugins/fileuploads/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js-top')
<script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('template/assets/plugins/repeater/jquery.repeater.min.js') }}"></script>
<script src="{{ asset('template/assets/plugins/fileuploads/js/dropify.min.js') }}"></script>
@endsection

@section('js-bottom')
<script>
  $(document).ready(function(){
    $('.select2').select2();
    $('.repeater-custom-show-hide').repeater({
      show: function () {
        $(this).slideDown();
      },
      hide: function (remove) {
        if (confirm('Are you sure you want to remove this item?')) {
          $(this).slideUp(remove);
        }
      }
    });

    $('.unit-kerja').on('change', function(){ 
      var id_unit_kerja = $(this).val();
      var kphAx = $(this).data('list-kph');
      var kphEl = $(this).parent().parent().find('.kph');
      
      kphEl.find('option').remove().end().append('<option>Pilih KPH</option>')
      $.getJSON(kphAx, { id_unit_kerja }).then(function(data){
        $.each(data, function(index, value){
          var option = "<option value='"+ value.id +"'>"+ value.nama +"</option>";
          kphEl.append(option);
        });//end each
      });//end ajax
    });//end onchange

    $('.dropify').dropify({
      messages: {
        'default': 'Drag and drop a file here or click',
        'replace': 'Drag and drop or click to replace',
        'remove': 'Remove',
        'error': 'Ooops, something wrong appended.'
      },
      error: {
        'fileSize': 'The file size is too big (1M max).'
      }
    });    
  });
</script>
@endsection
