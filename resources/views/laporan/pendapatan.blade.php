@extends('app')

@section('title')
Laporan Perolehan
@endsection

@section('content')
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="card-box">
      <h4 class="header-title m-t-0">Laporan Perolehan</h4>
      <form action="">
        <div class="row">
            <div class="col-sm-12 m-b-10">
              <select name="laporan" class="form-control select2" required>
                <option value="">Pilih Jenis Laporan</option>
                <option value="pendapatan">Pendapatan</option>
                <option value="pengunjung">Pengunjung</option>
              </select>
            </div>
            <div class="col-sm-12 m-b-10">
              <select name="kph" class="form-control select2" required>
                <option value="">Pilih Kph</option>
                @foreach($kph as $o)
                <option value="{{ $o->id }}">{{ $o->nama }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-sm-12 m-b-10">
              <select name="periode" class="form-control" required>
                <option value="">Pilih Periode</option>
                <option value="1">Periode I</option>
                <option value="2">Periode II</option>
              </select>
            </div>
            <div class="col-sm-12 m-b-10">
              <input type="text" name="bulan" class="form-control" placeholder="Bulan" id="datepicker" required>
            </div>
            <div class="col-sm-2">
              <button class="btn btn-primary">Lihat Laporan</button>
            </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('css-top')
<!-- DataTables -->
<link href="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Datatable init js -->
<script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
<!-- datepicker -->
<link href="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
<!-- select2 -->
<link href="{{ asset('template/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('template/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('js-top')
  <!-- Datatables-->
  <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
  <!-- Datatable init js -->
  <script src="{{ asset('template/assets/pages/datatables.init.js') }}"></script>
  <!-- Datepicker -->
  <script src="{{ asset('template/assets/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- select2 -->
  <script src="{{ asset('template/assets/plugins/select2/dist/js/select2.min.js') }}"></script>
@endsection

@section('js-bottom')
<script type="text/javascript">
  $(document).ready(function () {
    $('.select2').select2();
    $('#datepicker').datepicker({
      orientation: 'top',
      format: "yyyy-mm",
      viewMode: "months", 
      minViewMode: "months",
    });
    TableManageButtons.init()
  });
</script>
@endsection