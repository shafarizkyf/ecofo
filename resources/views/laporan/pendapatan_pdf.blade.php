<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <style>
    
  @page { 
    margin: 0.5cm;
  }
  
  body { 
    margin: 0.5px; 
    font-family: Arial, Helvetica, sans-serif;
  }
  
  table{
    width: 100%;
    border: 1px black solid;
    border-collapse: collapse;
    font-size: 8px;
  }
  
  table th, table td{
    border: 1px black solid;
    padding: 2px 5px;
  }

  table th{
    text-align: center;
    padding: 2px;
  }

  h5{
    margin: 0;
    padding: 0;
    font-size: 10px;
    text-align: center;
    text-transform: uppercase;
  }
  .center{
    text-align: center;
  }
  .right{
    text-align: right;
  }
  .bold{
    font-weight: bold;
  }
  #tbody-header{
    background: #e2e2e2;
    text-align: center;
    font-weight: bold;
  }

  </style>
</head>
<body>

  <div style="margin-bottom:10px;">
    <h5>Laporan Pendapatan Wisata</h5>
    <h5>Kph Banyumas Timur</h5>
    <h5>Periode {{ strtoupper($date->format('F Y')) }}</h5>
  </div>
  <table align="center">
    <thead>
      <tr>
        <th rowspan="2">NO</th>
        <th rowspan="2">KPH</th>
        <th rowspan="2">NAMA LOKASI</th>
        <th rowspan="2">TARGET PENDAPATAN</th>
        <th colspan="5">REALISASI PENDAPATAN SD PERIODE LALU</th>
        <th colspan="5">REALISASI PENDAPATAN DLM PERIODE II-{{ strtoupper($date->format('F Y')) }}</th>
        @if($periode == 2)
        <th colspan="5">REALISASI PENDAPATAN SD PERIODE II-{{ strtoupper($date->format('F Y')) }}</th>
        @endif
        @if($periode == 2)
        <th colspan="2">% PENDAPATAN BERSIH</th>
        @else
        <th>% PENDAPATAN BERSIH</th>
        @endif
      </tr>
      <tr>
        <th>PENDAPATAN KOTOR</th>
        <th>SHARING</th>
        <th>ASURANSI</th>
        <th>KEWAJIBAN LAIN</th>
        <th>PENDAPATAN BERSIH</th>

        <th>PENDAPATAN KOTOR</th>
        <th>SHARING</th>
        <th>ASURANSI</th>
        <th>KEWAJIBAN LAIN</th>
        <th>PENDAPATAN BERSIH</th>

        @if($periode == 2)
        <th>PENDAPATAN KOTOR</th>
        <th>SHARING</th>
        <th>ASURANSI</th>
        <th>KEWAJIBAN LAIN</th>
        <th>PENDAPATAN BERSIH</th>
        @endif

        <th>DLM PERIODE</th>
        @if($periode == 2)
        <th>SD PERIODE</th>
        @endif
      </tr>
    </thead>
    <tbody>
      <tr id="tbody-header">
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>6</td>
        <td>7</td>
        <td>8</td>
        <td>9(5-(6+7+8))</td>
        <td>10</td>
        <td>11</td>
        <td>12</td>
        <td>13</td>
        <td>14(10-(11+12+13))</td>
        @if($periode == 2)
        <td>15</td>
        <td>16</td>
        <td>17</td>
        <td>18</td>
        <td>19(15-(16+17+18))</td>
        @endif
        <td>20(14:4)</td>
        @if($periode == 2)
        <td>21(19:4)</td>
        @endif
      </tr>

      @php
        $no=1;
        $dalamPeriode = 0;
        $sampaiPeriode = 0;
        //kalau bulan januari, periode sebelumnya 0
        $isJanuari = date('n', strtotime($bulan)) == 1
      @endphp
      <!-- MAIN DATA -->
      @foreach($lokasi as $o)
      <tr>
        <td>{{ $no++ }}</td> <!-- no -->
        <td style="width:80px;">{{ $o->kph->nama }}</td> <!-- kph -->
        <td style="width:80px;">{{ $o->nama }}</td> <!-- lokasi -->

        <td class="right">{{ number_format($o->targetPendapatan($tahun)) }}</td> <!-- target pendapatan -->
        <td class="right">{{ $isJanuari ? '0' : number_format($o->pendapatanKotor(1, $bulan)) }}</td> <!-- pendapatan kotor -->
        <td class="right">{{ $isJanuari ? '0' : number_format($o->sharing(1, $bulan)) }}</td> <!-- sharing -->
        <td class="right">{{ $isJanuari ? '0' : number_format($o->asuransi(1, $bulan)) }}</td> <!-- asuransi -->
        <td class="right">{{ $isJanuari ? '0' : number_format($o->kewajibanLain(1, $bulan)) }}</td> <!-- kewajibal lain -->
        <td class="right">{{ $isJanuari ? '0' : number_format($o->pendapatanBersih(1, $bulan)) }}</td> <!-- pendapatan bersih -->

        <td class="right">{{ number_format($o->pendapatanKotor(2, $bulan)) }}</td> <!-- pendapatan kotor -->
        <td class="right">{{ number_format($o->sharing(2, $bulan)) }}</td> <!-- sharing -->
        <td class="right">{{ number_format($o->asuransi(2, $bulan)) }}</td> <!-- asuransi -->
        <td class="right">{{ number_format($o->kewajibanLain(2, $bulan)) }}</td> <!-- kewajibal lain -->
        <td class="right">{{ number_format($o->pendapatanBersih(2, $bulan)) }}</td> <!-- pendapatan bersih -->

        @if($periode == 2)
        <td class="right">{{ number_format($o->pendapatanKotor(3, $bulan)) }}</td> <!-- pendapatan kotor -->
        <td class="right">{{ number_format($o->sharing(3, $bulan)) }}</td> <!-- sharing -->
        <td class="right">{{ number_format($o->asuransi(3, $bulan)) }}</td> <!-- asuransi -->
        <td class="right">{{ number_format($o->kewajibanLain(3, $bulan)) }}</td> <!-- kewajibal lain -->
        <td class="right">{{ number_format($o->pendapatanBersih(3, $bulan)) }}</td> <!-- pendapatan bersih -->
        @endif

        @if(!$o->targetPendapatan($tahun))
          @continue
        @endif

        @php 
          $dalamPeriode = $dalamPeriode + ($o->pendapatanBersih(2, $bulan)/$o->targetPendapatan($tahun));
          $sampaiPeriode = $sampaiPeriode + ($o->pendapatanBersih(3, $bulan)/$o->targetPendapatan($tahun));
        @endphp

        <td class="right">{{ number_format($o->pendapatanBersih(2, $bulan)/$o->targetPendapatan($tahun), 5) }}</td> <!-- dlm periode -->
        @if($periode == 2)
        <td class="right">{{ number_format($o->pendapatanBersih(3, $bulan)/$o->targetPendapatan($tahun), 5) }}</td> <!-- sd periode -->
        @endif
      </tr>
      @endforeach
      <!-- END MAIN DATA -->

      <!-- SUM MAIN DATA -->
      <tr>
        <td>&nbsp;</td> <!-- no -->
        <td>&nbsp;</td> <!-- kph -->
        <td>&nbsp;</td> <!-- lokasi -->
        <td class="right bold">{{ number_format($targetPendapatan) }}</td> <!-- target pendapatan -->
        <td class="right bold">{{ number_format($o->totalPendapatanKotor(1, $bulan)) }}</td> <!-- pendapatan kotor -->
        <td class="right bold">{{ number_format($o->totalSharing(1, $bulan)) }}</td> <!-- sharing -->
        <td class="right bold">{{ number_format($o->totalAsuransi(1, $bulan)) }}</td> <!-- asuransi -->
        <td class="right bold">{{ number_format($o->totalKewajibanLain(1, $bulan)) }}</td> <!-- kewajibal lain -->
        <td class="right bold">{{ number_format($o->totalPendapatanBersih(1, $bulan)) }}</td> <!-- pendapatan bersih -->

        <td class="right bold">{{ number_format($o->totalPendapatanKotor(2, $bulan)) }}</td> <!-- pendapatan kotor -->
        <td class="right bold">{{ number_format($o->totalSharing(2, $bulan)) }}</td> <!-- sharing -->
        <td class="right bold">{{ number_format($o->totalAsuransi(2, $bulan)) }}</td> <!-- asuransi -->
        <td class="right bold">{{ number_format($o->totalKewajibanLain(2, $bulan)) }}</td> <!-- kewajibal lain -->
        <td class="right bold">{{ number_format($o->totalPendapatanBersih(2, $bulan)) }}</td> <!-- pendapatan bersih -->

        @if($periode == 2)
        <td class="right bold">{{ number_format($o->totalPendapatanKotor(3, $bulan)) }}</td> <!-- pendapatan kotor -->
        <td class="right bold">{{ number_format($o->totalSharing(3, $bulan)) }}</td> <!-- sharing -->
        <td class="right bold">{{ number_format($o->totalAsuransi(3, $bulan)) }}</td> <!-- asuransi -->
        <td class="right bold">{{ number_format($o->totalKewajibanLain(3, $bulan)) }}</td> <!-- kewajibal lain -->
        <td class="right bold">{{ number_format($o->totalPendapatanBersih(3, $bulan)) }}</td> <!-- pendapatan bersih -->
        @endif

        <td class="right bold">{{ number_format($dalamPeriode, 4) }}</td> <!-- dlm periode -->
        @if($periode == 2)
        <td class="right bold">{{ number_format($sampaiPeriode, 4) }}</td> <!-- sd periode -->
        @endif
      </tr>
      <!-- END SUM MAIN DATA -->


    </tbody>
  </table>
  
</body>
</html>