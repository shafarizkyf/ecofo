<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <style>
  @page { 
    margin: 1.5cm;
  }
  
  body { 
    margin: 0.5px; 
    font-family: Arial, Helvetica, sans-serif;
  }
  
  table{
    width: 100%;
    border: 1px black solid;
    border-collapse: collapse;
    font-size: 12px;
  }  

  table th, table td{
    border: 1px black solid;
    padding: 2px 10px; 
  }
  
  h5{
    text-align: center;
    text-transform: uppercase;
  }

  h5, p{
    margin: 0;
    padding: 0;
    font-size: 12px;    
  }

  .tbody-header{
    background: #e2e2e2;
    text-align: center;
    font-weight: bold;
  }

  .text-right{
    text-align: right;
  }

  .text-left{
    text-align: left;
  }

  .text-center{
    text-align: center;
  }

  </style>
</head>
<body>
  
  <div style="margin-bottom:30px;">
    <h5>Berita Acara dan Lampiran Pendapatan</h5>
    <h5>{{ $tiketWisata->lokasi->nama }}</h5>
    <h5>Periode Tanggal {{ $tanggal['dari']->format('d-m-Y') }} s.d {{ $tanggal['sampai']->format('d-m-Y') }}</h5>
  </div>

  <div class="" style="margin-bottom:10px;">
    <p>Yang bertanda tangan di bawah ini: </p>
    <p style="margin-left:20px; margin-top:10px;">Nama : ...............</p>
    <p style="margin-left:20px; margin-bottom:10px;">Jabatan : .............</p>
    <p>Menyerahkan Bagi Hasil Pendapatan kepada Perum Perhutani</p>
    <p><strong>Periode Tanggal {{ $tanggal['dari']->format('d-m-Y') }} s.d {{ $tanggal['sampai']->format('d-m-Y') }}</strong></p>
  </div>

  @php $i=0; @endphp
  @foreach ($tiketByLokasi as $tiket)    
    <table align="center" style="margin-top:30px; {{ $i != 0 ? 'page-break-before: always;' : null }} ">
      <thead>
        <tr>
          <th rowspan="2" style="width:20px;">No</th>
          <th rowspan="2">Tanggal</th>
          <th colspan="4" class="text-center">{{ $tiket->jenisTiket->nama }} <br> {{ $tiket->harga }} - {{ $tiket->jenis_waktu }}</th>
        </tr>
        <tr>
          <th colspan="2">Nomor Karcis</th>
          <th>Jumlah Lembar</th>
          <th>Rp</th>
        </tr>
      </thead>
      <tbody>
        <tr class="tbody-header">
          <td>1</td>
          <td>2</td>
          <td colspan="2">3</td>
          <td>4</td>
          <td>5</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        @php 
          $no=1; 
          $jmhLembar=0; 
          $jmhPendapatan=0; 
          $jmhBagiPerhutani=0; 
          $jmhBagiMitra=0;
          $transaksi = $tiket->transaksi()
          ->startDate($tanggal['dari']->format('Y-m-d'))
          ->endDate($tanggal['sampai']->format('Y-m-d'))
          ->byDate()
          ->transaksi()
          ->get();
        @endphp

        @foreach($transaksi as $o)
        <tr>
          <td class="text-center">{{ $no++ }}</td>
          <td class="text-center">{{ $o->created_at->format('d-m-Y') }}</td>
          <td class="text-center">{{ $o->min }}</td>
          <td class="text-center">{{ $o->max }}</td>
          <td class="text-right">{{ number_format($o->total) }}</td>
          <td class="text-right">{{ number_format($tiket->harga * $o->total) }}</td>
          @php
            $pendapatan = $tiket->harga * $o->total;
            $jmhLembar += $o->total; $jmhPendapatan += $pendapatan; 
          @endphp
        </tr>
        @endforeach
        <tr class="tbody-header">
          <td></td>
          <td class="text-center">TOTAL</td>
          <td></td>
          <td></td>
          <td class="text-right">{{ number_format($jmhLembar) }}</td>
          <td class="text-right">{{ number_format($jmhPendapatan) }}</td>
        </tr>
      </tbody>
    </table>
    
    <div style="margin:20px 0 10px 0">
      <h5 class="">Rekapitalusai Pendapatan para Pihak</h5>
    </div>

    <table align="center" style="margin-top:30px">
      <thead>
        <tr>
          <th rowspan="2" style="width:20px;">No</th>
          <th rowspan="2">Komponen</th>
          <th rowspan="2">Total</th>
          <th colspan="2" class="text-center">Perhutani</th>
          <th colspan="2" class="text-center">LMDH</th>
        </tr>
        <tr>
          <th>Rp</th>
          <th>Jumlah</th>
          <th>Rp</th>
          <th>Jumlah</th>
        </tr>
      </thead>
      <tbody>
        @php $no=1; $total=0; @endphp
        @foreach($tiket->komponenHarga as $o)
        <tr>
          <td class="text-center">{{ $no++ }}</td>
          <td>{{ $o->biayaOperasional->nama }}</td>
          <td class="text-right">{{ number_format($jmhLembar) }}</td>
          <td class="text-right">{{ number_format($o->biaya) }}</td>
          <td class="text-right">{{ number_format($jmhLembar * $o->biaya) }}</td>
          <td class="text-right">-</td>
          <td class="text-right">-</td>
          @php 
            $total = $jmhLembar * $o->biaya;
            $jmhBagiPerhutani += $total 
          @endphp
        </tr>
        @endforeach

        @php $totalPerhutani=0; $totalMitra=0; @endphp
        @foreach($tiket->bagiHasil as $o)
        <tr>
          <td class="text-center">{{ $no++ }}</td>
          <td>Bagi Hasil {{ $o->mitra->nama }}</td>
          <td class="text-right">{{ $jmhLembar }}</td>
          <td class="text-right">{{ number_format($o->bagi_perhutani) }}</td>
          <td class="text-right">{{ number_format($jmhLembar * $o->bagi_perhutani) }}</td>
          <td class="text-right">{{ number_format($o->bagi_mitra)  }}</td>
          <td class="text-right">{{ number_format($jmhLembar * $o->bagi_mitra) }}</td>
          @php 
            $totalPerhutani = $jmhLembar * $o->bagi_perhutani;
            $totalMitra = $jmhLembar * $o->bagi_mitra;
            $jmhBagiPerhutani += $totalPerhutani; 
            $jmhBagiMitra += $totalMitra 
          @endphp
        </tr>
        @endforeach
        <tr class="tbody-header">
          <td></td>
          <td>TOTAL</td>
          <td></td>
          <td></td>
          <td class="text-right">{{ number_format($jmhBagiPerhutani) }}</td>
          <td></td>
          <td class="text-right">{{ number_format($jmhBagiMitra) }}</td>
        </tr>
      </tbody>
    </table>
  @php $i++; @endphp
  @endforeach

  <div style="margin:20px 0 10px 0">
    <h5 class="">Total Rekapitalusai Pendapatan</h5>
  </div>

  <table align="center" style="margin-top:30px">
    <thead>
      <tr>
        <th style="width:20px;">No</th>
        <th>Komponen</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody>
      @php $no=1; @endphp
      @foreach($biayaKomponen as $o)
      <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $o->kebutuhan }}</td>
        <td class="text-right">{{ number_format($o->total) }}</td>
      </tr>
      @endforeach
      <tr>
        <td>{{ $no++ }}</td>
        <td>Bagi Hasil Mitra</td>
        <td class="text-right">{{ number_format($bagiHasil->bagi_hasil_mitra) }}</td>
      </tr>
      <tr>
        <td>{{ $no++ }}</td>
        <td>Bagi Hasil Perhutani</td>
        <td class="text-right">{{ number_format($bagiHasil->bagi_hasil_perhutani) }}</td>
      </tr>
    </tbody>
  </table>

  <div class="" style="margin-top:10px; margin-bottom:50px;">
    <div style="width:50%; float:left">
      <p>Yang Menerima</p>
      <p>Junior Manager Bisnis</p>
      <p style="margin-top:50px;"><strong>( ...................... )</strong></p>
    </div>
    <div style="width:50%; float:right">
      <p>Yang Menyerahkan</p>
      <p>Ketua ......</p>
      <p style="margin-top:50px;"><strong>( ...................... )</strong></p>
    </div>
  </div>

  <span style="margin-top:50px; color:grey; font-size:10px">di-download pada: {{ date('d F Y H:i:s') }}</span>
</body>
</html>