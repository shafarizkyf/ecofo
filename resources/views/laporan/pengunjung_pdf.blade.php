<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <style>
    
  @page { 
    margin: 0.5cm;
  }
  
  body { 
    margin: 0.5px; 
    font-family: Arial, Helvetica, sans-serif;
  }
  
  table{
    width: 100%;
    border: 1px black solid;
    border-collapse: collapse;
    font-size: 10px;
  }
  
  table th, table td{
    border: 1px black solid;
    padding: 2px 5px;
  }

  table th{
    text-align: center;
    padding: 2px;
  }

  h5{
    margin: 0;
    padding: 0;
    font-size: 12px;
    text-align: center;
    text-transform: uppercase;
  }

  #tbody-header{
    background: #e2e2e2;
    text-align: center;
    font-weight: bold;
  }
  .center{
    text-align: center;
  }
  .right{
    text-align: right;
  }
  .bold{
    font-weight: bold;
  }
  </style>
</head>
<body>

  <div style="margin-bottom:10px;">
    <h5>Laporan Pengunjung Wisata</h5>
    <h5>Kph Banyumas Timur</h5>
    <h5>Periode {{ $date->format('F Y') }}</h5>
  </div>
  <table align="center">
    <thead>
      <tr>
        <th rowspan="2">NO</th>
        <th rowspan="2">KPH</th>
        <th rowspan="2">NAMA LOKASI</th>
        <th colspan="3">TARGET PENGUNJUNG</th>
        <th colspan="3">REALISASI PENGUNJUNG SD PERIODE LALU</th>
        <th colspan="3">REALISASI PENGUNJUNG DLM <br> PERIODE II-{{ strtoupper($date->format('F Y')) }}</th>
        @if($periode == 2)
        <th colspan="3">REALISASI PENGUNJUNG SD <br> PERIODE II-{{ strtoupper($date->format('F Y')) }}</th>
        @endif
        @if($periode == 2)
        <th colspan="2">% PENGUNJUNG</th>
        @else
        <th>% PENGUNJUNG</th>
        @endif
      </tr>
      <tr>

        <th>WISMAN</th>
        <th>WISNUS</th>
        <th>JUMLAH</th>

        <th>WISMAN</th>
        <th>WISNUS</th>
        <th>JUMLAH</th>

        <th>WISMAN</th>
        <th>WISNUS</th>
        <th>JUMLAH</th>

        @if($periode == 2)
        <th>WISMAN</th>
        <th>WISNUS</th>
        <th>JUMLAH</th>
        @endif

        <th>DLM PERIODE</th>
        @if($periode == 2)
        <th>SD PERIODE</th>
        @endif
      </tr>
    </thead>
    <tbody>
      <tr id="tbody-header">
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>6</td>
        <td>7</td>
        <td>8</td>
        <td>9(7+8)</td>
        <td>10</td>
        <td>11</td>
        <td>12(10+11)</td>
        @if($periode == 2)
        <td>13(9+10)</td>
        <td>14(8+11)</td>
        <td>15(9+12)</td>
        @endif
        <td>16(12:6)</td>
        @if($periode == 2)
        <td>17(15:6)</td>
        @endif
      </tr>

      @php
        $no=1;
        $dalamPeriode = 0;
        $sampaiPeriode = 0;
      @endphp

      <!-- MAIN DATA -->
      @foreach($lokasi as $o)
      <tr>
        <td>{{ $no++ }}</td>
        <td style="width:100px">{{ $o->kph->nama }}</td>
        <td style="width:100px">{{ $o->nama }}</td>
        <td class="right">{{ number_format($o->targetPengunjung($tahun, 'wna')) }}</td>
        <td class="right">{{ number_format($o->targetPengunjung($tahun, 'wni')) }}</td>
        <td class="right">{{ number_format($o->targetPengunjung($tahun)) }}</td>

        <td class="right">{{ number_format($o->pengunjung(1, $bulan, 'wna')) }}</td>
        <td class="right">{{ number_format($o->pengunjung(1, $bulan, 'wni')) }}</td>
        <td class="right">{{ number_format($o->pengunjung(1, $bulan)) }}</td>

        <td class="right">{{ number_format($o->pengunjung(2, $bulan, 'wna')) }}</td>
        <td class="right">{{ number_format($o->pengunjung(2, $bulan, 'wni')) }}</td>
        <td class="right">{{ number_format($o->pengunjung(2, $bulan)) }}</td>

        @if($periode == 2)
        <td class="right">{{ number_format($o->pengunjung(3, $bulan, 'wna')) }}</td>
        <td class="right">{{ number_format($o->pengunjung(3, $bulan, 'wni')) }}</td>
        <td class="right">{{ number_format($o->pengunjung(3, $bulan)) }}</td>
        @endif

        @if(!$o->targetPengunjung($tahun))
          @continue
        @endif

        @php 
          $dalamPeriode = $dalamPeriode + ($o->pengunjung(2, $bulan)/$o->targetPengunjung($tahun));
          $sampaiPeriode = $sampaiPeriode + ($o->pengunjung(3, $bulan)/$o->targetPengunjung($tahun));
        @endphp

        <td class="right">{{ number_format($o->pengunjung(2, $bulan)/$o->targetPengunjung($tahun), 4) }}</td>
        @if($periode == 2)
        <td class="right">{{ number_format($o->pengunjung(3, $bulan)/$o->targetPengunjung($tahun), 4) }}</td>
        @endif
      </tr>
      @endforeach
      <!-- END MAIN DATA -->

      <!-- SUM MAIN DATA -->
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td class="right bold">{{ number_format($targetPengunjung->sum('pengunjung_wna')) }}</td>
        <td class="right bold">{{ number_format($targetPengunjung->sum('pengunjung_wni')) }}</td>
        <td class="right bold">{{ number_format($targetPengunjung->sum('pengunjung')) }}</td>

        <td class="right bold">{{ number_format($o->totalPengunjung(1, $bulan, 'wna')) }}</td>
        <td class="right bold">{{ number_format($o->totalPengunjung(1, $bulan, 'wni')) }}</td>
        <td class="right bold">{{ number_format($o->totalPengunjung(1, $bulan)) }}</td>

        <td class="right bold">{{ number_format($o->totalPengunjung(2, $bulan, 'wna')) }}</td>
        <td class="right bold">{{ number_format($o->totalPengunjung(2, $bulan, 'wni')) }}</td>
        <td class="right bold">{{ number_format($o->totalPengunjung(2, $bulan)) }}</td>

        @if($periode == 2)
        <td class="right bold">{{ number_format($o->totalPengunjung(3, $bulan, 'wna')) }}</td>
        <td class="right bold">{{ number_format($o->totalPengunjung(3, $bulan, 'wni')) }}</td>
        <td class="right bold">{{ number_format($o->totalPengunjung(3, $bulan)) }}</td>
        @endif

        <td class="right bold">{{ number_format($dalamPeriode, 4) }}</td>
        @if($periode == 2)
        <td class="right bold">{{ number_format($sampaiPeriode, 4) }}</td>
        @endif
      </tr>
      <!-- END SUM MAIN DATA -->

    </tbody>
  </table>
  
</body>
</html>